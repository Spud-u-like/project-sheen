﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;




public class Search
{

    public Graph graph;
    public Game_Main main;
    public List<Node> reachable;
    public List<Node> explored;
    public List<Node> path;
    public Node goalNode;
    public int iterations = 0;
    public bool finished;
    public List<Node> finished_path;

    public Search(Graph graph, Game_Main main, bool or = false)
    {
        this.main = main;
        this.graph = graph;

    }



    

    public void Start(Node start, Node goal, moveModifiers unit)
    {

        reachable = new List<Node>();
        reachable.Add(start);

        goalNode = goal;

        explored = new List<Node>();
        path = new List<Node>();
        iterations = 0;
        start_sets(start, goal, unit);
        foreach (Node Node in graph.nodes)
        {
            Node.Clear();
        }

    }

    public void RetracePath(Node startNode, Node endNode)
    {

        Debug.Log(startNode.gridX + " " + startNode.gridY);
        Debug.Log(endNode.gridX + " " + endNode.gridY);
        List<Node> path = new List<Node>();
        Node currentNode = endNode;
        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }

        path.Reverse();
        finished_path = path;
        Debug.Log(path.Count);
        return;

    }

    public void Step()
    {
        if (path.Count > 0)
        {
            return;
        }
        if (reachable.Count == 0)
        {
            finished = true;
            return;
        }

        iterations++;

        var node = ChoseNode();
        if (node == goalNode)
        {
            while (node != null)
            {
                path.Insert(0, node);
                node = node.parent;
            }
            finished = true;
            return;
        }

        reachable.Remove(node);
        explored.Add(node);

        for (var i = 0; i < node.adjecent.Count; i++)
        {
            AddAdjacent(node, node.adjecent[i]);
        }
    }

    public void AddAdjacent(Node node, Node adjacent)
    {
        if (FindNode(adjacent, explored) || FindNode(adjacent, reachable))
        {
            return;
        }
        adjacent.parent = node;
        reachable.Add(adjacent);
    }

    public bool FindNode(Node node, List<Node> list)
    {
        return GetNodeIndex(node, list) >= 0;
    }

    public int GetNodeIndex(Node node, List<Node> list)
    {
        for (var i = 0; i < list.Count; i++)
        {
            if (node == list[i])
            {
                return i;
            }
        }

        return -1;
    }

    public Node ChoseNode()
    {
        //you need to assign a value to each node to decide
        //which one to return, for now returning random
        return reachable[Random.Range(0, reachable.Count)];
    }


    public void start_sets(Node start, Node goal, moveModifiers unit)
    {
        Node startNode = start;
        Node targetNode = goal;

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();
        openSet.Add(startNode);

        while (openSet.Count > 0)
        {

            Node currentNode = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < currentNode.fCost || openSet[i].fCost == currentNode.fCost && openSet[i].hCost < currentNode.hCost)
                {
                    currentNode = openSet[i];
                }
            }

            openSet.Remove(currentNode);
            closedSet.Add(currentNode);
            //Debug.Log("made it this far...");
            if (currentNode == targetNode)
            {

                RetracePath(startNode, targetNode);
                return;
            }
            int a = 0;
            foreach (Node neighbour in graph.GetNeighbours(currentNode))
            {
                a++;
                

                if (!neighbour.traversable(unit, false) || closedSet.Contains(neighbour) || !dBlocked(unit, currentNode, neighbour))
                {
                    Debug.Log("should not be..." + neighbour.gCost  + " name " + neighbour.label);
                    //a = 0;
                    continue;
                }

                int newMovementCostToNeighbour = currentNode.gCost + GetDistance(currentNode, neighbour);
                //Debug.Log("made it this far..." + neighbour.gCost + " ng " + neighbour.gCost +" " + newMovementCostToNeighbour + " name "+neighbour.label);
                //Debug.Log("should not be..." + a + " " + neighbour.label);
                if (newMovementCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newMovementCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetNode);
                    neighbour.parent = currentNode;

                    if (!openSet.Contains(neighbour))

                        openSet.Add(neighbour);
                }


            }
        }
    }

    bool dBlocked(moveModifiers playerScript, Node node, Node neighbour)
    {
        if (!main.dBlocker)
        {
            return true;
        }
        int x = node.gridX - neighbour.gridX;
        int y = node.gridY - neighbour.gridY;
        if (x == 0 || y == 0)
        {
            return true;
        }
        x = graph.diagonalBlocker(node.gridX + x, y, x, playerScript);
        y = graph.diagonalBlocker(x, node.gridY + y, y, playerScript);
        if(x!=0 && y != 0)
        {
            return true;
        }
        return false;
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int distX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int distY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (distX > distY) { return 14 * distY + 10 * (distX - distY); }
        return 14 * distX + 10 * (distY - distX);
    }
}

