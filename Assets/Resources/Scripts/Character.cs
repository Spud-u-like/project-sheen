﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

public class Character : MonoBehaviour
{

    public string NPCName = "king";
    NPC n;
    public bool leader = true;
    animationPlayer[] animations;

    animationHandler ah = new animationHandler();
    int loadCheck = 0;
    public GameObject currrent;
   //Main main;
    //NPCMap_Main NPCMapMain;

    int plotLocation = 0;
    public string NPCMapName = "king";
    NPCLoader loader;
    public string sortingLayerName = "default";
    //load npc from given file
    //load animations from list in NPC
    //animations assigned as child objects that are enabled on and off
    //lookup function for animation

    //also should probably have lookup for items
    //stats etc

    private void Start()
    {
        //print(transform.position + " START POS");
        //main = Main.instance;
        //buildCharacter();
        //print(transform.position);
  
        //transform.position = new Vector3(0, 0, 0);
    }

    public void changeSortingLayer(string s)
    {
        sortingLayerName = s;
        foreach (Transform child in transform)
        {
            print(child.GetComponent<SpriteRenderer>().sortingLayerName);
            child.GetComponent<SpriteRenderer>().sortingLayerName = s;
        }
    }

    public bool startBuild(NPCLoader _loader)
    {
        loader = _loader;
        return buildCharacter();
    }

    public bool buildCharacter()
    {
        SaveAndLoadNPC saveloadNPC = new SaveAndLoadNPC();
        Debug.Log("NPC NAME " + NPCName);
        NPC n = saveloadNPC.loadNPC(NPCName);
        if (n == null)
        {
            return false;
        }
        animations = new animationPlayer[n.animationNames.Length];
        for (int i = 0; i < n.animationNames.Length; i++)
        {
            //print(n.animationHeaders[i].name);
            //print(n.animationHeaders[i].spriteSheet);
            if (n.animationHeaders[i].name.Length != 0)
            {
                contLoadNPC(n.animationNames[i], n.animationHeaders[i], n.animationNames[i]);
            } else
            {
                loadCheck++;
            }
        }
        return true;
    }

    void contLoadNPC(string name, AnimationHeader currentAnimation, string animationName)
    {
        StartCoroutine(ah.LoadSpriteSheet(currentAnimation.spriteSheet, "NPCAnimations", (remoteTexture) => {
            if (remoteTexture)
            {
                Texture _texture = remoteTexture;
                GameObject o = buildAnimation(_texture, currentAnimation, false);
                o.transform.SetParent(this.transform);
                o.name = animationName;
                if (o.name != "down")
                {
                    o.SetActive(false);                    
                } else
                {
                    currrent = o;
                }
                loadCheck++;
                o.transform.position = new Vector3(0, 0, 0);
                if (loadCheck == animations.Length)
                {
                    print("DoneLoading");
                    
                    print(transform.position + " START POS");
                    foreach (Transform child in transform)
                    {
                        child.transform.localPosition = new Vector3(0, 0, 0);
                        child.GetComponent<SpriteRenderer>().sortingLayerName = sortingLayerName;
                    }
                    loader.NPCBuildCount();
                    //transform.position = new Vector3(0, 0, 0);
                }
            }
        }));
    }
    GameObject buildAnimation(Texture _texture, AnimationHeader ah, bool addDelegate)
    {
        GameObject o = new GameObject();

        animationPlayer a = o.AddComponent<animationPlayer>();
        //o.AddComponent<CanvasRenderer>();
        o.transform.localPosition = new Vector3(0, 0, 0);
        o.transform.localScale = new Vector3(5, 5, 1);
        
        o.AddComponent<SpriteRenderer>();
        a.startAnimation(ah, _texture, true);
        return o;
    }

    public void findAnimation(string name)
    {
        //find animation
        //disable current
        //switch to new
        foreach(Transform child in transform)
        {
            if(child.name == name)
            {
                currrent.SetActive(false);
                //currrent.GetComponent<SpriteRenderer>().enabled = false;
                currrent = child.gameObject;
                //currrent.GetComponent<SpriteRenderer>().enabled = true;
                currrent.SetActive(true);
            }
        }
    }

    public void sortingOrder(int i)
    {
        foreach (Transform child in transform)
        {
            child.GetComponent<SpriteRenderer>().sortingOrder = i;
        }
    }

    /*public void interact(Node node)
    {
        // plotLocation
        int x = main.movement.x;
        int y = main.movement.y;
        string dir = "down";
        if(x > node.gridY)
        {
            dir = "right";
        }
        print(x + " " + node.gridX + " " + dir);
        findAnimation(dir);
        Main.instance.uiHandler.goText("@size:18,bold,12@ Well would you look at that, NPC's are back in the game! @size:22,ital,bold,color:#ff94f2ff,4@ ONLY TOOK A YEAR!");
    }*/
}
