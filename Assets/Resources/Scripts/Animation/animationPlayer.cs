﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class animationPlayer : MonoBehaviour
{
    bool loading = true;
    Sprite[] currentAnimation;
    public float frameCounter = 100;
    public float frameTime = 0;
    public int frameNumber = 0;
    public bool pauseAnimation = true;
    public Texture texture;
    public AnimationHeader aHeader;
    public bool useSpriteRenderer = false;

    public void startAnimation(AnimationHeader _aHeader, Texture _texture, bool _useSpriteRenderer)
    {
        
        useSpriteRenderer = _useSpriteRenderer;
        aHeader = _aHeader;
        currentAnimation = new Sprite[aHeader.cords.Length];
        texture = _texture;
        texture.filterMode = FilterMode.Point;
        //print("aHeader.cords.Length" + aHeader.cords.Length);
        for (int k = 0; k < aHeader.cords.Length; k++)
        {
            AnimationCords f = aHeader.cords[k];
            //Debug.Log(texture.width);
            currentAnimation[k] = Sprite.Create(texture as Texture2D, new Rect(f.x, f.y, f.width, f.height), new Vector2(0.5f, 0.5f));
            //print(i + " f.x " + f.x);
        }
        //print(aHeader.flipX + " " + transform.localScale.x + " " + (transform.localScale.x > 0));
        //print(transform.localScale);
        if (aHeader.flipX)
        {
            if (transform.localScale.x > 0)
            {
                transform.localScale = new Vector3(transform.localScale.x * -1, transform.localScale.y, transform.localScale.z);
            }
        }
        if (aHeader.flipY)
        {
            if (transform.localScale.y > 0)
            {
                transform.localScale = new Vector3(transform.localScale.x, transform.localScale.y * -1, transform.localScale.z);
            }
        }
        //print(transform.localScale);
        frameCounter = aHeader.fps;
        //print(currentAnimation.Length);
        // transform.position = new Vector3(0, 0, 0);
        loading = false;
        pauseAnimation = false;
        GetComponent<SpriteRenderer>().sprite = currentAnimation[0];
    }
    void LateUpdate()
    {
        bool en = true;
        if(GetComponent<SpriteRenderer>())
        {
            en = GetComponent<SpriteRenderer>().enabled;
        }
        if (!loading && en)
        {
            if (frameTime > frameCounter / 1000 && !pauseAnimation)
            {
                changeFrame();
                frameTime = 0;
                frameNumber++;
            }
            frameTime += 1.0f * Time.deltaTime;
        }
    }

    void changeFrame()
    {
        //print("changeframe " + currrentAnimation.Length + " ");

        int childCount = currentAnimation.Length;
        if (childCount == 0)
        {
            return;
        }
        if (frameNumber > childCount - 1)
        {
            frameNumber = 0;
        }
        if (frameNumber < 0)
        {
            frameNumber = childCount - 1;
        }
        //AnimationCords f = cords[frameNumber];
        if (useSpriteRenderer)
        {
            GetComponent<SpriteRenderer>().sprite = currentAnimation[frameNumber];
        }
        else
        {
            GetComponent<Image>().sprite = currentAnimation[frameNumber]; //Sprite.Create(texture as Texture2D, new Rect(f.x, f.y, f.width, f.height), Vector2.zero);
        }
    }

}
