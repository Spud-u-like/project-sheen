﻿using UnityEngine;

public class animationPropsHolder
{
    public string animationName;
    public bool flipX;
    public bool flipY;
    public Sprite[] sprites;
    public float fps;
}
