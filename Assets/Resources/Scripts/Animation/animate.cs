﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
//using UnityEditor;
using Better;
using System;
using System.IO;


public class animate : MonoBehaviour
{
    /* Oh boy!
     * preload the spritesheets
     * and the json files for animations
     * move animations to folder named after the .png file
     * this script should then grab all assets within that folder
     * should be able to call this script, pass it a movement name and it will run it
     * https://answers.unity.com/questions/1080430/create-animation-clip-from-sprites-programmaticall.html
     * https://docs.unity3d.com/Packages/com.unity.2d.animation@4.1/manual/SpriteSwapIntro.html
     * http://gyanendushekhar.com/2018/03/18/create-play-animation-runtime-unity-tutorial/
    */

    Texture texture;

    public float frameCounter = 100;
    public float frameTime = 0;
    public int frameNumber = 0;
    public bool pauseAnimation = true;
    public string spriteSheetFileName = "bowie.png";
    public string jsonFileName = "left";
    public string animationName = "";
    public AnimationHeader[] animationHeaderList;
    public AnimationClip[] animationClipList;
    //AnimationCords[] cords;
    //AnimancerComponent ac;

    //Sprite[] up;
    //Sprite[] down;
    //Sprite[] left;
    //Sprite[] right;

    //float upSpeed = 0;
    //float downSpeed = 0;
    //float leftSpeed = 0;
    //float rightSpeed = 0;

    Sprite[] currrentAnimation;
    animationPropsHolder[] aph;

    bool loading = true;

    //ErrorHandler e = new ErrorHandler();
    animationHandler ah;

    void Start()
    {
        //ac = gameObject.AddComponent<AnimancerComponent>();
        //StartCoroutine(LoadSpriteSheet());
        extStart();
    }

    public void extStart()
    {
        ah = new animationHandler();
        print(spriteSheetFileName);
        print(ah);
        StartCoroutine(ah.LoadSpriteSheet(spriteSheetFileName, "NPCAnimations", (remoteTexture) => {
            if (remoteTexture)
            {
                texture = remoteTexture;
                texture.filterMode = FilterMode.Point;
                //Debug.Log(texture.width);
                getAllFileNames();
            }
        }));
    }

    void getAllFileNames()
    {
        StartCoroutine(ah.getAllFileNamesI(spriteSheetFileName, (filePaths) => {
            if (filePaths != null)
            {
                //Debug.Log(filePaths.Length);
                //StartCoroutine(getAnimationJson(filePaths));
                startBuildingAnimations(filePaths);
                //getAnimationJson(string spriteSheetFileName, string[] filePaths, System.Action<AnimationHeader[]> callback)
            }
        }));
    }

    void startBuildingAnimations(string[] filePaths)
    {
        StartCoroutine(ah.getAnimationJson(spriteSheetFileName, filePaths, (ahl) => {
            if (ahl != null)
            {
                animationHeaderList = ahl;
                //Debug.Log(animationHeaderList.Length);
                aph = new animationPropsHolder[filePaths.Length];
                for (int i = 0; i < aph.Length; i++)
                {
                    //makeAnime(animationHeaderList[i], animationHeaderList[i].name, i);
                    aph = ah.makeAnime(animationHeaderList[i], animationHeaderList[i].name, i, texture, aph);
                }
                pauseAnimation = false;
            }
        }));
    }

    /*public IEnumerator LoadSpriteSheet(System.Action<Texture> callback)
    {
        WWW localFile = new WWW(Application.dataPath + "/StreamingAssets/NPCAnimations/" + spriteSheetFileName);
        yield return localFile;
        if (localFile.error != null)
        {
            Debug.Log(localFile.error);
        }

        //remotetexture = localFile.texture;
        Debug.Log(localFile.texture.width);
        
        yield return null;
        callback(localFile.texture);    
    }*/

    /*public IEnumerator getAllFileNamesI(System.Action<string[]> callback)
    {
        string[] filePaths = Directory.GetFiles(Application.dataPath + "/StreamingAssets/NPCAnimations/" + Path.GetFileNameWithoutExtension(spriteSheetFileName) + "/", "*.json");
        yield return null;
        callback(filePaths);
    }*/

    public IEnumerator getAnimationJson(string[] filePaths)
    {
        animationHeaderList = new AnimationHeader[filePaths.Length];
        aph = new animationPropsHolder[filePaths.Length];
        for (int i = 0; i < filePaths.Length; i++)
        {
            //Debug.Log(filePaths[i]);
            AnimationHeader a = loadAnimation(filePaths[i]);
            yield return a;
            animationHeaderList[i] = a;
            //assignAnimation(makeAnime(a, a.name), a.name);
            makeAnime(a, a.name, i);

        }
        pauseAnimation = false;
    }

    public AnimationHeader loadAnimation(string name)
    {
        SaveAndLoadAnimations a = new SaveAndLoadAnimations();
        AnimationHeader ani = a.loadFile(Path.GetFileNameWithoutExtension(spriteSheetFileName) + "/" + Path.GetFileNameWithoutExtension(name));
        if (ani == null)
        {
            Debug.LogException(new Exception("Failed to load animation - null"), this);
            return null;
        }
        return ani;
    }

    public void findAnimation(string name)
    {
        if (name == animationName || aph == null || pauseAnimation)
        {
            return;
        }
        /*switch (name)
        {
            case "up":
                frameCounter = upSpeed;
                currrentAnimation = up;
                break;
            case "down":
                frameCounter = downSpeed;
                currrentAnimation = down;
                break;
            case "left":
                frameCounter = leftSpeed;
                currrentAnimation = left;
                break;
            case "right":
                frameCounter = rightSpeed;
                currrentAnimation = right;
                break;
            default:
                return;
        }*/
        for(int i  = 0; i < aph.Length; i++)
        {
            if(aph[i].animationName == name)
            {
                frameCounter = aph[i].fps;
                currrentAnimation = aph[i].sprites;
                GetComponent<SpriteRenderer>().flipX = aph[i].flipX;
                GetComponent<SpriteRenderer>().flipY = aph[i].flipY;
                changeFrame();
                loading = false;
            }
        }

        return;
        /*for (int i = 0; i < animationHeaderList.Length; i++)
        {
            if(animationHeaderList[i]!=null)
            {
                if(animationHeaderList[i].name == name)
                {
                    loadFrames(animationHeaderList[1]);
                    return;
                }
            }
        }*/
    }

    public void loadFrames(AnimationHeader ani)
    {
        if (ani == null || !pauseAnimation)
        {
            return;
        }
        if (pauseAnimation)
        {
            //assignAnimation(makeAnime(ani), ani.name);
            pauseAnimation = false;
        }
        /*
        frameCounter = ani.fps;
        //fileName = ani.spriteSheet;
        animationName = ani.name;
        cords = ani.cords;
        print(cords.Length);
        loading = false;
        pauseAnimation = false;*/
    }

    /*void assignAnimation(Sprite[] a, string name)
    {
        switch (name)
        {
            case "up":
                up = a;
                break;
            case "down":
                down = a;
                break;
            case "left":
                left = a;
                break;
            case "right":
                right = a;
                break;
            default:
                return;
        }
    }*/
    void LateUpdate()
    {
        if (!loading)
        {
            if (frameTime > frameCounter /1000  && !pauseAnimation)
            {
                changeFrame();
                frameTime = 0;
                frameNumber++;
            }
            frameTime += 1.0f * Time.deltaTime;
        }
    }

    void changeFrame()
    {
        //print("changeframe " + currrentAnimation + " ");
        if(currrentAnimation == null)
        {
            return;
        }
        int childCount = currrentAnimation.Length;
        if (childCount == 0)
        {
            return;
        }
        if (frameNumber > childCount - 1)
        {
            frameNumber = 0;
        }
        if (frameNumber < 0)
        {
            frameNumber = childCount - 1;
        }
        //AnimationCords f = cords[frameNumber];
        GetComponent<SpriteRenderer>().sprite = currrentAnimation[frameNumber]; //Sprite.Create(texture as Texture2D, new Rect(f.x, f.y, f.width, f.height), Vector2.zero);
    }

    void makeAnime(AnimationHeader ani, string name, int n) {
        // NANI!?!
        Sprite[] spriteList = new Sprite[ani.cords.Length];
        for (int i = 0; i < ani.cords.Length; i++)
        {
            AnimationCords f = ani.cords[i];
            Debug.Log(texture.width);
            spriteList[i] = Sprite.Create(texture as Texture2D, new Rect(f.x, f.y, f.width, f.height), new Vector2(0.5f, 0.5f));
            //print(i + " f.x " + f.x);
        }
        aph[n] = new animationPropsHolder();
        aph[n].animationName = name;
        aph[n].flipX = ani.flipX;
        aph[n].flipY = ani.flipY;
        aph[n].fps = ani.fps;
        aph[n].sprites = spriteList;
    }

    /*AnimationClip makeAnime(AnimationHeader ani)
    {*/

    /*Sprite[] spriteList = new Sprite[ani.cords.Length];
    for(int i = 0; i < ani.cords.Length; i++)
    {
        AnimationCords f = ani.cords[i];
        spriteList[i] = Sprite.Create(texture as Texture2D, new Rect(f.x, f.y, f.width, f.height), new Vector2(0.5f, 0.5f));
        print(i + " f.x " + f.x );
    }

    Sprite[] sprites = Resources.LoadAll<Sprite>("Textures"); // load all sprites in "assets/Resources/sprite" folder
    AnimationClip animClip = new AnimationClip();
    //animClip.legacy = true;
    animClip.frameRate = ani.fps;   // FPS
    animClip.wrapMode = WrapMode.Loop;
    AnimationClipSettings tSettings = AnimationUtility.GetAnimationClipSettings(animClip);
    Debug.Log(tSettings.loopTime);
    tSettings.loopTime = true;
    Debug.Log(tSettings.loopTime);
    AnimationUtility.SetAnimationClipSettings(animClip, tSettings);
    Debug.Log(AnimationUtility.GetAnimationClipSettings(animClip).loopTime);
    EditorCurveBinding spriteBinding = new EditorCurveBinding();
    spriteBinding.type = typeof(SpriteRenderer);
    spriteBinding.path = "";
    spriteBinding.propertyName = "m_Sprite";
    ObjectReferenceKeyframe[] spriteKeyFrames = new ObjectReferenceKeyframe[spriteList.Length];
    print("spriteList " + spriteList.Length);
    for (int i = 0; i < (spriteList.Length); i++)
    {
        spriteKeyFrames[i] = new ObjectReferenceKeyframe();
        spriteKeyFrames[i].time = ((float)i) / animClip.frameRate;
        spriteKeyFrames[i].value = spriteList[i];
        //GetComponent<SpriteRenderer>().sprite = spriteList[i];
        //GameObject.Instantiate(spriteList[i]);
        //GameObject new_sprite = new GameObject();

        //SpriteRenderer sss = new_sprite.AddComponent<SpriteRenderer>();
        //sss.sprite = spriteList[i];
    }
    AnimationUtility.SetObjectReferenceCurve(animClip, spriteBinding, spriteKeyFrames);


    print("animClip is looping " + animClip.isLooping);
    return animClip;
    ac.Play(animClip);
    //[SerializeField] private AnimancerComponent _Animancer;
    */
    //Animation anim = GetComponent<Animation>();

    /*print(animClip.name);
    anim.AddClip(animClip, animClip.name);
    anim.clip = animClip;
    print(anim.Play("test"));
    print(anim.isPlaying);*/

    /*animClip.name = "test";
    Animator animator = GetComponent<Animator>();



    AnimatorOverrideController animatorOverrideController = new AnimatorOverrideController(animator.runtimeAnimatorController);
    //animatorOverrideController.runtimeAnimatorController = animator.runtimeAnimatorController;
    animator.runtimeAnimatorController = animatorOverrideController;
    animatorOverrideController["test"] = animClip;
    animator.runtimeAnimatorController = animatorOverrideController;*/


    //AssetDatabase.CreateAsset(animClip, "assets/walk.anim");
    //AssetDatabase.SaveAssets();
    //AssetDatabase.Refresh();
    //}
}
