﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;

public class animationHandler// : MonoBehaviour
{

    //ErrorHandler e = new ErrorHandler();


    /*public IEnumerator LoadSpriteSheet(string spriteSheetFileName, System.Action<Texture> callback)
    {
        WWW localFile = new WWW(Application.dataPath + "/StreamingAssets/NPCAnimations/" + spriteSheetFileName);
        yield return localFile;
        if (localFile.error != null)
        {
            Debug.Log(localFile.error);
        }

        //remotetexture = localFile.texture;
        Debug.Log(Application.dataPath + "/StreamingAssets/NPCAnimations/" + spriteSheetFileName);
        Debug.Log(localFile.texture.width);

        yield return null;
        callback(localFile.texture);
    }*/

    public IEnumerator LoadSpriteSheet(string spriteSheetFileName, string ext, System.Action<Texture> callback)
        {
            Debug.Log(spriteSheetFileName);
            Texture texture = null;
        // NPCAnimations
        using (UnityWebRequest uwr = UnityWebRequestTexture.GetTexture(Application.dataPath + "/StreamingAssets/"+ ext + "/" + spriteSheetFileName))
            {
                yield return uwr.SendWebRequest();

                if (uwr.isNetworkError || uwr.isHttpError)
                {
                    Debug.Log(uwr.error);
                }
                else
                {
                    texture = DownloadHandlerTexture.GetContent(uwr);
                }
            }
            yield return null;
            //Debug.Log(texture.width);
            callback(texture);
        }

    public IEnumerator getAllFileNamesI(string spriteSheetFileName, System.Action<string[]> callback)
    {
        string[] filePaths = Directory.GetFiles(Application.dataPath + "/StreamingAssets/NPCAnimations/" + Path.GetFileNameWithoutExtension(spriteSheetFileName) + "/", "*.json");
        yield return null;
        callback(filePaths);
    }

    public IEnumerator getAnimationJson(string spriteSheetFileName, string[] filePaths, System.Action<AnimationHeader[]> callback)
    {
        AnimationHeader[] animationHeaderList = new AnimationHeader[filePaths.Length];
        SaveAndLoadAnimations saveloadani = new SaveAndLoadAnimations();
        for (int i = 0; i < filePaths.Length; i++)
        {
            //Debug.Log(filePaths[i]);
            AnimationHeader a = saveloadani.loadFile(Path.GetFileNameWithoutExtension(spriteSheetFileName) + "/" + Path.GetFileNameWithoutExtension(filePaths[i]));
            yield return a;
            animationHeaderList[i] = a;
        }
        yield return null;
        callback(animationHeaderList);
    }

    public SpritesheetHolder divideSpriteSheet(Texture texture, Transform adTo, int spriteHeight, int spriteWidth, string filename, bool isNode, SpritesheetHolder spritesheetholder, bool isSprite)
    {
        //this code used to be a thing of beauty! Not the monster its grown into!
        //still, it works! 
        texture.filterMode = FilterMode.Point;
        int h = Mathf.FloorToInt(texture.height / spriteHeight);
        int w = Mathf.FloorToInt(texture.width / spriteWidth);

        SpritesheetHolder newssh = new SpritesheetHolder();
        newssh.nodeHolderArray = new NodeHolderArray[h];

        //Debug.Log(h + " " + w + " stating values");
        h = 0;
        for (int b = 0; b < texture.height; b += spriteHeight)
        {
            NodeHolderArray nhaa = new NodeHolderArray();
            NodeHolder[] nha = new NodeHolder[w];
            w = 0;
            for (int a = 0; a < texture.width; a += spriteWidth)
            {
                int _spriteWidth = spriteWidth;
                int _spriteHeight = spriteHeight;
                if (a >= texture.width)
                {
                    continue;
                }
                if (b >= texture.height)
                {
                    continue;
                }
                if (a + _spriteWidth > texture.width)
                {
                    _spriteWidth -= (a + _spriteWidth - texture.width);
                }

                //print(b + _spriteHeight - texture.height);
                if (b + _spriteHeight > texture.height)
                {
                    _spriteHeight -= (b + _spriteHeight - texture.height);
                }

                GameObject g = addSprite(a, b, _spriteWidth, _spriteHeight, texture, adTo, filename, isNode, isSprite);

                if (isNode)
                {

                    if (spritesheetholder != null)
                    {
                        if (h < spritesheetholder.nodeHolderArray.Length)
                        {
                            if (w < spritesheetholder.nodeHolderArray[h].nodeHolder.Length)
                            {
                                nha[w] = spritesheetholder.nodeHolderArray[h].nodeHolder[w];
                                if (g)
                                {
                                    g.name = nha[w].nodeName;
                                    /*Debug.Log("g name" + g.name);
                                    if(g.name == "")
                                    {
                                        Debug.Log("doing a thing");
                                        g.name = w + filename;
                                        nha[w].nodeName = g.name;
                                    }*/

                                    
                                   Node node = g.GetComponent<Node>();
                                    node.flipX = nha[w].flipX;
                                    node.nodeName = nha[w].nodeName;
                                    node.nodeSpriteGridPosX = h;
                                    node.nodeSpriteGridPosY = w;
                                    node.landtype = nha[w].landType;
                                    node.tag = nha[w].tag;
                                    if (node.tag != "")
                                    {
                                        g.tag = node.tag;
                                    } else
                                    {
                                        node.tag = "Untagged";
                                    }
                                    /*if (g.name == "waterPath")
                                    {
                                        Debug.Log("doing a thing");
                                        Debug.Log("node.tag " + node.tag);
                                        Debug.Log("g.tag " + g.tag);
                                    }*/
                                }
                                w++;
                                continue;
                            }
                        }
                    }
                    nha[w] = new NodeHolder();
                    if (g)
                    {
                        //if its a new sheet, and the object has no name and DOES exist
                        string tempname = w.ToString() + h.ToString() + filename;
                        g.name = tempname;
                        g.GetComponent<Node>().nodeName = tempname;
                        nha[w].nodeName = tempname;
                    }
                }
                w++;
            }
            nhaa.nodeHolder = nha;
            newssh.nodeHolderArray[h] = nhaa;
            h++;
        }

        return newssh;
        //emptySpriteAnimator();
    }

    public GameObject addSprite(int x, int y, int width, int height, Texture texture, Transform parent, string fileName, bool isNode, bool isSprite)
    {
        Sprite newSprite = Sprite.Create(texture as Texture2D, new Rect(x, y, width, height), new Vector2(0.5f, 0.5f));
        if (IsTransparent(newSprite.texture as Texture2D, x, y, width, height))
        {
            return null;
        }
        GameObject g = new GameObject();
        if (isSprite)
        {
            SpriteRenderer sr = g.AddComponent<SpriteRenderer>();
            sr.sprite = newSprite;
        }
        else
        {
            Image I = g.AddComponent<Image>();
            I.sprite = newSprite;
        }
        if (parent)
        {
            g.transform.SetParent(parent, false);
        }
        if (isNode)
        {
            //Debug.Log(fileName);
            Node node = g.AddComponent<Node>();
            node.spriteX = x;
            node.spriteY = y;
            node.spriteWidth = width;
            node.spriteHeight = height;
            node.spritesheetName = fileName;
            node.layergroup = "default";
            node.nodeName = "unnamed";
        }
        else
        {
            frameData framedata = g.AddComponent<frameData>();
            framedata.x = x;
            framedata.y = y;
            framedata.width = width;
            framedata.height = height;
            framedata.parentSpriteName = fileName;
        }
        return g;
    }

    bool IsTransparent(Texture2D tex, int x, int y, int width, int height)
    {
        
        Color[] pix = tex.GetPixels(x, y, width, height);
        //Debug.Log(pix.Length);
        for (int i = 0; i < pix.Length; i++)
        {
            //Debug.Log(pix[i].a);
            if(pix[i].a != 0)
            {
                return false;
            }
        }

        return true;
    }

    public animationPropsHolder[] makeAnime(AnimationHeader ani, string name, int n, Texture texture, animationPropsHolder[] aph)
    {
        // NANI!?!
        Sprite[] spriteList = new Sprite[ani.cords.Length];
        for (int i = 0; i < ani.cords.Length; i++)
        {
            AnimationCords f = ani.cords[i];
            //Debug.Log(texture.width);
            texture.filterMode = FilterMode.Point;
            spriteList[i] = Sprite.Create(texture as Texture2D, new Rect(f.x, f.y, f.width, f.height), new Vector2(0.5f, 0.5f));
            //print(i + " f.x " + f.x);
        }
        aph[n] = new animationPropsHolder();
        aph[n].animationName = name;
        aph[n].flipX = ani.flipX;
        aph[n].flipY = ani.flipY;
        aph[n].fps = ani.fps;
        aph[n].sprites = spriteList;
        return aph;
    }

    /*
    public async Task<AnimationHeader> loadAnimation(string spriteSheetFileName, string name)
    {
        SaveAndLoadAnimations a = new SaveAndLoadAnimations();
        //Debug.Log("/StreamingAssets/NPCAnimations/" + Path.GetFileNameWithoutExtension(spriteSheetFileName) + "/" + Path.GetFileNameWithoutExtension(name));
        AnimationHeader ani = await a.loadFile(Path.GetFileNameWithoutExtension(spriteSheetFileName) + "/" + Path.GetFileNameWithoutExtension(name));
        if (ani == null)
        {
            e.error(new Exception("Failed to load animation - null"), true);
            return null;
        }
        return ani;
    }

    public async Task<Texture> RequestFile(string spriteSheetFileName)
    {
        //WWW www = new WWW(Application.dataPath + "/StreamingAssets/NPCAnimations/" + spriteSheetFileName);
        WWW localFile = await new WWW(Application.dataPath + "/StreamingAssets/NPCAnimations/" + spriteSheetFileName);
        if (localFile.error != null)
        {
            Debug.Log(localFile.error);
            Debug.Log(Application.dataPath + "/StreamingAssets/NPCAnimations/" + spriteSheetFileName);
            return null;
        }
        return localFile.texture;
    }

    public async Task<String[]> RequestMultiFile(string spriteSheetFileName)
    {
        String[] list = null;
       await Task.Run(() =>
        {
            Debug.Log("anus" +"/NPCAnimations/" + spriteSheetFileName + "/");
            list = BetterStreamingAssets.GetFiles("/NPCAnimations/" + spriteSheetFileName + "/", "*.json");
            Debug.Log("am called" + list.Length);
            //var info = new DirectoryInfo(Application.dataPath + "/StreamingAssets/NPCAnimations/" + spriteSheetFileName + "/");
            //FileInfo[] fileInfo = info.GetFiles();
            //print(fileInfo[0].FullName);

        });
        list = BetterStreamingAssets.GetFiles("/NPCAnimations/" + spriteSheetFileName + "/", "*.json");
        return list;
    }
    */
}
