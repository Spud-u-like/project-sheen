﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MousedownNPCMap : MonoBehaviour
{
    NPCMap_Main m;
    bool mousewait = false;
    public bool child = false;
    Node node;
    private void Start()
    {
        m = NPCMap_Main.instance;
        node = gameObject.GetComponent<Node>();

    }
    void OnMouseOver()
    {

        if (Input.GetMouseButton(0) && !m.mousewait)
        {
            m.mousewait = true;
            if (child)
            {
                transform.parent.GetComponent<MousedownNPCMap>().MouseHit();
            }
            else
            {
                MouseHit();
            }
        }

        //start highlight effect
    }

    void MouseHit()
    {
        //already have an NPC? Delete and replace
        //open dialog screen
        //add npc to node

        //AND if marking route, mouse down marks start
        //Second mousedown marks end

        //thinking about it, this should directly link back to the NPCMap_main

        print("NPC MAP MOUSE DOWN");
        //GameObject npc = new GameObject();
        //npc.AddComponent<moveModifiers>();
        //Character c = npc.AddComponent<Character>();
        //node.npc = npc;

        //node.npc
        //m.l.NPCBuildCount();
        //c.startBuild(m.l);
        //npc.transform.position = transform.position;

        if(m.npcToggle.isOn && node.npc == null)
        {
            makeChar();
        } else
        {
            m.changeTarget(this.gameObject);
            expandNPCDetails();
        }
    }

    void makeChar()
    {
        m.l.makeSingleNPC("king", node);
        Character c = node.npc.GetComponent<Character>();
        c.changeSortingLayer("foreground");
    }

    void expandNPCDetails()
    {
        m.expandDetails(node);
    }


    private void OnMouseExit()
    {
        //stop highlight
    }

}
