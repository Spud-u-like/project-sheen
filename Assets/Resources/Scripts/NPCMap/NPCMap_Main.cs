﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;

public class NPCMap_Main : Main
{
    public static NPCMap_Main instance;
    public GameObject settings;
    public bool mousewait = false;
    public Dropdown mapDropDown;
    public Dropdown NPCDropdown;
    repopDropdown repopDropdown;
    public MapBuilder mapBuilder = new MapBuilder();
    //public Graph graph;
    public NPCLoader l = new NPCLoader();
    public Toggle npcToggle;
    public GameObject blackBackground;
    public GameObject loadMenu;

    public bool inMenu = false;
    void Awake()
    {
        instance = this;
        BetterStreamingAssets.Initialize();
        repopDropdown = GetComponent<repopDropdown>();

        repopDropdown.repopulateDropdown(mapDropDown, "Maps", "*.JSON");
        repopDropdown.repopulateDropdown(NPCDropdown, "NPCs", "*.JSON");

        settings = GameObject.Find("SettingsHolder");
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0) && mousewait)
        {
            if (!inMenu) { 
              mousewait = false;
            }
        }

        float xAxisValue = Input.GetAxis("Horizontal");
        float zAxisValue = Input.GetAxis("Vertical");
        transform.Translate(new Vector3(xAxisValue, zAxisValue, 0.0f));
        if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
        {
            Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize - 1, 5);
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0) // backward
        {
            Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize + 1, 50);
        }
        
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            if (currentTarget != null)
            {
                if (currentTarget.tag != "ControlObject")
                {
                    //currentTarget.transform.parent.GetComponent<Image>().enabled = true;
                    //GameObject g = currentTarget.transform.parent.gameObject;
                    Destroy(currentTarget);
                    //currentTarget = g;
                    //changeTarget(g);
                    listOfLines.SetActive(false);
                    currentTarget = null;
                }
            }
        }
        
    }

    public void loadMap()
    {
        buildNewMap(mapDropDown.options[mapDropDown.value].text, false, "NPCMap", this);
        
        /*string mapName = mapDropDown.options[mapDropDown.value].text;
        if (!mapBuilder.init(mapName))
        {
            Debug.LogError("Failed to load level");
        } else
        {
            mapBuilder.buildMap(mapName, false, "NPCMap", this);
            l.init(graph, this);
        }
        //blackBackground.SetActive(false);*/
        loadMenu.SetActive(false);
        mousewait = false;
        inMenu = false;
    }

    public void expandDetails(Node node)
    {

    }

    public void loadButton()
    {
        //blackBackground.SetActive(true);
        loadMenu.SetActive(true);
        mousewait = true;
        inMenu = true;
    }

}
