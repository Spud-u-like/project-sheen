﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

public class GetVarsFromScript  {

    public FieldInfo[] getFields(Effects _e)
    {
        const BindingFlags flags = BindingFlags.Public | BindingFlags.Instance | BindingFlags.DeclaredOnly;


        FieldInfo[] fields = _e.GetType().GetFields(flags);
        return fields;
    }

}
