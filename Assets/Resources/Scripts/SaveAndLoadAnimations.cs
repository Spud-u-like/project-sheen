﻿using UnityEngine;
using System;
using System.IO;
using System.Threading.Tasks;

public class SaveAndLoadAnimations
{
    private string gameDataProjectFilePath = "/StreamingAssets/NPCAnimations/";

    public bool buildFile(string name, string folder, string spriteSheet, float fps, Transform animationHolder, bool _flipX, bool _flipY)
    {
        AnimationHeader ani = new AnimationHeader();
        ani.name = name;
        ani.spriteSheet = spriteSheet;
        ani.fps = fps;
        ani.flipX = _flipX;
        ani.flipY = _flipY;
        ani.cords = new AnimationCords[animationHolder.childCount];
        for (int i = 0; i < animationHolder.childCount; i++)
        {
            AnimationCords cords = new AnimationCords();
            Debug.Log(i + " " + animationHolder.GetChild(i).GetComponent<frameData>().x);
            cords.x = animationHolder.GetChild(i).GetComponent<frameData>().x;
            cords.y = animationHolder.GetChild(i).GetComponent<frameData>().y;
            cords.width = animationHolder.GetChild(i).GetComponent<frameData>().width;
            cords.height = animationHolder.GetChild(i).GetComponent<frameData>().height;
            ani.cords[i] = cords;
        }
        string dataAsJson = JsonUtility.ToJson(ani);
        return saveWrite(dataAsJson, name, folder);
    }

    bool saveWrite(string dataAsJson, string filename, string folder)
    {
        try
        {
            string filePath = Application.dataPath + gameDataProjectFilePath + "/" + folder + "/" + filename + ".json";
            File.WriteAllText(filePath, dataAsJson);
            return true;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return false;
        }
    }

    public AnimationHeader loadFile(string levelName)
    {
        try
        {
            string filePath = Application.dataPath + gameDataProjectFilePath + levelName + ".json";
            string dataAsJson = File.ReadAllText(filePath);
            //init_build(dataAsJson);
            AnimationHeader ani = new AnimationHeader();
            ani = JsonUtility.FromJson<AnimationHeader>(dataAsJson);
            return ani;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return null;
        }
    }

    /*public async Task<AnimationHeader> loadFileAsync(string levelName)
    {
        try
        {
            string filePath = Application.dataPath + gameDataProjectFilePath + levelName + ".json";
            string dataAsJson = File.ReadAllText(filePath);
            //init_build(dataAsJson);
            AnimationHeader ani = new AnimationHeader();
            ani = JsonUtility.FromJson<AnimationHeader>(dataAsJson);
            return ani;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return null;
        }
    }*/
}
