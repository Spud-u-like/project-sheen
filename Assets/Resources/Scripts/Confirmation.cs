﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class Confirmation : MonoBehaviour
{
    public delegate void messageCallbackDelegate(bool choice);
    public messageCallbackDelegate messageCallback = delegate { };

    private void Start()
    {
        messageCallback = blank;
    }

    public void blank(bool choice)
    {
        Debug.LogWarning("Blank called on Confirmation, this should never happen.");
    }

    public bool that( string header = "Continue?", string yes = "Yes", string no = "No")
    {
        // set button names
        // set function to call after?
        Debug.LogWarning("this far");
        messageCallback(true);
        return true;
    }
}
