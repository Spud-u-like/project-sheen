﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Game_Main : Main {

    public static Game_Main instance;

    public GameObject player;
    public Camera mainCamera;
    public GameObject SceneObjects;
    public string gamestate = "play";
    public UiHandler uiHandler;
    //public Graph graph;
    //public VillageTestMap testMap;
    public Npc npc;
    public string map = "BowiesRoom";
    GameObject settings;
    public bool dBlocker = true; //blocks diagonal movement if squares directly above and below are impassable
    public bool loading = true;


    public Movement movement;

    public GameObject menuHolder;
    public Menu menu;

    public int NPCcount = 0;
    public int numNPC = 2;
    public float offset_y = 0.35f;
    public GameObject done;

    //NPCLoader loader;

    void Awake ()
    {
        instance = this;
        settings = GameObject.Find("SettingsHolder");
        print("locating settings");
        //print(settings.GetComponent<Settings>());
        if(settings)
        {
            map = settings.GetComponent<Settings>().map;
            settings.GetComponent<Settings>().editor = false;
        }
    }

    void Start () {
        //setDepth();
        //playerScript = player.GetComponent<moveModifiers>();


        //testMap = new VillageTestMap();
        //graph = testMap.init();

        //movement = new Movement();
        //movement.init_movement(player, graph, this);

        //menu = menuHolder.GetComponent<Menu>();
        //movement.InitMenu(menu);

        //buildNewMap("map", false, "normal", this);

        buildNewMap(map, false, "normal", this);
    }

    void Update () {
        if (loading)
        {
            return;
        }

        movement.move();


        if (Input.GetKeyDown("space"))
        {
            //buildNewMap("villageTest");

        }
        

    }

    /*public void NPCBuildCount()
    {
        NPCcount++;
        if(numNPC == NPCcount)
        {
            GameObject o = new GameObject();
            moveModifiers m = o.AddComponent<moveModifiers>();
            m.landMovement = true;

            Character c = o.AddComponent<Character>();
            c.NPCName = "Bowie";
            print(o.transform.position);
            //o.transform.position = graph.getNode(4, 6).transform.position;
            graph.getNode(4, 6).npc = o;
            print(o.transform.position);
            movement = new Movement();
            movement.init_movement(player, graph, this);        
            o.transform.position = new Vector2(graph.getNode(4, 6).transform.position.x, graph.getNode(4, 6).transform.position.y + offset_y);

            menu = menuHolder.GetComponent<Menu>();
            loading = false;
        }
    }*/

    /*public void buildNewMap(string map)
    {

        //BushTestMap testMap = new BushTestMap();
        MapBuilder newmap = new MapBuilder();
        if (!newmap.init(map))
        {
            Debug.LogError("Failed to load level");
        }
        else
        {
            if (graph != null)
            {
                Destroy(graph.map);
                Destroy(graph.NPCS);
            }
            graph = newmap.buildMap("Map", false, "normal");

            loader = new NPCLoader();
            loader.init(graph, this);
            loader.NPCBuildCount();
        }
    }*/

    public override void finishBuildingMap()
    {
        player = loader.mainChar;
        if (player == null)
        {
            Debug.LogError("No Main Character Found");
        }
        else
        {
            movement = new Movement();
            movement.init_movement(player, graph, this);
        }
        menu = menuHolder.GetComponent<Menu>();
        loading = false;
        //done.SetActive(true);
    }


    private void LateUpdate()
    {
        //Camera Positioning
        if(loading)
        {
            return;
        }
        if (!graph.cameraLock)
        {
            Vector3 desiredPosition = new Vector3(player.transform.position.x, player.transform.position.y, mainCamera.transform.position.z);
            Vector3 smoothedPosition = Vector3.Lerp(mainCamera.transform.position, desiredPosition, 2.0f * Time.deltaTime);
            mainCamera.transform.position = desiredPosition;
        }
        else
        {
            mainCamera.transform.position = new Vector3(graph.cameraXPos, graph.cameraYPos, mainCamera.transform.position.z);
        }
    }

    private void setDepth()
    {
        foreach (Transform s in SceneObjects.transform)
        {
            s.GetComponent<SpriteRenderer>().sortingOrder = Mathf.RoundToInt((s.transform.position.y * 10) * -1);
        }
    }

    public Search find_path(int gridX, int gridY)
    {
        var search = new Search(graph, this);
        search.start_sets(graph.nodes[0, 0], graph.nodes[gridX, gridY], player.GetComponent<moveModifiers>());
        return search;
        /*print("Search done. Path length " + search.finished_path.Count);
        //print("Search done. Path length " + search.path.Count + " iter " + search.iterations);

        //ResetMapGroup(graph);
        Material test = Resources.Load("path") as Material;
        Material goal = Resources.Load("goal") as Material;
        Material start = Resources.Load("start") as Material;
        foreach (var node in search.finished_path)
        {
            print("howmany");
            node.ownedby.GetComponent<Renderer>().material.SetColor("_TintColor", Color.red);
            node.ownedby.GetComponent<Renderer>().material = test;
            //GetComponent<Renderer>().material
            //GetImage(node.label).color = Color.red;
            //Image target_img = mapGroup.GetComponent<Image>() as Image; // .transform.Find("Image1") as Image;
            //target_img.color = Color.red;
        }
        graph.nodes[0, 0].GetComponent<Renderer>().material = start;
        graph.nodes[gridX, gridY].ownedby.GetComponent<Renderer>().material = goal;
        */
    }
}