﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Rendering;

public class NPCLoader
{
    public float offset_y = 0.35f;
    int NPCcount = 0;
    int numNPC = 1;
    int failed = 0;
    Graph graph;
    public GameObject mainChar;
    Main m;

    string NPCName = "King";
    Node node;
    bool single = false;


    public void init(Graph _graph, Main _m)
    {
        graph = _graph;
        m = _m;
    }

    bool MakeNPC()
    {
        try
        {
            GameObject o = new GameObject();
            moveModifiers m = o.AddComponent<moveModifiers>();
            m.landMovement = true;

            Character c = o.AddComponent<Character>();
            SortingGroup layer = o.AddComponent <SortingGroup>();
            c.NPCName = NPCName;
            o.name = NPCName;
            //print(o.transform.position);
            //o.transform.position = graph.getNode(4, 6).transform.position;

            c.startBuild(this);
            if (c.leader)
            {
                mainChar = o;
            }
            //graph.getNode(4, 6).npc = o;
            //print(o.transform.position);
            //o.transform.position = new Vector2(graph.getNode(4, 6).transform.position.x, graph.getNode(4, 6).transform.position.y + offset_y);
            if (node)
            {
                o.transform.position = node.transform.position;
                layer.sortingOrder = node.gameObject.GetComponent<SpriteRenderer>().sortingOrder + 2;
                node.npc = o;
            }

            return true;
        } catch (Exception e)
        {
            Debug.LogError(e);
            return false;
        }
    }

    public void makeSingleNPC(string _name, Node _node)
    {
        NPCName = _name;
        node = _node;
        numNPC = 1;
        failed = 0;
        single = true;
        NPCBuildCount();
    }

    public void NPCBuildCount()
    {
        if (numNPC > NPCcount)
        {
            if (!MakeNPC())
            {
                failed++;
            }
            NPCcount++;
            return;
        }
        m.finishBuildingMap();
        NPCcount = 0;
        node = null;
    }
}
