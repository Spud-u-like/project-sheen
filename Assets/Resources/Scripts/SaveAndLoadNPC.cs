﻿using UnityEngine;
using System;
using System.IO;

public class SaveAndLoadNPC
{
    private string gameDataProjectFilePath = "/StreamingAssets/NPCs/";
    public NPC loadNPC(string name)
    {
        try
        {
            string filePath = Application.dataPath + gameDataProjectFilePath + name + ".json";
            string dataAsJson = File.ReadAllText(filePath);
            //init_build(dataAsJson);
            NPC n = new NPC();
            n = JsonUtility.FromJson<NPC>(dataAsJson);
            return n;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return null;
        }
    }

    public bool saveWrite(string dataAsJson, string filename)
    {
        try
        {
            string filePath = Application.dataPath + gameDataProjectFilePath + "/" + filename + ".json";
            File.WriteAllText(filePath, dataAsJson);
            return true;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return false;
        }
    }
}
