﻿using UnityEngine;
using System;
using System.IO;

public class levelSaveLoad
{
    private string gameDataProjectFilePath = "/StreamingAssets/Levels/";

    public bool buildFile(string levelName, string map, string npcMap, string music)
    {
        Level level = new Level();
        level.name = levelName;
        level.map = map;
        level.npcMap = npcMap;
        level.music = music;
        string dataAsJson = JsonUtility.ToJson(level);
        return saveWrite(dataAsJson, levelName);
    }

    bool saveWrite(string dataAsJson, string levelName)
    {
        try
        {
            string filePath = Application.dataPath + gameDataProjectFilePath + levelName + ".json";
            File.WriteAllText(filePath, dataAsJson);

            return true;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return false;
        }
    }

    public Level loadFile(string levelName)
    {
        try
        {
            string filePath = Application.dataPath + gameDataProjectFilePath + levelName + ".json";
            string dataAsJson = File.ReadAllText(filePath);
            //init_build(dataAsJson);
            Level level = new Level();
            level = JsonUtility.FromJson<Level>(dataAsJson);
            return level;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return null;
        }
    }
}
