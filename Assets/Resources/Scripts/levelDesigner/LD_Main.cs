﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System;

public class LD_Main : Main
{
    //instance
    public static LD_Main instance;
    levelSaveLoad saveLevel;
    Level level;

    //dropdowns
    public Dropdown mapDropDown;

    //text

    public GameObject levelName;

    //Confirm
    Confirmation confirm;

    repopDropdown repopDropdown;

    //oh boy, here we are again, writing checks you can't johnny cash
    //need a console log window to display errors, needs to be able to scroll - Done, it was shockingly easy, credit :https://gist.github.com/mminer/975374
    //need a confirmation window (delete level, delete map etc...)

    void Start()
    {
        instance = this;
        saveLevel = new levelSaveLoad();
        BetterStreamingAssets.Initialize();
        repopDropdown = GetComponent<repopDropdown>();

        repopDropdown.repopulateDropdown(mapDropDown, "Maps", "*.JSON");
        //repopLoadLevelDropdown(mapDropDown, "Maps");
        confirm = new Confirmation();
        //repopLoadLevelDropdown(npcDropDown, "NPCS");
        //repopLoadLevelDropdown(npcDropDown, "Music");
    }

    /*public void repopLoadLevelDropdown(Dropdown DropDown, string Extention)
    {
        try
        {
            string[] list = BetterStreamingAssets.GetFiles("/" + Extention + "/", "*.JSON");
            if (list.Length == 0)
            {
                throw new Exception("No " + Extention + " Files Found");
            }
            DropDown.ClearOptions();
            //add blank at start
            Dropdown.OptionData OD = new Dropdown.OptionData()
            {
                text = "",
            };
            DropDown.AddOptions(new List<Dropdown.OptionData>() { OD });

            foreach (string s in list)
            {
                string _text = s.Replace(".json", "");
                _text = _text.Replace("/" + Extention + "/", "");
                OD = new Dropdown.OptionData()
                {
                    text = _text,
                };
                DropDown.AddOptions(new List<Dropdown.OptionData>() { OD });
            }
        }
        catch (Exception e)
        {
            errorHandler(e, true);
        }
    }*/

    public void save()
    {
        //(saveLevel.buildFile(name, map, npcMap, music)
        if (!saveLevel.buildFile(name, "map", "npcMap", "music"))
        {
            errorHandler(new Exception("Failed to Save level"), true);
        }
    }

    public void load()
    {
        continueLoad(saveLevel.loadFile(name));
    }

    public void continueLoad(Level _level)
    {
        level = _level;
        print(level.name);
        if (level == null)
        {
            errorHandler(new Exception("Failed to load level"), true);
        }
        //print(levelName.GetComponent<Text>().text);
        levelName.GetComponent<InputField>().text = level.name;
        mapDropDown.value = GetIndexByName(mapDropDown, level.map);
        //print(levelName.GetComponent<Text>().text + " " + level.name);
        /*.text = level.name;
        .text = level.map;
        .text = level.npcmap;
        .text = level.music;*/
        // open main display
        // then highlight music track Play() 
        // npcmap to link to map (if out of bounds then highlight error)
        // get last npc x & y if greater than map dims throw error
        // do we want map to display anyway if npc out of bounds?
        // estimate tile size and place there?
        // 
        // NPC editor (yes, this again)
        // Sprite map and order 
        // designate custom sprite size, so you can select specific sprite? - back burner
        // for now all sprite are the same size, auto selected and displayed
        // put in numerical order
        // only choice should be what order are they played in (eg: 1,4,7 for forward motion)

    }

    public void deleteButtonDown()
    {
        confirm.messageCallback = del;
        confirm.that("Are you sure you want to Delete this Level?");
    }

    public void del(bool choice)
    {
        if (choice)
        {
            Debug.LogWarning("that far");
            //delete, close popup and details 
        } else
        {
            //close details
        }
    }

    public void errorHandler(Exception error, bool showToUser)
    {
        Debug.LogException(error, this);
        if (showToUser)
        {
            print(error.ToString());
        }
    }

    public int GetIndexByName(Dropdown dropDown, string name)
    {
        if (dropDown == null) {
            errorHandler(new Exception("Dropdown pop failed, no dropdown provided"), true);
            return 0;
        } // or exception
        if (string.IsNullOrEmpty(name) == true) {
            errorHandler(new Exception("Dropdown pop failed, empty name - " + dropDown.name + ", " + name), true);
            return 0;
        }
        List<Dropdown.OptionData> list = dropDown.options;
        for (int i = 0; i < list.Count; i++)
        {
            if (list[i].text.Equals(name)) { return i; }
        }
        errorHandler(new Exception("Dropdown pop failed, unable to find entry " + dropDown.name + ", " + name), true);
        return 0;
    }
}
