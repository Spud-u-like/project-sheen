﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{

    public List<Node> adjecent = new List<Node>();
    public Node parent = null;
    public GameObject ownedby = null;
    public string label = "";
    public int gCost = 0;
    public int hCost = 0;
    public int gridX = 0;
    public int gridY = 0;
    public Graph graph;
    public string landtype = "open";
    public bool isRoof = false;
    public float movementTax = 0;
    public List<Effects> effects;
    public List<Effects> eRemove;
    public List<Effects> eAdd;

    public Obj obj;
    public GameObject npc;

    bool coRunning = false;
    public SpriteRenderer sR;
    public float alpha = 1;
    public float red = 1;
    public float green = 1;
    public float blue = 1;
    public int colorSpeed = 2;

    public int offsetX;
    public int offsetY;
    public bool flipX = false;
    public bool flipY = false;
    public string layergroup;

    public int spriteX;
    public int spriteY;
    public int spriteWidth;
    public int spriteHeight;
    public string spritesheetName;
    public string nodeName;
    public int nodeSpriteGridPosX = 0;
    public int nodeSpriteGridPosY = 0;

    public string tag;


    public void Node_int(int _gridX, int _gridY, GameObject _ownedby, Graph _graph, bool debug, bool replace = false)
    {
        graph = _graph;
        gridX = _gridX;
        gridY = _gridY;
        ownedby = _ownedby;
        if (replace)
        {
            waterPath w = GetComponent<waterPath>();
            if (w)
            {
                w.StartUp();
                w.whatTile(true);
            }
        }
        if (!debug)
        {
            effects_init();

            sR = GetComponent<SpriteRenderer>();
            if (sR)
            {
                alpha = sR.color.a;
                red = sR.color.a;
                green = sR.color.a;
                blue = sR.color.a;
            }
        }

    }

    public void effects_init()
    {
        effects = new List<Effects>();
        foreach (Effects script in gameObject.GetComponents<Effects>())
        {
            //print("test");
            effects.Add(script);
        }
    }

    public void Clear()
    {
        parent = null;
        gCost = 0;
        hCost = 0;
    }

    public int fCost
    {
        get
        {
            return gCost + hCost;
        }
    }


    public void OnMouseDownExt()
    {
        print("test");
        Game_Main.instance.find_path(gridX, gridY);
    }

    bool getLandType(moveModifiers a)
    {
        switch (landtype)
        {
            case "open":
                return true;
            case "door":
                return true;
            case "water":
                return a.waterMovement;
            case "roof":
                return a.ignoreRoof;
            default:
                return false;
        }
    }

    public bool traversable(moveModifiers a, bool _overrideNpc)
    {
        
        if (npc && !_overrideNpc)
        {
            return false;
        }
        //Debug.Log(Main.instance.movement.gridX + " " + gridY + " " + Main.instance.movement.y + " " + gridX);
        /*if (Main.instance.movement.gridX == gridX && Main.instance.movement.gridX == gridY)
        {
            
            return false;
        }*/
        if (landtype == "impassable")
        {
            return false;
        }
        bool land = getLandType(a);
        if (a.flyingMovement)
        {
            land = true;
        }

        return land;
    }

    public bool runEffects(string _type, moveModifiers a, Movement movement)
    {
        bool outbound = false;
        //print(effects.Count + " e count");
        foreach(Effects e in effects)
        {
            print(e);
            if (!e)
            {
                eRemove.Add(e);
            }
            else if (e.type() == _type)
            {
                if (e.runEffect(graph, gridX, gridY, a, movement))
                {
                    outbound = true;
                }
            }
            else if(_type == "init")
            {
                //probably shouldn't be passing the movement for init
                //also, theres a good change it will be null for init anyway
                if (e.initEffect(graph, gridX, gridY, a, movement))
                {
                    outbound = true;
                }
            }
        }
        runCleanUp();
        return outbound;
    }

    void runCleanUp()
    {
        foreach (Effects r in eRemove)
        {
            effects.Remove(r);
            Destroy(r);
        }
        foreach (Effects a in eAdd)
        {
            effects.Add(a);
        }
        eRemove = new List<Effects>();
        eAdd = new List<Effects>();
    }


    public void removeEffects(Effects a)
    {
        //adding and removing to lists while in use screws things up
        //so these lists are run after the main list is done iterating through effects
        eRemove.Add(a);
    }

    public void addEffects(Effects a)
    {
        eAdd.Add(a);
    }

    public void interact(moveModifiers player, Game_Main main)
    {
        if (npc)
        {
            //npc.GetComponent<Npc>().interact(-1);
            //npc.GetComponent<Charater>().interact(this);
        }
        else {

            runEffects("interact", player, null);
        }
    }


    /// <summary>
    /// IEnumerator that changes color/alpha of tile over time
    /// 
    /// </summary>
    public void startCo()
    {
        if (!coRunning) { StartCoroutine("co"); }
    }

    IEnumerator co()
    {
        sR = GetComponent<SpriteRenderer>();
        float a = sR.color.a;
        float r = sR.color.r;
        float g = sR.color.g;
        float b = sR.color.b;
        //print(a + " " + b + " " + " " + r + " " + g + " " + (a != alpha));
        while (a != alpha || r != red || b != blue || g != green)
        {

            a = colSwitch(a, alpha);
            r = colSwitch(r, red);
            r = colSwitch(g, green);
            r = colSwitch(g, blue);
            sR.color = new Color(r, g, b, a);
            yield return null;
        }
        //print("co done");
        coRunning = false;
    }

    float colSwitch(float a, float b)
    {
        if (greaterthan(a, b))
        {
            a = colUp(a, b, colorSpeed, true);
        }
        else if (greaterthan(b, a))
        {
            a = colUp(a, b, colorSpeed * -1, false);
        }
        return a;
    } 

    float colUp(float a, float b, int s, bool updown)
    {
        //adds/subtracks from a, then checks if it overshoots and sets to b;
        a -= s * Time.deltaTime;
        if(updown)
        {
            if (greaterthan(b, a))
            {
                a = b;
            }
        } else
        {
            if (greaterthan(a, b))
            {
                a = b;
            }
        }
        return a;
    }

    public bool greaterthan(float a, float b)
    {
       
        if(a > b)
        {
            return true;
        }
        return false;
    }

}