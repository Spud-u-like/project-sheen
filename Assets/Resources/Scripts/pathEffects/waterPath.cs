﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waterPath : MonoBehaviour {

    public Graph graph;
    public Node node;
    bool recheck = false;

    // Use this for initialization
    public void StartUp () {
        node = this.GetComponent<Node>();
        graph = node.graph;
    }
	
	// Update is called once per frame
    public void reCheck()
    {
        foreach (Transform child in transform)
        {
            if (child.tag == "boarders")
            {
                GameObject.Destroy(child.gameObject);
            }
            
        }
        //print(node.gridX + " " + node.gridY + " Recheck");
        recheck = true;
        whatTile(false);
    }

	public void whatTile (bool reCheckNeighbours) {

        List<Node> nodeList = new List<Node>();

        int gridX = node.gridX;
        int gridY = node.gridY;

        Node topLeftNode = graph.getNode(gridY-1, gridX - 1);

        //print((gridX - 1) + " " + (gridY - 1));
        //print(node);
        //print(graph);
        //print(topLeftNode);
        if (topLeftNode)
        {
            if (checkCorner(topLeftNode, graph.getNode(topLeftNode.gridY + 1, topLeftNode.gridX), graph.getNode(topLeftNode.gridY, topLeftNode.gridX + 1), "TopLeftCorner"))
            {
                nodeList.Add(topLeftNode);
            }
        }

        Node topRightNode = graph.getNode(gridY + 1, gridX - 1);

        if (topRightNode)
        {
            if (checkCorner(topRightNode, graph.getNode(topRightNode.gridY - 1, topRightNode.gridX), graph.getNode(topRightNode.gridY, topRightNode.gridX + 1), "TopRightCorner"))
            {
                nodeList.Add(topRightNode);
            }
        }

        Node bottomLeftNode = graph.getNode(gridY - 1, gridX + 1);
        if (bottomLeftNode)
        {
            if (checkCorner(bottomLeftNode, graph.getNode(bottomLeftNode.gridY, bottomLeftNode.gridX - 1), graph.getNode(bottomLeftNode.gridY + 1, bottomLeftNode.gridX), "BottomLeftCorner"))
            {
                nodeList.Add(bottomLeftNode);
            }
        }
        
        Node bottomRightNode = graph.getNode(gridY + 1, gridX + 1);
        if (bottomRightNode)
        {
            if (checkCorner(bottomRightNode, graph.getNode(bottomRightNode.gridY - 1, bottomRightNode.gridX), graph.getNode(bottomRightNode.gridY, bottomRightNode.gridX - 1), "BottomRightCorner"))
            {
                nodeList.Add(bottomRightNode);
            }
        }
        
        Node topMidNode = graph.getNode(gridY, gridX - 1);
        if(checkSides(topMidNode, "TopLong"))
        {
            nodeList.Add(topMidNode);
        }

        Node bottomMidNode = graph.getNode(gridY, gridX + 1) ;
        if(checkSides(bottomMidNode, "BottomLong"))
        {
            nodeList.Add(bottomMidNode);
        }
        
        Node leftMidNode = graph.getNode(gridY -1, gridX);
        if(checkSides(leftMidNode, "LeftLong"))
        {
            nodeList.Add(leftMidNode);
        }
        
        Node rightMidNode = graph.getNode(gridY + 1, gridX);
        //print(rightMidNode.gridX + " " + rightMidNode.gridY + " Gird Pos");
        if(checkSides(rightMidNode, "RightLong"))
        {
            nodeList.Add(rightMidNode);
        }

        if(reCheckNeighbours)
        {
            //print(nodeList.Count);
            foreach (Node n in nodeList)
            {
                Transform[] ts = n.gameObject.GetComponentsInChildren<Transform>();
                foreach (Transform t in ts)
                {
                    //print(t.name);
                    if (t.name == "waterPath")
                    {
                        
                        t.gameObject.GetComponent<waterPath>().reCheck();
                    }
                }

            }
        }

    }

    bool checkCorner(Node n, Node a, Node b, string output)
    {
        if (n)
        {
            if (n.tag != "water")
            {
                //also check 
                if(!b || !a)
                {
                    return true;
                }
                //Node down = graph.getNode(n.gridX, n.gridY + 1);
                //Node right = graph.getNode(n.gridX + 1, n.gridY);
                //print("stats");
                //print(n.tag + " " + n.name + " X:" + n.gridX + " Y:" + n.gridY);
                //print(a.tag + " " + a.name + " X:" + a.gridX + " Y:" + a.gridY);
                //print(b.tag + " " + b.name + " X:" + b.gridX + " Y:" + b.gridY);
                if (recheck)
                {
                    //a.transform.position = new Vector3(a.transform.position.x + 1, a.transform.position.y + 1, a.transform.position.z);
                    //b.transform.position = new Vector3(b.transform.position.x + 1, b.transform.position.y + 1, b.transform.position.z);
                }

                if (a.gameObject.tag != "water" && b.gameObject.tag != "water")
                {
                    //print("curvedOut");
                    addThisOne(n, output + "CurveIn");
                }
                if(a.gameObject.tag == "water" && b.gameObject.tag == "water")
                {
                    //print("curvedOut");
                    addThisOne(n, output + "CurveOut");
                }
            }
            return true;
        }
        return false;
    }

    bool checkSides(Node n, string output)
    {
        if (n)
        {
            //print(n.name + " " + n.gridX + " " + n.gridY + " Tag On Node:" + n.tag + " Tag On Object:" + n.gameObject.tag);
            if (n.tag != "water")
            {
                //print("am working");
                addThisOne(n, output);
            }
            return true;
        }
        return false;
    }

    void addThisOne(Node n, string type)
    {
        GameObject cube = GameObject.Instantiate((GameObject)Resources.Load("Tiles/autoBoarders/" + dirtOrGrass(n) + type));
        cube.transform.position = new Vector3(transform.position.x + cube.transform.position.x, transform.position.y + cube.transform.position.y, 0);
        cube.transform.parent = transform;
        cube.GetComponent<SpriteRenderer>().sortingOrder = 1 + transform.childCount;
    }

    string dirtOrGrass(Node n)
    {
        if (n.tag == "dirt")
        {
            return "dirt";
        } else
        {
            return "grass";
        }
   }
}
