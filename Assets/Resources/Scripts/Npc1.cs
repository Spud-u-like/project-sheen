﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class Npc1 : MonoBehaviour {
/*
    //npc dialog
    //npc movement
    //npc move player
    //so you need a system to move npc's, read plot, hand over times and the like
    //Player controls taken away? Scan ahead, see if there is an exit point
    //no exit point? cant take controls away!
    public int gridX;
    public int gridY;
    public int storyPos = 0;
    public int totalPos = 0;
    public int[,] movementPattern;
    string direction = "";
    string dir_animation = "";
    public List<Action> actionList;
    public TextAsset npcJSON;
    public float offset = 0.3f;

    public bool moving = false;
    Graph graph;
    Vector2 dest;
    Search search;
    int point = 0;
    public float speed = 8.0f;
    int target_x;
    int target_y;


    public void Start()
    {
        if (Main.instance)
        {
            graph = Main.instance.graph;
        }
    }


    public void interact (int _storyPos) {

        if(_storyPos > 0)
        {
            storyPos = _storyPos;
        }
        Main.instance.npc = this;
        var N = JSON.Parse(npcJSON.text);
        string action = N["action"][storyPos]["type"];
        print(N["action"].Count);

        int a = N["action"].Count;
        totalPos = N["action"].Count;
        if (action == "text")
        {
            Main.instance.uiHandler.goText(N["action"][storyPos]["text"]);
            Main.instance.gamestate = "text";
            if (N["action"][storyPos]["advance"])
            {

            }
            storyPos++;
            return;
        }
        if (action == "SFX")
        {
            GameObject sound = new GameObject();
            sound.name = "SFX";
            sound.AddComponent<AudioSource>();
            string clipName = N["action"][storyPos]["name"];
            print(clipName);
            AudioClip clip1 = (AudioClip)Resources.Load("SFX/"+ clipName);
            sound.GetComponent<AudioSource>().clip = clip1;
            sound.GetComponent<AudioSource>().Play();
            storyPos++;
            return;
        }
        if (action == "move")
        {
            search = new Search(graph, Main.instance, true);
            print(gridY + " " + gridX);
            target_x = gridX + N["action"][storyPos]["x"][0];
            target_y = gridY + N["action"][storyPos]["y"][0];
            moveModifiers MM = new moveModifiers();
            MM = GetComponent<moveModifiers>().copyVals(MM);
            MM = movementOverrides(MM, N);

            search.Start(graph.nodes[gridY, gridX], graph.nodes[gridY + N["action"][storyPos]["y"][0], gridX + N["action"][storyPos]["x"][0]], MM);
            dest = search.finished_path[point].transform.position;
            graph.getNode(gridX, gridY).npc = null;
            graph.getNode(target_x, target_y).npc = this.gameObject;
            moving = true;
            StartCoroutine("co");
            storyPos++;
        }
        if (action == "orMove")
        {
            search = new Search(graph, Main.instance);
            print(gridY + " " + gridX);
            target_x = gridX + N["action"][storyPos]["x"][0];
            target_y = gridY + N["action"][storyPos]["y"][0];
            search.Start(graph.nodes[gridY, gridX], graph.nodes[target_x, target_y], this.gameObject.GetComponent<moveModifiers>());
            dest = search.finished_path[point].transform.position;
            moving = true;
            StartCoroutine("co");
            storyPos++;
        }
        if (action == "exit")
        {
            print("exiting");
            storyPos = N["action"][storyPos]["value"];
            
            Main.instance.npc = null;
            Main.instance.gamestate = "play";
            print("storyPos" + storyPos);
            return;
        }
    }

    public bool traveling()
    {
        //graph.getNode(gridX + h, y + v).transform.position
        if (moving)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(dest.x, dest.y + offset), Time.deltaTime * (speed));
            if (new Vector2(transform.position.x, transform.position.y) == new Vector2(dest.x, dest.y + offset))
            {
                if (point < search.finished_path.Count - 1)
                {
                    point++;
                    dest = search.finished_path[point].transform.position;
                }
                else {
                    moving = false;
                    gridX = target_x;
                    gridY = target_y;
                }
            }
        }
        return moving;
    }

    IEnumerator co()
    {
        while(traveling())
        {
            yield return null;
        }
        interact(-1);
    }

    private int[] getInts(int[] st, SimpleJSON.JSONNode N, string param1, string param2)
    {
        for (int j = 0; j < st.GetLength(0); j++)
        {
            st[j] = N["action"][param1][param2][j];
        }
        return st;
    }

    private moveModifiers movementOverrides (moveModifiers m, SimpleJSON.JSONNode N)
    {
        string t = N["action"][storyPos]["or"];
        if(t == "roof")
        {
            m.ignoreRoof = true;
        }
        return m;
    }


    public GameObject lookUpItem(string a)
    {
        //checks player inventory for specific item
        return new GameObject();
    }

    public GameObject lookUpCharacter(string a)
    {
        //checks player roster for specific Character
        //note for furture, if char has died, dont remove from roster
        //mark cause of death and mark as dead, not veiwerable to player
        return new GameObject();
    }

    public bool lookUpFlag(string a)
    {
        //checks player roster for specific Character
        //note for furture, if char has died, dont remove from roster
        //mark cause of death and mark as dead, not veiwerable to player
        return true;
    }


    void Update () {
		
	}
    */
}
