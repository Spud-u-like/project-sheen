﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Main : MonoBehaviour
{
    public GameObject listOfLines;
    public GameObject currentTarget;

    public Graph graph;
    public NPCLoader loader;

    public MapBuilder mapBuilder = new MapBuilder();

    public repopDropdown repopDropdown;
    public virtual void finishBuildingMap()
    {
        //Debug.Log("The fruit has been chopped.");
    }

    public void changeTarget(GameObject t)
    {     
        currentTarget = t;
        listOfLines.SetActive(true);
        foreach (Transform c in listOfLines.transform)
        {
            lineManager lm = c.GetComponent<lineManager>();
            if (lm)
            {
                lm.target = t;
                lm.makeLines();
            }
        }
    }
    public void buildNewMap(string map, bool preserveNPC, string mapType, Main m)
    {

        //BushTestMap testMap = new BushTestMap();
        //MapBuilder newmap = new MapBuilder();
        if (!mapBuilder.init(map))
        {
            Debug.LogError("Failed to load level");
        }
        else
        {
            if (graph != null)
            {
                Destroy(graph.map);
                Destroy(graph.NPCS);
            }
            //newmap.buildMap(map, preserveNPC, mapType, m);
            mapBuilder.buildMapInit(map, preserveNPC, mapType, m);
        }
    }

    public virtual void finishBuild(Graph _graph)
    {
        graph = _graph;
        Destroy(graph.preloader);
        loader = new NPCLoader();
        loader.init(graph, this);
        loader.NPCBuildCount();
    }
}
