﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethods {

    public static bool hasNode(this Transform t)
    {
        if (t.gameObject.GetComponent<Node>())
        {
            return true;
        }
        return false;
    }

	public static int childCountIgnoringNodeless(this Transform t)
    {
        int x = 0;
        foreach (Transform child in t)
        {
            if (child.GetComponent<Node>())
            {
                
                x++;
            }
        }
        return x;
    }

	public static GameObject getChildIgnoringNodeless(this Transform t, int pos)
    {
		Node[] l = t.GetComponentsInChildren<Node>();
		if (pos+1 >= l.Length)
		{
			return null;
		}
        //Debug.Log(pos + 1 >= l.Length);
        //Debug.Log(l.Length + " " + pos + " " + t.gameObject);
		return l[pos+1].gameObject;
	}

    public static void delAllChildren(this Transform t)
    {
        foreach (Transform child in t)
        {
            GameObject.Destroy(child.gameObject);
        }
    }

    public static void delAllChildrenWithoutTag(this Transform t, string tag)
    {
        foreach (Transform child in t)
        {
            if (child.tag != tag)
            {
                GameObject.Destroy(child.gameObject);
            }
        }
    }

}
