﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class textAppear : MonoBehaviour {

    int remainingWords = 0;
    string closetags;
    int counter = 0;
    int startingcounter;
    string strCurrent;
    string strComplete;
    string openingTags = "";
    float speed = 0.01F;
    float defaultSpeed = 0.01F;
    float nextText = 0f;
    int charCount = 0;
    List<string> textChunks;
    public bool textGo = false;
    public bool cont = false;
    int maxPerBox = 120;
    int maxPerLine = 40;
    int linecounter = 0;
    public bool instant = false;

	void Update () {

        if (textGo)
        {
            if (instant)
            {
                while (!AnimateText(strCurrent))
                {
                    textGo = false;
                    cont = true;
                    instant = false;
                }
            } else {
                if (nextText > 0.0001f)
                {
                    if (AnimateText(strCurrent))
                    {
                        textGo = false;
                        cont = true;
                    }
                    nextText = 0f;
                }
                nextText += speed * Time.deltaTime;
            }
        }
    }

    public bool textContinue()
    {
        if (counter != strComplete.Length)
        {
            if (textGo)
            {
                instant = true;

            } else {
                startingcounter = counter;
                strCurrent = "" + openingTags;
                openingTags = "";
                textGo = true;
            }
            return true;
        }
        cont = false;
        return false;
    }

    public void textStart(string t)
    {
       
        clear();
        strComplete = t;
        textGo = true;
    }


    bool AnimateText(string str)
    {
        if (counter < strComplete.Length)
        {
            string temp = "";
            int wordSize = 0;

            temp += strComplete[counter];
            if (charCount == 0)
            {
                if (temp == " ")
                {
                    //ignores leading spaces when starting a paragraph 
                    counter++;
                    return false;
                }
                temp = temp.ToUpper();
            }
            if (temp == "@")
            {
                string t = strComplete.Substring(counter + 1);
                t = t.Split("@"[0])[0];
                counter += t.Length+2;
                string[] elements = t.Split(","[0]);
                //print(t);
                openingTags = lookup(elements);
                str += openingTags;
                //print("elements " + elements);
                remainingWords = int.Parse(elements[elements.Length-1]) + 1;
                temp = " ";
            }
            else
            {
                str += temp;
            }
            if (remainingWords > 0)
            {
                str = closeTagHandler(temp, str);
            }

            //print(str + closetags);
            GetComponent<Text>().text = str+ closetags;
            charCount++;
            strCurrent = str;
            
            if(temp == " " && counter+1 < strComplete.Length)
            {
                wordSize = getNextWord(strComplete.Substring(counter + 1));
            }
            if (charCount+ wordSize> maxPerBox)
            {
                //box is full? wait for next box
                textChunks.Add(str);
                str = "";
                charCount = 0;
                remainingWords++;
                linecounter = 0;
                return true;
            }
            else if(wordSize + linecounter > maxPerLine)
            {
                charCount += maxPerLine - linecounter;
                linecounter = 0;
                str += "\n";
                GetComponent<Text>().text = str + closetags;
                strCurrent = str;
            }
            if(linecounter > maxPerLine)
            {
                linecounter = 0;
            }
            else
            {
                linecounter++;
            }
            //print(linecounter);
            counter++;
            return false;
        }
        return true;
    }

    int getNextWord(string t)
    {
        string temp = t.Split(" "[0])[0];
        //print(temp.Substring(0,1) + " temp length " + temp.Length + " temp name " + temp);
        if (temp.Substring(0,1) == "@")
        {
            return 0;
        }
        return temp.Length;
    }

    string closeTagHandler(string temp, string str)
    {
        if (temp == " ")
        {
            remainingWords--;
        }
        if (remainingWords == 0)
        {
            str += closetags;
            closetags = "";
            speed = defaultSpeed;
            openingTags = "";
        }
        return str;
    }

    string lookup(string[] elements)
    {
        closetags = "";
        string outString = "";
        for(int i =0; i<elements.Length-1; i++)
        {
            string[] eParts = elements[i].Split(":"[0]);
            switch (eParts[0])
            {
                case "size":
                    outString += "<size=" + eParts[1] + ">";
                    closetags = "</size>"+ closetags;
                    break;
                case "ital":
                    outString += "<i>";
                    closetags = "</i>" + closetags;
                    break;
                case "bold":
                    outString += "<b>";
                    closetags = "</b>" + closetags;
                    break;
                case "color":
                    outString += "<color=" + eParts[1] + ">";
                    closetags = "</color>" + closetags;
                    break;
                case "speed":
                    speed = float.Parse(eParts[1]);
                    break;
                default:
                    break;
            }
        }
        
        return outString;
    }

    public void clear()
    {
        counter = 0;
        charCount = 0;
        linecounter = 0;
        strCurrent = "";
        closetags = "";
        openingTags = "";
        textChunks = new List<string>();
        GetComponent<Text>().text = "";
    }
    /* cheat sheet
          \n    New line
      \t    Tab
      \v    Vertical Tab
      \b    Backspace
      \r    Carriage return
      \f    Formfeed
      \\    Backslash
      \'    Single quotation mark
      \"    Double quotation mark
      \d    Octal
      \xd    Hexadecimal
      \ud    Unicode character
    */
}
