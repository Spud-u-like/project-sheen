﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using UnityEngine.Rendering;


public class Graph
{

    public int rows = 0;
    public int cols = 0;
    public Node[,] nodes;
    public List<Npc> npc;
    public GameObject mapGroup;
    public Vector3 offset;

    public int startX;
    public int startY;
    public GameObject map;
    public GameObject NPCS;

    public bool coRunning = false;

    int[,] grid;
    public int[,][] rawGrid;
    GameObject[] tiles;
    public string mapName = "Map";
    bool preserveNPC = false;
    string Maptype = "normal";
    bool DevelopMode = false;

    public bool cameraLock = true;
    public float cameraXPos = 6.39f;
    public float cameraYPos = -7.16f;

    public GameObject preloader;

    public Graph(string[] tileNames ,int[,][] _grid, GameObject _preloader, int _startX, int _startY, string _mapName = "Map", bool _preserveNPC = false, string _Maptype = "normal", bool usingCo = false, bool _cameraLock = false)
    {
        //Debug.Log("Camera Lock: " +_cameraLock);

        try
        {
            preloader = _preloader;
            rawGrid = _grid;
            grid = new int[_grid.GetLength(0), _grid.GetLength(1)];
            //tiles = _tiles;
            startX = _startX;
            startY = _startY;
            mapName = _mapName;
            preserveNPC = _preserveNPC;
            Maptype = _Maptype;
            if (Maptype != "normal") { DevelopMode = true; }
            cameraLock = _cameraLock;
            if (usingCo)
            {
                return;
            }
            //Debug.Log("How many? " + node.label);
            int x_pos = 0;
            int y_pos = 0;
            int max_X = _grid.GetLength(1);
            float sizeup_a = 0;
            float sizeup_b = 0;
            rows = _grid.GetLength(0);
            cols = _grid.GetLength(1);
            Debug.Log(rows + " " + cols);
            nodes = new Node[_grid.GetLength(0), _grid.GetLength(1)];
            map = new GameObject();
            map.name = mapName;
            if (!preserveNPC)
            {
                NPCS = new GameObject();
                NPCS.name = "NPCS";
            }

            for (int r = 0; r < rows; r++)
            {
                for (int c = 0; c < cols; c++)
                {
                    //var node = nodes[cols * r + c];
                    //Node node = new Node(grid[r, c], r, c);

                    //Debug.Log(sizeup_a +" " + x_pos + " " + x_pos* sizeup_a);
                    sizeup_a = sizeup_a * x_pos;
                    sizeup_b = sizeup_b * -y_pos;
                    offset = new Vector2(sizeup_a, sizeup_b);

                    //Debug.Log(r + " " + c);
                    //Debug.Log(grid[r, c]);
                    //Debug.Log(tiles[_grid[r, c][0]]);
                    grid[r, c] = _grid[r, c][0];
                    //GameObject tileName = tiles[_grid[r, c][0]];
                    GameObject tileName = null;
                    //Debug.Log("preloader " + _grid[r, c][0]);
                    //Debug.Log("preloader " + _grid[r, c][0]);
                    if (preloader)
                    {
                        if (_grid[r, c][0] < tileNames.Length && _grid[r, c][0] != -1)
                        {
                            //Debug.Log("preloader " + preloader);
                            tileName = findTile(tileNames[_grid[r, c][0]], preloader);
                            if (tileName)
                            {
                                tileName.transform.localScale = new Vector3(5, 5, tileName.transform.localScale.z);
                            }
                        }
                    }
                    if (!tileName)
                    {
                        tileName = (GameObject)Resources.Load("Tiles(Unsorted)/blank", typeof(GameObject));

                    }
                    GameObject cube = GameObject.Instantiate(tileName);

                    //XY flips
                    flipXY(cube, _grid[r, c][1]);
                    ///////////

                    cube.transform.position = new Vector2(offset.x, offset.y);
                    Vector2 sizeup = cube.GetComponent<Renderer>().bounds.size;
                    cube.transform.parent = map.transform;
                    //cube.gameObject.AddComponent<Node>();
                    Node node = cube.GetComponent<Node>();
                    node.Node_int(r, c, cube, this, DevelopMode);
                    node.label = r.ToString() + "," + c.ToString();
                    nodes[r, c] = node;
                    cube.name = cube.name.Replace("(Clone)", "");
                    cube.GetComponent<SpriteRenderer>().sortingOrder = r;
                    //cube.GetComponent<SpriteRenderer>().sortingLayerName = "Ground";
                    //SortingGroup sg = cube.AddComponent<SortingGroup>();
                    //sg.sortingOrder = r;

                    if (Maptype == "MapMaker")
                    {
                        addMouseDown(cube, false);
                    }

                    if (Maptype == "NPCMap")
                    {
                        addMouseDownNPCMaker(cube, false);
                    }

                    //-0.01f added because unity has SUCH as hard time lining up the sprite just right
                    //changed it back later because, for some reason, now it doesn't
                    sizeup_a = sizeup.x; //- 0.01f;
                    sizeup_b = sizeup.y; //- 0.01f;
                    x_pos++;

                    if (x_pos > max_X - 1)
                    {
                        x_pos = 0;
                        y_pos++;
                    }

                }
            }
            afterLoadFeatures();
            

        }
        catch (Exception e)
        {
            Debug.LogWarning(e);
            Debug.LogWarning("Failed to load Graph");
        }
    }

    public GameObject findTile(string name, GameObject preloader)
    {
        string[] parts = name.Split('/');
        if(parts[0] == "SYSTEM")
        {
            return systemTiles(parts[1]);
        }
        foreach (Transform trans in preloader.transform)

        {
            //Debug.Log(parts.Length + " " + name + " " + trans.name);

            if (parts[0] == trans.name)
            {
                foreach (Transform child in trans.transform)
                {
                    //Debug.Log(parts.Length + " " + name + " " + child.name);
                    if (parts[1] == child.name)
                    {
                        return child.gameObject;
                    }
                }
            }
        }
        return null;
    }

    GameObject systemTiles(String name)
    {
        GameObject instance = Resources.Load("Tiles/Additional/" + name, typeof(GameObject)) as GameObject;
        return instance;
    }


    public IEnumerator co(MM_Main m)
    {
        int x_pos = 0;
        int y_pos = 0;
        int max_X = grid.GetLength(1);
        float sizeup_a = 0;
        float sizeup_b = 0;
        rows = grid.GetLength(0);
        cols = grid.GetLength(1);
        Debug.Log(rows + " " + cols);
        nodes = new Node[grid.GetLength(0), grid.GetLength(1)];
        map = new GameObject();
        preloader = new GameObject();
        preloader.name = "preloader";
        preloader.SetActive(false);
        map.name = mapName;
        if (!preserveNPC)
        {
            NPCS = new GameObject();
            NPCS.name = "NPCS";
        }

        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < cols; c++)
            {
                //var node = nodes[cols * r + c];
                //Node node = new Node(grid[r, c], r, c);


                sizeup_a = sizeup_a * x_pos;
                sizeup_b = sizeup_b * -y_pos;
                offset = new Vector2(sizeup_a, sizeup_b);
                //Debug.Log(r + " " + c);
                //Debug.Log(grid[r, c]);
                //Debug.Log(tiles[grid[r, c]]);

                //GameObject cube = GameObject.Instantiate(tiles[grid[r, c]]);

                GameObject tileName = (GameObject)Resources.Load("Tiles(Unsorted)/blank", typeof(GameObject));
                GameObject cube = GameObject.Instantiate(tileName);

                cube.transform.position = new Vector2(offset.x, offset.y);
                Vector2 sizeup = cube.GetComponent<Renderer>().bounds.size;
                cube.transform.parent = map.transform;
                //cube.gameObject.AddComponent<Node>();
                Node node = cube.GetComponent<Node>();
                node.Node_int(r, c, cube, this, DevelopMode);
                node.label = r.ToString() + "," + c.ToString();
                nodes[r, c] = node;
                cube.name = cube.name.Replace("(Clone)", "");
                //cube.GetComponent<SpriteRenderer>().sortingOrder = r;
                SortingGroup sg = cube.AddComponent<SortingGroup>();
                sg.sortingOrder = r;
                

                if (Maptype == "MapMaker")
                {
                    addMouseDown(cube, false);
                }

                if (Maptype == "NPCMap")
                {
                    addMouseDownNPCMaker(cube, false);
                }

                //-0.01f added because unity has SUCH as hard time lining up the sprite just right
                sizeup_a = sizeup.x - 0.01f;
                sizeup_b = sizeup.y - 0.01f;
                x_pos++;

                if (x_pos > max_X - 1)
                {
                    x_pos = 0;
                    y_pos++;
                }
                
            }
            yield return null;
        }
        afterLoadFeatures();
        m.coDone();
        coRunning = false;
    }

    void afterLoadFeatures()
    {
        foreach (Node n in nodes)
        {
            if (n.gameObject.GetComponent<waterPath>() != null)
            {
                n.gameObject.GetComponent<waterPath>().StartUp();
            }
        }
        foreach (Node n in nodes)
        {
            if (n.gameObject.GetComponent<waterPath>() != null)
            {
                n.gameObject.GetComponent<waterPath>().whatTile(true);
            }
        }        
    }

    void flipXY(GameObject cube, int flips)
    {
        SpriteRenderer sr = cube.GetComponent<SpriteRenderer>();
        if (flips == 1 || flips == 3)
        {
            sr.flipX = true;
        }
        if (flips == 2 || flips == 3)
        {
            sr.flipY = true;
        }
    }


    public Graph flip_hoz(int[] _x, int[] _y, Graph outbound)
    {
        for (int i = 0; i < _x.Length; i++)
        {
            Node n = outbound.getNode(_x[i], _y[i]);
            n.GetComponent<SpriteRenderer>().flipX = true;
            SpriteRenderer[] ts = n.GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer t in ts)
            {
                //Debug.Log("   asdsa " + t.gameObject.transform.GetComponent<Node>());
                if (t.gameObject.tag != "roof")
                {
                    t.flipX = true;
                }
            }
        }
        return outbound;
    }

    public Graph flip_tiles(int[,] _x, int[,] _y, Graph outbound)
    {
        for (int i = 0; i < _x.GetLength(0); i++)
        {
            for (int k = 0; k < _x.GetLength(1); k++)
            {
                if (_x[i, k] == 1)
                {
                    Node n = outbound.getNode(i, k);
                    n.GetComponent<SpriteRenderer>().flipX = true;
                    SpriteRenderer[] ts = n.GetComponentsInChildren<SpriteRenderer>();
                    foreach (SpriteRenderer t in ts)
                    {
                        //Debug.Log("   asdsa " + t.gameObject.transform.GetComponent<Node>());
                        if (t.gameObject.tag != "roof")
                        {
                            t.flipX = true;
                        }
                    }
                }
                if (_y[i, k] == 1)
                {
                    Node n = outbound.getNode(i, k);
                    n.GetComponent<SpriteRenderer>().flipY = true;
                    SpriteRenderer[] ts = n.GetComponentsInChildren<SpriteRenderer>();
                    foreach (SpriteRenderer t in ts)
                    {
                        //Debug.Log("   asdsa " + t.gameObject.transform.GetComponent<Node>());
                        if (t.gameObject.tag != "roof")
                        {
                            t.flipY = true;
                        }
                    }
                }

            }
        }
                
        return outbound;
    }

    public Graph gen_layer(string[] tileNames, int[,][] layer, GameObject preloader, Graph outbound, int layerNum, string Maptype = "normal")
    {
        if(layer.GetLength(0) == 0 && layer.GetLength(1) ==0)
        {
            return outbound;
        }
        for (int r = 0; r < layer.GetLength(0); r++)
        {
            for (int c = 0; c < layer.GetLength(1); c++)
            {
               //Debug.Log(roof.GetLength(0) + " " + roof.GetLength(1) + " " + roof[r, c].Length);
                if (layer[r, c][0] != 0)
                {
                    //Debug.Log(c + " " + r);
                    try
                    {
                        GameObject tileName = null;
                        //Debug.Log("preloader " + _grid[r, c][0]);
                        //Debug.Log("preloader " + tileNames.Length);
                        if (preloader)
                        {
                            if (layer[r, c][0] - 1 < tileNames.Length)
                            {
                                //Debug.Log(tileNames[layer[r, c][0] - 1]);
                                tileName = findTile(tileNames[layer[r, c][0] - 1], preloader);
                                if (tileName)
                                {
                                    tileName.transform.localScale = new Vector3(5, 5, tileName.transform.localScale.z);
                                }
                            }
                        }
                        if (!tileName)
                        {
                            //cube = (GameObject)Resources.Load("Tiles(Unsorted)/blank", typeof(GameObject));
                            throw(new Exception ("tile not found " + tileNames[layer[r, c][0] - 1]));
                        }
                        GameObject cube = GameObject.Instantiate(tileName);
                        //cube = GameObject.Instantiate(layerTiles[layer[r, c][0] - 1]);
                        Transform t = outbound.getNode(c, r).gameObject.transform;
                        //the y add is for sprites that are not the same height as regular blocks due to transparency issues

                        float xAdditional = 0;
                        float yAdditional = 0;
                        //Debug.Log("Layer size " + layer[r, c].Length) ;
                        if (layer[r, c].Length > 2)
                        {
                            int xInt = layer[r, c][2];
                            
                            if (xInt != 0)
                            {
                                xAdditional = (float)xInt / 100;
                            }

                            int yInt = layer[r, c][3];
                            
                            if (yInt != 0)
                            {
                                yAdditional = (float)yInt / 100;
                            }

                            Debug.Log("Layer divsion  " + (float)xInt / 100 + " " + xInt);
                            Debug.Log("Layer additonal  " + layer[r, c][0] + " " + layer[r, c][1] + " " + layer[r, c][2] + " " + layer[r, c][3] + " " + layer[r, c][4]);
                            cube.GetComponent<Node>().offsetX = xInt;
                            cube.GetComponent<Node>().offsetY = yInt;
                        }

                        cube.transform.position = new Vector2(t.position.x + cube.transform.position.x + xAdditional, t.position.y + cube.transform.position.y + yAdditional);
                        cube.transform.parent = t;
                        cube.GetComponent<Node>().gridX = t.GetComponent<Node>().gridX;
                        cube.GetComponent<Node>().gridY = t.GetComponent<Node>().gridY;
                        cube.name = cube.name.Replace("(Clone)", "");
                        if (Maptype == "MapMaker")
                        {
                            addMouseDown(cube, true);
                        }
                        if (Maptype == "NPCMap")
                        {
                            addMouseDownNPCMaker(cube, true);
                        }
                        flipXY(cube, layer[r, c][1]);
                        //Debug.Log("Am I a roof? " +  roof[r, c][4] + " " + (roof[r, c][4] == 1));
                        cube.GetComponent<Node>().isRoof = (layer[r, c][4] == 1);
                        //cube.GetComponent<SpriteRenderer>().sortingOrder = t.GetComponent<SpriteRenderer>().sortingOrder + 1;
                        SortingGroup sg = cube.AddComponent<SortingGroup>();
                        sg.sortingOrder = t.GetComponent<SpriteRenderer>().sortingOrder + 1 + layerNum;
                    }
                    catch (Exception e)
                    {
                        Debug.Log("error: " + e);
                        //Debug.Log(layerTiles[layer[r, c][0] - 1] + " could not be loaded");
                    }
                }
            }

        }
        return outbound;
    }

    public Graph gen_npc(int[,] npc, GameObject[] roof_tiles, Graph outbound)
    {

        for (int r = 0; r < npc.GetLength(0); r++)
        {
            for (int c = 0; c < npc.GetLength(1); c++)
            {
                if (npc[r, c] != 0)
                {
                    GameObject cube = GameObject.Instantiate(roof_tiles[npc[r, c] - 1]);
                    Transform t = outbound.getNode(c, r).gameObject.transform;
                    //the y add is for sprites that are not the same height as regular blocks due to transparency issues
                    cube.transform.position = new Vector2(t.position.x, t.position.y + cube.transform.position.y);
                    //pull dialog for this char based on postion in array and cutscene system
                }
            }

        }
        return outbound;
    }

    /*public Graph add_effects_start(int[,][] tiles, string[] effect, Graph outbound)
    {

        for (int r = 0; r < tiles.GetLength(0); r++)
        {
            for (int c = 0; c < tiles.GetLength(1); c++)
            {
                if (tiles[r, c] != null)
                {
                    foreach (int e in tiles[r, c])
                    {
                        System.Type MyScriptType = System.Type.GetType(effect[e] + ",Assembly-CSharp");
                        getNode(c, r).gameObject.AddComponent(MyScriptType);
                        getNode(c, r).effects_init();
                    }
                }
            }

        }
        return outbound;
    }*/

    /*public Graph add_effects_start(int[][] types, string[] effect, Graph outbound, bool MM)
    {

        for (int r = 0; r < types.GetLength(0); r++)
        {
            Debug.Log(getNode(types[r][0], types[r][1]) + " " + types[r][0] + " " + types[r][1] + " " + types[r][2] + " ");
            if (getNode(types[r][0], types[r][1]) != null)
            {
                Debug.Log("got this far");
                for (int c = 2; c < types[r].Length; c++)
                {
                    Debug.Log(c);
                    System.Type MyScriptType = System.Type.GetType(effect[types[r][c]] + ",Assembly-CSharp");
                    getNode(types[r][0], types[r][1]).gameObject.AddComponent(MyScriptType);
                    if (!MM)
                    {
                        getNode(types[r][0], types[r][1]).effects_init();
                    }

                }
            }
        }
        return outbound;
    }*/


    public Graph add_effects_start(EffectJson[] e, Graph outbound, string Maptype)
    {

        for (int r = 0; r < e.Length; r++)
        {
            Node n = getNode(e[r].gridX, e[r].gridY);
            if(n)
            {
                //for (int j = 0; j < e[r].Length; j++)
                //{
                System.Type MyScriptType = System.Type.GetType(e[r].effectName + ",Assembly-CSharp");
                Effects effect = (Effects) n.gameObject.AddComponent(MyScriptType);
                //Debug.Log(e[r].variables.Length);
                //Debug.Log(e.Length);
                //Debug.Log(r);
                for (int k = 0; k < e[r].variables.Length; k++)
                    {
                        string variable = e[r].variables[k];
                        string data = e[r].data[k];
                        if (effect.GetType().GetField(variable).FieldType.Name == "String")
                        {
                            effect.GetType().GetField(variable).SetValue(effect, data);
                        }                   
                    }

                //}      
                n.effects_init();
            }
        }
        return outbound;
    }

    public Graph add_NPC_start(NpcHolder[] types, CharJsonHolder[] holder, Graph outbound)
    {
        Debug.Log(types.Length  + " " );

        for (int r = 0; r < types.GetLength(0); r++)
        {

            Debug.Log(types[r].sprite);
            GameObject npc = GameObject.Instantiate((GameObject)Resources.Load("Npc/"+ types[r].sprite));
                
            npc.gameObject.AddComponent<Npc>();
            npc.GetComponent<Npc>().npcJSON = holder[r].j;
            npc.GetComponent<Npc>().JSONName = types[r].c;
            Transform t = outbound.getNode(types[r].startX, types[r].startY).gameObject.transform;
            npc.transform.position = new Vector2(t.position.x, t.position.y + npc.transform.position.y);
            outbound.getNode(types[r].startX, types[r].startY).npc = npc;
            npc.transform.parent = NPCS.transform;
            npc.GetComponent<Npc>().gridX = outbound.getNode(types[r].startX, types[r].startY).gridY;
            npc.GetComponent<Npc>().gridY = outbound.getNode(types[r].startX, types[r].startY).gridX;
            npc.gameObject.AddComponent<moveModifiers>();
            npc.name = npc.name.Replace("(Clone)", "");

        }
        return outbound;
    }

    public Graph change_layer_type(int[,][] grid, string[] type, Graph outbound)
    {
        //return outbound;
        for (int r = 0; r < grid.GetLength(0); r++)
        {
            for (int c = 0; c < grid.GetLength(1); c++)
            {
                Node n = outbound.getNode(c, r);
                n.landtype = (type[grid[r, c][0]]);
                
                for(int i = 0; i< n.transform.childCountIgnoringNodeless(); i++)
                {
                    //Debug.Log(n.transform.childCountIgnoringNodeless() + " " + c + " " + r + " " + type[grid[r, c][i + 1]]);
                    Node node = n.transform.GetChild(i).GetComponent<Node>();
                    node.landtype = (type[grid[r, c][i + 1]]);
                    if (node.landtype == "Foreground")
                    {
                        //Debug.Log("Foreground WORKING!");
                        node.gameObject.GetComponent<SortingGroup>().sortingLayerName = "Foreground";
                    }
                }
                //Debug.Log(type[grid[r, c]] + " " + r + " " + c + " " + grid[r, c]);
                //if (grid[r, c] != 0)
                //{
                //Debug.Log(type[grid[r, c] - 1]);
                //    outbound.getNode(c, r).landtype = (type[grid[r, c] - 1]);
                //}
            }

        }
        return outbound;
    }

    public Node getNode(int x, int y)
    {

        if (x < 0 || x > nodes.GetLength(1) - 1)
        {
            //Debug.Log(nodes.GetLength(1) - 1 + " " + gridX);
            return null;
        }
        if (y < 0 || y > nodes.GetLength(0) - 1)
        {
            //Debug.Log(nodes.GetLength(0) - 1 + " " + y);
            return null;
        }
        //Debug.Log("gridX + h " + gridX + " " + y + " " + nodes.GetLength(1) + " " + nodes.GetLength(0));
        return nodes[y, x];
    }

    public void setNode(int x, int y, Node n)
    {

        if (x < 0 || x > nodes.GetLength(1) - 1)
        {
            //Debug.Log(nodes.GetLength(1) - 1 + " " + x);
            return;
        }
        if (y < 0 || y > nodes.GetLength(0) - 1)
        {
            //Debug.Log(nodes.GetLength(0) - 1 + " " + y);
            return;
        }
        //Debug.Log("gridX + h " + gridX + " " + y + " " + nodes.GetLength(1) + " " + nodes.GetLength(0));
        nodes[y, x] = n;
        return;
    }

    public List<Node> GetNeighbours(Node node, bool dblock = false)
    {
        //Debug.Log("How many? " + node.label);
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == 0 && y == 0)
                {
                    continue;
                }
                
                if(dblock)
                {
                    if(x == 1 || x == -1)
                    {
                        if(y == 1 || y == -1)
                        {
                            continue;
                        }
                    }
                }

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if (checkX >= 0 && checkX < rows && checkY >= 0 && checkY < cols)
                {
                    neighbours.Add(nodes[checkX, checkY]);
                }
            }
        }

        return neighbours;
    }

    public void addNpcs()
    {
        //get gameobject and add as npc
        //
    }

    public void runEffectInit(moveModifiers player)
    {
        rows = nodes.GetLength(0);
        cols = nodes.GetLength(1);

        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < cols; c++)
            {
                getNode(c, r).runEffects("init", player, null);
            }
        }
    }

    bool getLandType(moveModifiers a, string landtype)
    {
        switch (landtype)
        {
            case "open":
                return true;
            case "door":
                return true;
            case "water":
                return a.waterMovement;
            case "roof":
                return false;
            default:
                return false;
        }
    }

    public bool traversable(moveModifiers a, Node node)
    {
        //Debug.Log("landtype " + landtype);

        if(node == null)
        {
            return false;
        }
        string landtype = node.landtype;
        if (landtype == "impassable")
        {
            return false;
        }
        bool land = getLandType(a, landtype);
        if (a.flyingMovement)
        {
            land = true;
        }

        return land;
    }

    public int diagonalBlocker(int x, int y, int i, moveModifiers playerScript)
    {
        //prevents diagonal movement if tiles too the side of you are blocking movement
        Node Blocker = getNode(x, y);
        if (!traversable(playerScript, Blocker))
        {
            return 0;
        }
        return i;
    }

    void addMouseDown(GameObject cube, bool p)
    {
        MM_MouseDown md = cube.AddComponent<MM_MouseDown>();
        md.child = p;
        BoxCollider2D bc = cube.AddComponent<BoxCollider2D>();
        bc.isTrigger = true;
        bc.enabled = !p;
    }

    void addMouseDownNPCMaker(GameObject cube, bool p)
    {
        MousedownNPCMap md = cube.AddComponent<MousedownNPCMap>();
        md.child = p;
        BoxCollider2D bc = cube.AddComponent<BoxCollider2D>();
        bc.isTrigger = true;
    }

    public int gridWidth()
    {
        return grid.GetLength(0);
    }

    public int gridLength()
    {
        return grid.GetLength(1);
    }

}
