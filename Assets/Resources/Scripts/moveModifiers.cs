﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveModifiers : MonoBehaviour {



    public bool waterMovement = false;
    public bool landMovement = true;
    public bool flyingMovement = false;
    public bool ignoreRoof = false;



    public moveModifiers copyVals(moveModifiers m)
    {
        m.waterMovement = waterMovement;
        m.landMovement = landMovement;
        m.flyingMovement = flyingMovement;
        m.ignoreRoof = ignoreRoof;
        return m;
    }

   
}
