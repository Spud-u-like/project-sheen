﻿using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine.UI;
using UnityEngine.Rendering;

public class Movement {

    Game_Main main;

    public GameObject player;
    
    string dir_animation = "bowieTest";
    string direction = "down";
    public float speed = 8.0f;
    float vert = 0;
    float hoz = 0;
    
    public Vector2 dest;
    bool moving = false;
    bool initMoving = false;
    Graph graph;
    public int x = 0;
    public int y = 0;
    public int previousX = 0;
    public int previousY = 0;
    moveModifiers playerScript;
    float offset_y = 0.35f;

    int point = 0;
    Search search;
    //menu stuff
    bool menuPressed = false;
    Menu menu;

    SortingGroup playerLayer;

    public void InitMenu(Menu thisMenu)
    {
        menu = thisMenu;
    }

    public void init_movement(GameObject _player, Graph _graph, Game_Main _main)
    {
        main = _main;
        player = _player;
        playerLayer = player.GetComponent<SortingGroup>();
        offset_y = main.offset_y;
        playerScript = player.GetComponent<moveModifiers>();
        graph = _graph;
        //somehow this has flipped, and at this point I'm too afraid to find out why
        y = graph.startX;
        x = graph.startY;
        //Debug.Log(gridX + " " + y);
        GameObject n = graph.getNode(x, y).gameObject;
        
        player.transform.position = new Vector2 (n.transform.position.x, n.transform.position.y+offset_y);
        playerLayer.sortingOrder = n.gameObject.GetComponent<SpriteRenderer>().sortingOrder + 2;
        _graph.runEffectInit(playerScript);
    }

    public bool traveling()
    {
        //graph.getNode(gridX + h, y + v).transform.position
        if (moving)
        {
            player.transform.position = Vector2.MoveTowards(player.transform.position, new Vector2 (dest.x, dest.y + offset_y), Time.deltaTime * (speed));
            if(new Vector2(player.transform.position.x, player.transform.position.y) == new Vector2(dest.x, dest.y + offset_y))
            {

                //runs effects that happen on player finishing move on tile
                playerLayer.sortingOrder = y + 2;
                moving = graph.getNode(x, y).runEffects("after", playerScript, this);

            }
        }
        return moving;
    }

    public void initMoveByPoints(int _x, int _y)
    {
        //Debug.Log(gridX + " " + y + " " + " " + _x + " " +_y);
        if (_x != x || _y != y)
        {
            search = new Search(graph, main);
            
            main.gamestate = "cutscene";
            search.Start(graph.getNode(y, x), graph.getNode(y, x), player.GetComponent<moveModifiers>());
        }
    }

    void moveByPoints()
    {
        if (search!=null && !moving)
        {
            //Debug.Log(point + "  " + search.finished_path.Count);
            dest = search.finished_path[point].transform.position;
            moving = true;
            int new_x = search.finished_path[point].gridX;
            int new_y = search.finished_path[point].gridY;
            if (new_y < x)
            {
                dir_animation = "bowieLeft";
                direction = "left";
            }
            if (new_y > x)
            {
                dir_animation = "bowieRight";
                direction = "right";
            }
            if (new_x > y)
            {
                dir_animation = "bowieTest";
                direction = "down";
            }
            if (new_x < y)
            {
                dir_animation = "bowieUp";
                direction = "up";
            }

            //player.GetComponent<Animator>().Play(dir_animation);
            Debug.Log(direction);
            //player.GetComponent<animate>().findAnimation(direction);
            player.GetComponent<Character>().findAnimation(direction);
            Debug.Log(new_x + "  " + new_y + "   " + x + " " +y + " " + dir_animation);
            x = new_y;
            y = new_x;

            if (point < search.finished_path.Count-1)
            {
                point++;
            }
            else
            {
                point = 0;
                search = null;
                main.gamestate = "play";
            }
        }
    }

    public void move () {

        //parse button presses to check for in different gamestates later..

        if (initMoving) { return; }

        if (Input.GetButtonDown("Menu"))
        {
            menuPressed = true;
        }


        vert = Input.GetAxis("Vertical"); //* Time.deltaTime * (speed);
        hoz = Input.GetAxis("Horizontal"); //* Time.deltaTime * (speed);
        int h = 0;
        int v = 0;

       
        traveling();
        moveByPoints();

        if (main.gamestate == "play")
        {
            //parse pressing the menu button
            if (menuPressed)
            {                
                //generate the main menu with 4 blocks to it
                menu.GenerateMenu(4, "main","menu");

                main.gamestate = "menu";
                //pretend we havent pressed it this frame or else it will just exit out of the menu later in this script
                menuPressed = false;

            }



            if (!Input.GetButton("Vertical"))
            {
                vert = 0;
            }
            if (!Input.GetButton("Horizontal"))
            {
                hoz = 0;
            }

            if (hoz < 0)
            {
                h = -1;
            }
            if (hoz > 0)
            {
                h = 1;
            }
            if (vert < 0)
            {
                v = 1;
            }
            if (vert > 0)
            {
                v = -1;
            }

            if(h != 0 && v != 0 && main.dBlocker)
            {
                v = graph.diagonalBlocker(x, y + v, v, playerScript);
                h = graph.diagonalBlocker(x + h, y, h, playerScript);
            }

            setDirection(h, v);
            //if (!moving) { player.GetComponent<animate>().findAnimation(direction); }
            if (!moving) { player.GetComponent<Character>().findAnimation(direction); }
            if (graph.getNode(x + h, y + v)!=null && (vert!= 0 || hoz !=0) && !moving)
            {
                //player.transform.position = Vector2.Lerp(player.transform.position, graph.getNode(gridX + h, y + v).transform.position, Time.deltaTime * (speed));
                Node node = graph.getNode(x + h, y + v);
                if (node.traversable(playerScript, false) || graph.getNode(x, y).landtype == "stairs")
                {
                    //player.GetComponent<SpriteRenderer>().sortingOrder = y +1;
                    //player.GetComponent<Character>().sortingOrder(y + 1);
                    
                    dest = node.transform.position;
                    
                    //runs effects that happen on player leaving a tile
                    Node onLeave = graph.getNode(x, y);
                    Node before = graph.getNode(x + h, y + v);
                    previousX = x;
                    previousY = y;
                    x += h;
                    y += v;
                    if (y > previousY)
                    {
                        //if the player is heading in any direction but up, set the sorting order
                        //prevents being below node hes leaving
                        playerLayer.sortingOrder = y + 2;
                    }
                    //unless travling up in which case you adopt on arrival
                    onLeave.runEffects("onLeave", playerScript, this);
                    before.runEffects("before", playerScript, this);
                    moving = true;
                    initMoving = false;
                    //runs effects that happen on player starting to enter a tile
                    //Debug.Log("before");
                    //Debug.Log(x + " " + y + " " + graph.getNode(x, y).name);
                    //Debug.Log(graph.getNode(x, y).name);
                    //graph.getNode(x, y).effects_init();

                }
            }

            if (Input.GetButtonDown("Action"))
            {
                Node node = null;
                switch (direction)
                {
                    case "up":
                        node = graph.getNode(x, y -1);
                        break;
                    case "down":
                        node = graph.getNode(x, y +1);
                        break;
                    case "left":
                        node = graph.getNode(x - 1, y);
                        break;
                    case "right":
                        node= graph.getNode(x + 1, y);
                        break;
                    default:
                        break;
                }
                if (node)
                {
                    node.interact(playerScript, main);
                    return;
                }
                //initMoveByPoints(0, 5);
                //main.uiHandler.interact("");
                /*if (main.uiHandler.textBox.cont)
                {
                    if (!main.uiHandler.textBox.textContinue())
                    {
                        main.uiHandler.closeText();
                    }
                }
                else {
                    main.uiHandler.goText("I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@THIS BIG! " +
                        "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@THIS BIG!" +
                        "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@THIS BIG!" +
                        "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@THIS BIG!" +
                        "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@THIS BIG!"
                        );
                }*/


            }
        }
        if (main.gamestate == "text")
        {
            if (Input.GetButtonDown("Action"))
            {
                if(main.npc)
                {
                    main.npc.interact(-1);
                }
                if (!main.uiHandler.textBox.textContinue())
                {
                    main.uiHandler.closeText();
                    main.gamestate = "play";
                }
                return;
            }
        }

        if (main.gamestate == "menu")
        {
            //first check if we are shutting the menu
            if (menuPressed)
            {
                if (menu.canBeClosed)
                {
                    menu.CloseMenu();
                    main.gamestate = "play";
                    menuPressed = false;
                }
            }
            //now check the other ways we can interact with the menu... such as left down right and up!
            if (hoz != 0 || vert != 0)
            {
                menu.SelectMenuBlock(hoz, vert);
            }

        }


    }

    void setDirection(int h, int v)
    {
        if (h < 0)
        {
            dir_animation = "bowieLeft";
            direction = "left";
        }
        if (h > 0)
        {
            dir_animation = "bowieRight";
            direction = "right";
        }
        if (v > 0)
        {
            dir_animation = "bowieTest";
            direction = "down";
        }
        if (v < 0)
        {
            dir_animation = "bowieUp";
            direction = "up";
        }
    }


}
