﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marque : MonoBehaviour {

    MM_Main m;
    GameObject startingPoint;
    bool startingPointSet = false;
    GameObject endingPoint;
    bool endingPointSet = false;
    public Node[,] nodeArray;

    bool drawing = false;

    // Use this for initialization
    void Start () {
		m = MM_Main.instance;
    }

    public void interact(GameObject point)
    {
        if(startingPointSet && !endingPointSet)
        {
            finishMarque(point);
        } else
        {
            restartLines(point);
        }
    }
	
	// Update is called once per frame
	public void restartLines (GameObject point) {
        if (drawing)
        {
            return;
        }
        gameObject.SetActive(true);
        drawing = true;

        /*
        lineToMouse[] ltmArray = GetComponentsInChildren<lineToMouse>();
        Vector3 finalPos = getRelativeStart(point);
        //transform.position = finalPos;
        //print(ltmArray.Length + " length of ltmarray");
        foreach (lineToMouse l in ltmArray)
        {
            l.resetPoints(finalPos);
        }
        */
        lineToMouse[] ltmArray = GetComponentsInChildren<lineToMouse>();
        foreach (lineToMouse l in ltmArray)
        {
            l.lockToCurrentMousePos();
        }
        
        startingPoint = point;
        startingPointSet = true;
        endingPointSet = false;
        endingPoint = null;
        nodeArray = null;
    }

    public void finishMarque(GameObject point)
    {

        Node endingNode = point.GetComponent<Node>();
        Node startingNode = startingPoint.GetComponent<Node>();
        int startX = startingNode.gridX;
        int startY = startingNode.gridY;
        print(startX + " " + startY + " arse");
        int endX = endingNode.gridX;
        int endY = endingNode.gridY;
        if (endX < startX)
        {
            int temp = startX;
            startX = endX;
            endX = temp;
        }
        if (endY < startY)
        {
            int temp = startY;
            startY = endY;
            endY = temp;
        }
        print("End points " + endX + " " + endY);
        print("Start points " + startX + " " + startY);
        nodeArray = new Node[endX- startX + 1,endY- startY + 1];
        for(int i = 0; i < nodeArray.GetLength(0); i++)
        {
            for (int k = 0; k < nodeArray.GetLength(1); k++)
            {
                nodeArray[i,k] = m.graph.getNode(startY + k, startX + i);
            }
        }

        startingPoint = nodeArray[0, 0].gameObject;
        endingPoint = nodeArray[nodeArray.GetLength(0)-1, nodeArray.GetLength(1)-1].gameObject;

        float xBound = startingPoint.GetComponent<SpriteRenderer>().bounds.size.x;
        float yBound = startingPoint.GetComponent<SpriteRenderer>().bounds.size.y;
        Vector3 startingVector = new Vector3(startingPoint.transform.position.x - xBound / 2, startingPoint.transform.position.y + yBound / 2);
        Vector3 endingVector = new Vector3(endingPoint.transform.position.x + xBound / 2, endingPoint.transform.position.y - yBound / 2);

        lineToMouse[] ltmArray = GetComponentsInChildren<lineToMouse>();
        foreach (lineToMouse l in ltmArray)
        {
            l.resetPoints(startingVector);
            l.setEndPoints(endingVector);
        }

        startingPointSet = false;
        endingPointSet = true;
        drawing = false;
        m.currentMarque = this;
    }

    /*Vector3 getRelativeStart(GameObject point)
    {
        Vector3 finalPos = new Vector3(point.transform.position.x - point.GetComponent<SpriteRenderer>().bounds.size.x / 2, point.transform.position.y + point.GetComponent<SpriteRenderer>().bounds.size.y / 2, 0);
        return finalPos;
    }*/

    /*Vector3 getRelativeEnd(GameObject point)
    {

        Node sp = startingPoint.GetComponent<Node>();
        Node ep = point.GetComponent<Node>();
        int realStartingX = sp.gridX;
        int realStartingY = sp.gridY;
        float xBound = point.GetComponent<SpriteRenderer>().bounds.size.x;
        float yBound = point.GetComponent<SpriteRenderer>().bounds.size.y;


        if (sp.gridY > ep.gridY)
        {
            realStartingX -= 1;
            xBound = point.transform.position.x - xBound / 2;
            print("X S>E");
        } else
        {
            print("X S<E");
            xBound = point.transform.position.x + xBound / 2;
        }

        if (sp.gridX > ep.gridX)
        {
            realStartingY -= 1;
            yBound = point.transform.position.y + yBound / 2;
            print("Y S>E");
        } else
        {
            print("Y S<E");
            yBound = point.transform.position.y - yBound / 2;
        }


        //print("real starting location " + xBound + " " + yBound);
        //print("real starting location " + (point.transform.position.x + xBound / 2) + " " + (point.transform.position.y - yBound / 2));
        print("start points" + sp.gridX + " " + sp.gridY);
        print("End points" +ep.gridX + " " + ep.gridY);

        sp = m.ground.getNode(realStartingX, realStartingY);
        startingPoint = sp.gameObject;



        Vector3 finalPos = new Vector3(xBound, yBound, 0);
        print(finalPos);
        return finalPos;
    }*/
}
