﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScroll
{
    int maxScollValue = 10;
    int minScollValue = 2;


    public void scroll()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0) // forward
        {
            if (Camera.main.orthographicSize - 1 > minScollValue)
            {
                Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize - 1, 50);
            }
        }
        if (Input.GetAxis("Mouse ScrollWheel") < 0 && minScollValue != 0) // backward
        {
            if (Camera.main.orthographicSize + 1 < maxScollValue)
            {
                Camera.main.orthographicSize = Mathf.Min(Camera.main.orthographicSize + 1, 50);
            }
        }
    }
}
