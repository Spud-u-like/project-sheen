﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stopAnimationsInEditor : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GameObject settings = GameObject.Find("SettingsHolder");
        //print(settings + " settings");
        if (settings) {
            if (settings.GetComponent<Settings>().editor)
            {
                Animator anim = GetComponent<Animator>();
                anim.enabled = false;
            }
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
