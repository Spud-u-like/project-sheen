﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class lineToMouse : MonoBehaviour {

    List<Vector3> linePoints = new List<Vector3>();
    LineRenderer lineRenderer;
    public float startWidth = 0.1f;
    public float endWidth = 0.1f;
    public float threshold = 0.001f;
    Camera thisCamera;
    //int lineCount = 0;

    public bool lockX = false;
    public bool lockY = true;
    public bool followMouseX = false;
    public bool followMouseY = false;

    Vector3 lastPos = Vector3.one * float.MaxValue;
    Vector3 anchorPoint;
    Vector3 endPoint;
    bool endPointSet = false;

    // Use this for initialization
    void Awake () {
        lineRenderer = GetComponent<LineRenderer>();
        thisCamera = Camera.main;
        Vector3 lastPos = Vector3.one * float.MaxValue;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        Vector3 originPoint = anchorPoint;
        mousePos.z = thisCamera.nearClipPlane;
        Vector3 mouseWorld = thisCamera.ScreenToWorldPoint(mousePos);
        if(endPointSet)
        {
           mouseWorld = endPoint;
        }
        float dist = Vector3.Distance(lastPos, mouseWorld);
        if (dist <= threshold)
            return;



        //UpdateLine();
        if(lockX)
        {
            mouseWorld = new Vector3(originPoint.x, mouseWorld.y, mouseWorld.z);
        }
        if (lockY)
        {
            mouseWorld = new Vector3(mouseWorld.x, originPoint.y, mouseWorld.z);
        }
        if (followMouseX)
        {
            originPoint = new Vector3(mouseWorld.x, originPoint.y, originPoint.z);
        }
        if (followMouseY)
        {
            originPoint = new Vector3(originPoint.x, mouseWorld.y, originPoint.z);
        }

        UpdateLine(mouseWorld, originPoint);
    }


    void UpdateLine(Vector3 mouseWorld, Vector3 originPoint)
    {
        lineRenderer.SetPosition(1, mouseWorld);
        lineRenderer.SetPosition(0, originPoint);
    }

    public void setEndPoints(Vector3 _endpoint)
    {
        endPoint = _endpoint;
        endPointSet = true;
    }

    public void lockToCurrentMousePos()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos.z = thisCamera.nearClipPlane;
        Vector3 mouseWorld = thisCamera.ScreenToWorldPoint(mousePos);
        anchorPoint = mouseWorld;
        endPointSet = false;
    }

    public void resetPoints(Vector3 _anchorPoint)
    {
       anchorPoint = _anchorPoint;
       endPointSet = false;

        /*Vector3 mousePos = Input.mousePosition;
       mousePos.z = thisCamera.nearClipPlane;
       Vector3 mouseWorld = thisCamera.ScreenToWorldPoint(mousePos);

       lineRenderer.SetPosition(1, new Vector3(0,0,0));
       lineRenderer.SetPosition(0, new Vector3(0, 0, 0));*/
    }
}

