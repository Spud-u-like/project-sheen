﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System.IO;

public class Preloader : MonoBehaviour
{
    animationHandler ah = new animationHandler();
    int arrayNumber = 0;
    string[] list;
    MapBuilder mb;

    string Mapname;
    bool preserveNPC;
    string Maptype;
    Main m;

    public void init(string[] _list, MapBuilder _mb, string _Mapname, bool _preserveNPC, string _Maptype, Main _m)
    {
        list = _list;
        mb = _mb;
        transform.name = "preloader";

        //hold these my good man

        Mapname = _Mapname;
        preserveNPC = _preserveNPC;
        Maptype = _Maptype;
        m = _m;
        nextstep();
    }
    void nextstep()
    {
        print(arrayNumber + " " + list.Length);
        if(arrayNumber >= list.Length)
        {
            mb.buildMap(Mapname, preserveNPC, Maptype, m, gameObject);
            this.gameObject.SetActive(false);
        } else
        {
            if (list[arrayNumber] == "" || list[arrayNumber] == "SYSTEM")
            {
                arrayNumber++;
                nextstep();
            }
            else
            {
                preload(list[arrayNumber]);
            }
        }
    }
    void preload(string fileName)
    {
        StartCoroutine(ah.LoadSpriteSheet(fileName, "Tilesheets", (remoteTexture) => {
            if (remoteTexture)
            {
                Texture texture = remoteTexture;
                texture.filterMode = FilterMode.Point;
                Debug.Log(texture.width);
                string filePath = Application.dataPath + "/StreamingAssets/Tilesheets/" + Path.GetFileNameWithoutExtension(fileName) + "/" + fileName + ".json";
                string dataAsJson = File.ReadAllText(filePath);
                //init_build(dataAsJson);
                SpritesheetHolder spritesheetholder = JsonUtility.FromJson<SpritesheetHolder>(dataAsJson);
                GameObject subfolder = new GameObject();
                subfolder.name = fileName;
                subfolder.transform.SetParent(gameObject.transform);
                //OK HERES THE LOGIC
                //load sheet
                //check if 
                spritesheetholder = ah.divideSpriteSheet(texture, subfolder.transform, 24, 24, fileName, true, spritesheetholder, true);

                print("this far");
                /*foreach (Transform child in subfolder.transform)
                {               
                    child.name = fileName + "/" + child.name;
                }*/
                arrayNumber++;
                nextstep();
            }
        }));
    }
}
