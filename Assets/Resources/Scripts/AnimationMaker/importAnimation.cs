﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System;
using System.IO;
public class importAnimation : MonoBehaviour
{

    int spriteWidth = 24;
    int spriteHeight = 24;
    Texture texture;
    public GridLayoutGroup spriteGrid;
    public GridLayoutGroup frameGrid;
    public string fileName = "";
    public GameObject animatorSprite;
    public GameObject frameSelector;
    Vector3 frameSelectorOriginalPos;


    float frameCounter = 100;
    float frameTime = 0;
    int frameNumber = 0;
    public bool pauseAnimation = false;
    public GameObject fps;
    InputField fpsText;
    public InputField heightInput;
    public InputField widthInput;
    public InputField animationName;

    repopDropdown repopDropdown;
    public Dropdown spriteSheets;
    public Dropdown savedAnimations;

    //public bool flipX = false;
    //public bool flipY = false;

    public Toggle flipX;
    public Toggle flipY;

    animationHandler ah = new animationHandler();


    /*
     * Done quite a bit today, but tomorrow you need
     * Chanageable size paramiters for sprite sizes
     * drop down to import different sprite sheets
     * sf2 sprite sheet to import
     * Maybe if you have time, try and save it as an animation?
     * Oh and load
     * 
     * thinking about it, this just makes animations
     * don't know how well or how quickly it loads an animation
     */

    void Start()
    {
        savedAnimations.gameObject.SetActive(false);
        BetterStreamingAssets.Initialize();
        repopDropdown = GetComponent<repopDropdown>();
        repopDropdown.repopulateDropdown(spriteSheets, "NPCAnimations/", "*.png");
        fpsText = fps.GetComponent<InputField>();
        fpsText.characterValidation = InputField.CharacterValidation.Integer;
        fpsText.text = frameCounter.ToString();
        heightInput.characterValidation = InputField.CharacterValidation.Integer;
        heightInput.text = spriteHeight.ToString();
        widthInput.characterValidation = InputField.CharacterValidation.Integer;
        widthInput.text = spriteWidth.ToString();
        frameSelectorOriginalPos = frameSelector.transform.position;
    }

    /*public void swapOutTile()
    {

        //Sprite = Resources.LoadAll<Sprite>(spriteNames);
        string[] list = BetterStreamingAssets.GetFiles("/Tiles/", "*.png");
        //print(list[0]);
        WWW localFile;
        localFile = new WWW(list[0]);
        Texture texture = localFile.texture;
        Sprite sprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
        SpriteRenderer spriteR = gameObject.GetComponent<SpriteRenderer>();
        spriteR.sprite = sprite;

    }*/


    public void save()
    {
        SaveAndLoadAnimations a = new SaveAndLoadAnimations();
        a.buildFile(animationName.text, Path.GetFileNameWithoutExtension(fileName), fileName, frameCounter, frameGrid.transform, flipX.isOn, flipY.isOn);
    }

    public void load()
    {
        SaveAndLoadAnimations a = new SaveAndLoadAnimations();
        AnimationHeader ani = a.loadFile(Path.GetFileNameWithoutExtension(fileName) + "/" + savedAnimations.options[savedAnimations.value].text);
        if (ani == null)
        {
            errorHandler(new Exception("Failed to load animation - null"), true);
        }
        try
        {
            frameCounter = ani.fps;
            fpsText.text = ani.fps.ToString();
            fileName = ani.spriteSheet;
            frameGrid.transform.delAllChildren();
            animationName.text = ani.name;
            flipX.isOn = ani.flipX;
            flipY.isOn = ani.flipY;
            flips();
            foreach (AnimationCords cords in ani.cords)
            {
                ah.addSprite(cords.x, cords.y, cords.width, cords.height, texture, frameGrid.transform, fileName, false, false);
            }
        }
        catch (Exception e)
        {
            errorHandler(e, true);
        }

    }

    public void flips()
    {
        Vector3 v = animatorSprite.transform.localScale;
        float newX = v.x;
        float newY = v.y;
        if ((flipX.isOn && newX > 0) || (!flipX.isOn && newX < 0))
        {
            newX *= -1;
        }
        if ((flipY.isOn && newY > 0) || (!flipY.isOn && newY < 0))
        {
            newY *= -1;
        }
        animatorSprite.transform.localScale = new Vector3(newX, newY, v.z);
    }

    public void changeNumber()
    {
        int number;
        bool success;
        success = int.TryParse(heightInput.text, out number);
        if (success)
        {
            spriteHeight = number;
        }
        success = int.TryParse(widthInput.text, out number);
        if (success)
        {
            spriteWidth = number;
        }
    }

    public void loadSpriteSheet()
    {
        print(spriteSheets.options[spriteSheets.value].text);

        fileName = spriteSheets.options[spriteSheets.value].text;
        spriteGrid.transform.delAllChildren();
        frameGrid.transform.delAllChildren();
        //StartCoroutine(LoadSpriteSheet());
        StartCoroutine(ah.LoadSpriteSheet(fileName, "NPCAnimations", (remoteTexture) => {
            if (remoteTexture)
            {
                texture = remoteTexture;
                texture.filterMode = FilterMode.Point;
                Debug.Log(texture.width);
                if (!Directory.Exists(Application.dataPath + "/StreamingAssets/NPCAnimations/" + Path.GetFileNameWithoutExtension(fileName) + "/"))
                {
                    //if it doesn't, create it
                    Directory.CreateDirectory(Application.dataPath + "/StreamingAssets/NPCAnimations/" + Path.GetFileNameWithoutExtension(fileName) + "/");

                }
                divideSpriteSheet(texture);
                savedAnimations.gameObject.SetActive(true);
                repopDropdown.repopulateDropdown(savedAnimations, "NPCAnimations/" + Path.GetFileNameWithoutExtension(fileName) + "/", "*.JSON");
            }
        }));
    }




    /*public IEnumerator LoadSpriteSheet()
    {
        //string[] list = BetterStreamingAssets.GetFiles("/Tiles/", "*.png");
        //print(list[0]);
        WWW localFile = new WWW(Application.dataPath + "/StreamingAssets/NPCAnimations/" + fileName);
        //localFile = new WWW(list[0]);
        //print("file://" + System.IO.Path.Combine(Application.streamingAssetsPath, list[0]));
        yield return localFile;
        if (localFile.error != null)
        {
            Debug.Log(localFile.error);
        }

        texture = localFile.texture;
        texture.filterMode = FilterMode.Point;

        if (!Directory.Exists(Application.dataPath + "/StreamingAssets/NPCAnimations/" + Path.GetFileNameWithoutExtension(fileName) + "/"))
        {
            //if it doesn't, create it
            Directory.CreateDirectory(Application.dataPath + "/StreamingAssets/NPCAnimations/" + Path.GetFileNameWithoutExtension(fileName) + "/");

        }

        divideSpriteSheet(texture);
        savedAnimations.gameObject.SetActive(true);
        repopDropdown.repopulateDropdown(savedAnimations, "NPCAnimations/" + Path.GetFileNameWithoutExtension(fileName) + "/", "*.JSON");
    }*/

    void divideSpriteSheet(Texture texture)
    {

        ah.divideSpriteSheet(texture, spriteGrid.transform, spriteHeight, spriteWidth, fileName, false, null, false);

        foreach (Transform child in spriteGrid.transform) {
            Button button = child.gameObject.AddComponent<Button>();
            button.onClick.AddListener(delegate { buttonClick(child.gameObject); });
        }
        emptySpriteAnimator();
    }

    void emptySpriteAnimator()
    {
        animatorSprite.GetComponent<Image>().sprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, 1, 1), Vector2.zero);
    }

    /*void addSprite(int x, int y, int width, int height, Texture texture, Transform parent)
    {
        Sprite newSprite = Sprite.Create(texture as Texture2D, new Rect(x, y, width, height), Vector2.zero);
        GameObject g = new GameObject();
        Image I = g.AddComponent<Image>();
        I.sprite = newSprite;
        g.transform.SetParent(parent, false);
        frameData framedata = g.AddComponent<frameData>();
        framedata.x = x;
        framedata.y = y;
        framedata.width = width;
        framedata.height = height;
        framedata.parentSpriteName = fileName;
        Button button = g.AddComponent<Button>();
        button.onClick.AddListener(delegate { buttonClick(g); });
    }*/

    public void buttonClick(GameObject g)
    {
        print(g);
        GameObject copy = Instantiate(g, frameGrid.transform);
    }

    public void togglePause()
    {
        pauseAnimation = !pauseAnimation;
    }

    public void newAnimation()
    {
        frameGrid.transform.delAllChildren();
        frameCounter = 100;
        fpsText.text = frameCounter.ToString();
        animationName.text = "";
    }

    public void moveFrame(int amount)
    {
        frameNumber += amount;
        changeFrame();
    }

    private void Update()
    {
        int number;

        bool success = int.TryParse(fpsText.text, out number);
        if (success)
        {
            frameCounter = number;
        }
        else
        {
            return;
        }
        if (frameTime > frameCounter / 1000 && !pauseAnimation)
        {
            changeFrame();
            frameTime = 0;
            frameNumber++;
        }
        frameTime += 1.0f * Time.deltaTime;
    }

    void changeFrame()
    {
        if (!frameGrid)
        {
            //print("no framegrid");
            return;
        }
        int childCount = frameGrid.transform.childCount;
        if (childCount == 0)
        {
            //print("no frames to work with");
            frameSelector.transform.position = frameSelectorOriginalPos;
            emptySpriteAnimator();
            return;
        }
        if (frameNumber > childCount - 1)
        {
            frameNumber = 0;
        }
        if (frameNumber < 0)
        {
            frameNumber = childCount - 1;
        }
        //print(frameNumber);
        animatorSprite.GetComponent<Image>().sprite = frameGrid.transform.GetChild(frameNumber).GetComponent<Image>().sprite;
        frameSelector.transform.position = frameGrid.transform.GetChild(frameNumber).transform.position;
    }

    public void errorHandler(Exception error, bool showToUser)
    {
        Debug.LogException(error, this);
        if (showToUser)
        {
            print(error.ToString());
        }
    }

}
