﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



abstract public class Obj : MonoBehaviour
{
    public string itemName = "";

    abstract public bool runObj(GameObject player, Game_Main main);

    public bool basicFind(GameObject player, Game_Main main)
    {
        string alert = player.name + " found a " + itemName + " ! @pagebreak@ ";
        main.uiHandler.goText(alert);
        //check if inventory space
        //added to inventory
        //drop confirm deny?
        //christ the inventory system is gonna be complex
        //whats worse is the dialog system is gonna need an overhall just to cope!
        //Dialog split? Go to line whatever, all this needs to be in the @ brackets
        return true;
    }

    //abstract public bool initEffect(Graph graph, int gridX, int y, moveModifiers player);

    //abstract public string type();
}