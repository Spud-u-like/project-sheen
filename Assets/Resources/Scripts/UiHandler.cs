﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UiHandler : MonoBehaviour {

    public GameObject dialogBox;
    public textAppear textBox;
    Game_Main main;
    //bool displayingText = false;
    // Use this for initialization
    void Start () {
        main = Game_Main.instance;
	}
	
	// Update is called once per frame
	/*void Update () {
        if (displayingText)
        {

        }
	}*/

    /*public void interact(string t)
    {
        if (textBox.cont)
        {
            if (!textBox.textContinue())
            {
               closeText();
            }
            textBox.cont = false;
        }
        else {
            if (!textBox.textGo)
            {
                goText("I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG! " +
                     "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG! " +
                     "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG! " +
                     "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG! " +
                     "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG! " +
                     "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG! " +
                     "I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG! "
                     );
            }
        }
    }*/

    public void goText(string t)
    {
        dialogBox.SetActive(true);
        //textBox.GetComponent<UnityEngine.UI.Text>().text = t;
        //displayingText = true;
        main.gamestate = "text";
        textBox.textStart(t);
    }

    public void closeText()
    {
        textBox.clear();
        dialogBox.SetActive(false);
        main.gamestate = "play";
        //displayingText = false;
    }
}
