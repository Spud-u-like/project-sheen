﻿using UnityEngine;
using UnityEditor;
using System.IO;
//using OrbCreationExtensions;
//using SimpleJsonExtensions;

public class SaveLoad : MonoBehaviour {

    public static SaveLoad instance;

    void Awake()
    {
        instance = this;
    }

    public void WriteString()
    {
        string path = "Assets/Resources/test.txt";

        //Write some text to the test.txt file
        StreamWriter writer = new StreamWriter(path, true);
        writer.WriteLine("Test");
        writer.Close();

        //Re-import the file to update the reference in the editor
        //AssetDatabase.ImportAsset(path);
        TextAsset asset = Resources.Load("test") as TextAsset;
        string[] temp = new string[1];
        //string jsonString = temp.JsonString();
        //Print the text from the file
        Debug.Log(asset.text);

        /* 
         *  WHAT IT ACTUALLY NEEDS TO DO:
         *  grab the graph
         *  create an arraylist of 2d arraylists (length is the node with the biggest child count, size of each 2d is grid size)
         *  2d array is string, each string is empty
         *  create arraylist of string arrays (for tile names) tileNames
         *  interate through the grid
         *  if tile is new to tileNames add to list, else find place in array and thats its number in the 2d array
         *  
         *  Effects (remember its saved in that kackhanded way
         *  
         *  NPCS
         *  
         *  
        */
    }

    /*[MenuItem("Tools/Read file")]

    public static void ReadString()
    {
        string path = "Assets/Resources/test.txt";

        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(path);
        Debug.Log(reader.ReadToEnd());
        reader.Close();
    }*/
}
