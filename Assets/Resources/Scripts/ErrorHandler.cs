﻿using System;
using UnityEngine;

public class ErrorHandler //: MonoBehaviour
{
    public void error(Exception error, bool showToUser)
    {
        Debug.LogException(error, null);
        if (showToUser)
        {
            //print(error.ToString());
        }
    }

}
