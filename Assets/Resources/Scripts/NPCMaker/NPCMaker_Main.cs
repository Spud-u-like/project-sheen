﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

public class NPCMaker_Main : Main
{
    repopDropdown repopDropdown;
    public Dropdown spriteSheets;
    public GameObject spriteGrid;
    public GameObject NPCGrid;
    public string fileName;
    //ErrorHandler e = new ErrorHandler();
    animationHandler ah = new animationHandler();
    AnimationHeader[] animationHeaderList;
    animationPropsHolder[] animationPropsHolderList;
    Texture texture;
    public Image rectTexture;
    public GameObject pickPos;
    public Color c1 = Color.yellow;
    public Color c2 = Color.red;
    public int lengthOfLineRenderer = 4;


    public InputField NPCName;
    public GameObject childSprites;
    //private string gameDataProjectFilePath = "/StreamingAssets/NPCs/";

    List<string> listOfNPCS;

    void Start()
    {
        BetterStreamingAssets.Initialize();
        repopDropdown = GetComponent<repopDropdown>();
        repopDropdown.repopulateDropdown(spriteSheets, "NPCAnimations/", "*.png");
        repopNPCS();
    }



    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Delete))
        {
            if(currentTarget!= null)
            {
                if (currentTarget.tag != "ControlObject")
                {
                    currentTarget.transform.parent.GetComponent<Image>().enabled = true;
                    GameObject g = currentTarget.transform.parent.gameObject;
                    Destroy(currentTarget);
                    currentTarget = g;
                    changeTarget(g);
                }
            }
        }
    }

    public void buttonDownSave()
    {
        //NPCName.text;
        if(NPCName.text.Length == 0)
        {
            Debug.LogError(new Exception("Save Failed: No Name Provided"));
            return;
        }
        NPC n = new NPC();
        n.animationNames = new string[childSprites.transform.childCount];
        n.animationHeaders = new AnimationHeader[childSprites.transform.childCount];
        int i = 0;
        foreach (Transform child in childSprites.transform)
        {
            print(child);
            //take name of child
            //take animation header of child in child
            n.animationNames[i] = child.name;
            if (child.childCount > 0)
            {
                animationPlayer a = child.GetChild(0).GetComponent<animationPlayer>();
                n.animationHeaders[i] = a.aHeader;
            }
            i++;
        }
        string dataAsJson = JsonUtility.ToJson(n);
        //saveWrite(dataAsJson, NPCName.text);
        SaveAndLoadNPC saveloadNPC = new SaveAndLoadNPC();
        saveloadNPC.saveWrite(dataAsJson, NPCName.text);
    }

    void repopNPCS()
    {
        string[] list = BetterStreamingAssets.GetFiles("NPCs/", "*.json");
        print("test" +list.Length);
        for (int i = 0; i < list.Length; i++)
        {
            
            list[i] = list[i].Replace(".json", "");
            list[i] = list[i].Replace("NPCs/", "");
            GameObject b = new GameObject();
            Button click = b.AddComponent<Button>();
            b.transform.parent = NPCGrid.transform;
            b.transform.localScale = new Vector3(1, 1, 1);
            Text t = b.AddComponent<Text>();
            string n = list[i];
            t.text = n;
            t.font = Resources.GetBuiltinResource<Font>("Arial.ttf");
            click.onClick.AddListener(delegate { loadNPC(n); });
            //click.GetComponentInChildren<Text>().text = list[i];
        }
        listOfNPCS = new List<string>(list);
    }

    void loadNPC(string name)
    {
        SaveAndLoadNPC saveloadNPC = new SaveAndLoadNPC();
        NPC n = saveloadNPC.loadNPC(name);
        if(n == null)
        {
            return;
        }
        NPCName.text = name;
        foreach (Transform c in childSprites.transform)
        {
            c.delAllChildren();
            c.gameObject.GetComponent<Image>().enabled = true;
        }

        for (int i = 0; i < n.animationNames.Length; i++)
        {
            print(n.animationHeaders[i].name);
            print(n.animationHeaders[i].spriteSheet);
            if (n.animationHeaders[i].name.Length != 0)
            {
                contLoadNPC(n.animationNames[i], n.animationHeaders[i]);
            }
        }
    }

    void contLoadNPC(string name, AnimationHeader currentAnimation) { 
        StartCoroutine(ah.LoadSpriteSheet(currentAnimation.spriteSheet, "NPCAnimations", (remoteTexture) => {
            if (remoteTexture)
            {
                Texture _texture = remoteTexture;
                GameObject o = buildAnimation(_texture, currentAnimation, false);
                
                foreach (Transform child in childSprites.transform)
                {
                    if (child.name == name)
                    {
                        addAnimationToUI(o, child.gameObject);
                    }
                }
                
            }
        }));
    }

    /*bool saveWrite(string dataAsJson, string filename)
    {
        try
        {
            string filePath = Application.dataPath + gameDataProjectFilePath + "/" + filename + ".json";
            File.WriteAllText(filePath, dataAsJson);
            return true;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return false;
        }
    }*/


    public void populateAnimations()
    {
        spriteGrid.transform.delAllChildren();
        Debug.Log(spriteSheets.options[spriteSheets.value].text);
        fileName = spriteSheets.options[spriteSheets.value].text;
        StartCoroutine(ah.LoadSpriteSheet(spriteSheets.options[spriteSheets.value].text, "NPCAnimations", (remoteTexture) => {
            if (remoteTexture)
            {
                texture = remoteTexture;
                getAllFileNames(texture);
            }
        }));
        
    }


    void getAllFileNames(Texture _texture)
    {
        StartCoroutine(ah.getAllFileNamesI(fileName, (filePaths) => {
            if (filePaths != null)
            {
                Debug.Log(filePaths.Length);
                //StartCoroutine(getAnimationJson(filePaths));
                startBuildingAnimations(filePaths, _texture);
                //getAnimationJson(string spriteSheetFileName, string[] filePaths, System.Action<AnimationHeader[]> callback)
            }
        }));
    }
    void startBuildingAnimations(string[] filePaths, Texture _texture)
    {
        StartCoroutine(ah.getAnimationJson(fileName, filePaths, (ahl) => {
            if (ahl != null)
            {
                animationHeaderList = ahl;
                Debug.Log(animationHeaderList.Length);
                //animationPropsHolderList = new animationPropsHolder[filePaths.Length];
                for (int i = 0; i < animationHeaderList.Length; i++)
                {
                    //makeAnime(animationHeaderList[i], animationHeaderList[i].name, i);
                    //animationPropsHolderList = ah.makeAnime(animationHeaderList[i], animationHeaderList[i].name, i, texture, animationPropsHolderList);    
                   /* GameObject o = new GameObject();
                    animationPlayer a = o.AddComponent<animationPlayer>();
                    o.AddComponent<CanvasRenderer>();
                    o.AddComponent<Image>();
                    a.startAnimation(animationHeaderList[i], _texture);
                    //o.transform.parent = spriteGrid.transform;
                    o.transform.SetParent(spriteGrid.transform, false);
                    Button button = o.gameObject.AddComponent<Button>();
                    button.onClick.AddListener(delegate { buttonClick(o.gameObject); });*/

                    buildAnimation(_texture, animationHeaderList[i], true);
                }
                return;

                //pauseAnimation = false;
            }
        }));
    }

    GameObject buildAnimation(Texture _texture, AnimationHeader ah, bool addDelegate)
    {
        GameObject o = new GameObject();
        animationPlayer a = o.AddComponent<animationPlayer>();
        o.AddComponent<CanvasRenderer>();
        o.AddComponent<Image>();
        a.startAnimation(ah, _texture, false);
        //o.transform.parent = spriteGrid.transform;
        o.transform.SetParent(spriteGrid.transform, false);
        Button button = o.gameObject.AddComponent<Button>();
        if (addDelegate)
        {
            button.onClick.AddListener(delegate { buttonClick(o.gameObject); });
        }

        return o;
    }

    public void buttonClick(GameObject g)
    {
        
        if(currentTarget ==null)
        {
            return;
        }
        GameObject target = currentTarget;
        if (target.tag != "ControlObject")
        {
            print(target);
            print(target.tag);
            target = target.transform.parent.gameObject;
        }
        target.transform.delAllChildren();
        print(g);
        GameObject copy = Instantiate(g, target.transform);
        RectTransform rt = copy.GetComponent<RectTransform>();
        animationPlayer a = g.GetComponent<animationPlayer>();
        copy.GetComponent<animationPlayer>().startAnimation(a.aHeader, a.texture, false);
        addAnimationToUI(copy, target);
        changeTarget(copy);
    }

    void addAnimationToUI(GameObject copy, GameObject target)
    {
        RectTransform rt = copy.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(100, 100);
        copy.transform.position = target.transform.position;
        //copy.transform.parent = target.transform;
        copy.transform.SetParent(target.transform);
        copy.transform.localScale = normaliseScale(copy.transform.localScale);
        target.GetComponent<Image>().enabled = false;     
        copy.GetComponent<Button>().onClick.AddListener(delegate { changeTarget(copy); });
    }

    Vector3 normaliseScale(Vector3 s)
    {
        return new Vector3(tooOne(s.x), tooOne(s.y), tooOne(s.z));
    }

    float tooOne (float x)
    {
        
        if (x < 0)
        {
            x = -1;
        }
        else
        {
            x = 1;
        }
        return x;
    }
}
