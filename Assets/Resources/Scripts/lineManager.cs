﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System;
using System.IO;
using System.Threading.Tasks;
using System.Linq;

public class lineManager : MonoBehaviour
{
    // Start is called before the first frame update
    //public Image rectTexture;
    public GameObject pickPos;
    public Color c1 = Color.yellow;
    public Color c2 = Color.red;
    public int lengthOfLineRenderer = 2;
    public GameObject target;
    public int corner = 0;
    float acceleration = -10;
    float movementAmount = 0;
    bool bounce = true;
    public bool bobDirection = false;
    Vector3[] originalPos = new Vector3[2];
    LineRenderer lineRenderer;
    Gradient gradient = new Gradient();
    void Awake()
    {
        lineRenderer = gameObject.GetComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        lineRenderer.widthMultiplier = 0.02f;

        //makeLines();
    }

    public void makeLines()
    {

        // A simple 2 color gradient with a fixed alpha of 1.0f.


        float alpha = 1.0f;
        gradient.SetKeys(
            new GradientColorKey[] { new GradientColorKey(c1, 0.0f), new GradientColorKey(c2, 1.0f) },
            new GradientAlphaKey[] { new GradientAlphaKey(alpha, 0.0f), new GradientAlphaKey(alpha, 1.0f) }
        );
        lineRenderer.colorGradient = gradient;
        print(target);
        //RectTransform r = (RectTransform)target.transform;
        RectTransform r = (RectTransform)target.transform;
        Vector3[] v = new Vector3[4];
        r.GetWorldCorners(v);
        float x = target.transform.position.x + r.rect.width / 2;
        //print(x);
        //print(r.rect.width + " r.rect.width");
        //print(target.transform.position);
        float y = target.transform.position.y; //+ r.rect.height / 2;

        lineRenderer.positionCount = lengthOfLineRenderer;
        CanvasScaler canvasScaler = GameObject.Find("Canvas").gameObject.GetComponent<CanvasScaler>();
        float referenceWidth = canvasScaler.referenceResolution.x;
        float referenceHeight = canvasScaler.referenceResolution.y;
        float xOffset = 0.2f; // (Screen.width / referenceWidth);
        float yOffset = 0.2f; // (Screen.height / referenceHeight);

        int t_x = 1;
        if(target.transform.localScale.x < 0)
        {
            t_x = -1;
        }

        switch (corner)
        {
            case 0:
                xOffset *= t_x;
                break;
            case 1:
                xOffset *= t_x;
                yOffset *= -1;
                break;
            case 2:
                xOffset *= -t_x;
                yOffset *= -1;
                break;
            case 3:
                xOffset *= -t_x;
                break;
        }
        if (bobDirection)
        {
            lineRenderer.SetPosition(0, v[corner]);
            lineRenderer.SetPosition(1, new Vector3(v[corner].x + xOffset, v[corner].y, 0.0f));
            originalPos[0] = v[corner];
            originalPos[1] = new Vector3(v[corner].x + xOffset, v[corner].y, 0.0f);
        }
        else
        {
            lineRenderer.SetPosition(0, v[corner]);
            lineRenderer.SetPosition(1, new Vector3(v[corner].x, v[corner].y + yOffset, 0.0f));
            originalPos[0] = v[corner];
            originalPos[1] = new Vector3(v[corner].x, v[corner].y + yOffset, 0.0f);
        }
        //lineRenderer.SetPosition(2, new Vector3(x + 0.2f, target.transform.position.y + 0.2f, 0.0f));
        //print(lineRenderer.GetPosition(1));
        //print(lineRenderer.GetPosition(2));
        //lineRenderer.SetPosition(2, new Vector3(1, 1, 0.0f));
        //lineRenderer.SetPosition(3, new Vector3(1, 0, 0.0f));
        //lineRenderer.SetPosition(4, new Vector3(0, 0, 0.0f));
        //Destroy(this);
    }

    void Update()
    {
        LineRenderer lineRenderer = GetComponent<LineRenderer>();
        float t = Time.deltaTime * 0.1f;



        //updown += 0.1f;
        /*if (updown <= 0f && !bounce)
        {
            bounce = !bounce;
        }
        if (bounce && updown > 1f)
        {
            bounce = !bounce;
        }
        if (bounce)
        {
            updown -= 1f * t;
        } else
        {
            updown += 1f * t;
        }*/
        if (acceleration <= -1)
        {
            acceleration = 1;
            lineRenderer.SetPosition(0, originalPos[0]);
            lineRenderer.SetPosition(1, originalPos[1]);
            //bounce = !bounce;
        }

        movementAmount = acceleration * t;
        acceleration -= 0.01f;

        
        //print(movementAmount);

        for (int i = 0; i < lengthOfLineRenderer; i++)
        {
            float x = lineRenderer.GetPosition(i).x;
            float y = lineRenderer.GetPosition(i).y;
            if (!bobDirection)
            {
                switch (corner)
                {
                    case 0:
                        x = x - movementAmount;
                        break;
                    case 1:
                        x = x - movementAmount;
                        break;
                    case 2:
                        x = x + movementAmount;
                        break;
                    case 3:
                        x = x + movementAmount;
                        break;
                }
            }
            else
            {
                switch (corner)
                {
                    case 0:
                        y = y - movementAmount;
                        break;
                    case 1:
                        y = y + movementAmount;
                        break;
                    case 2:
                        y = y + movementAmount;
                        break;
                    case 3:
                        y = y - movementAmount;
                        break;
                }
            }
            lineRenderer.SetPosition(i, new Vector3(x, y, 0.0f));
            
        }

    }
}
