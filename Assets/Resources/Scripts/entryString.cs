﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class entryString : MonoBehaviour {

    string startingVal = null;
    public Button reset;
    public Text named;
    public InputField iF;
    public GameObject valid;
    MM_Main m;
    Effects e;
    string propName;
    // Use this for initialization
    public void init (string val, Effects _e, string _propName) {
		//onload grab name and inputfeild
        //set according to pass vals

        if(startingVal == null)
        {
            startingVal = val;
        }

        m = MM_Main.instance;
        reset = this.gameObject.transform.GetChild(0).GetComponent<Button>();
        named = this.gameObject.transform.GetChild(1).GetComponent<Text>();
        iF = this.gameObject.transform.GetChild(2).GetComponent<InputField>();
        valid = this.gameObject.transform.GetChild(3).gameObject;

        iF.text = val;
        named.text = _propName;
        e = _e;
        propName = _propName;
    }

    public void resVal()
    {
        iF.text = startingVal;
    }

    public void onChange()
    {
        //@@@THIS IS WHERE YOU WERE UP TO!
        //so the fade needs to be added when this thing is open
        //save load not in yet
        //also effect vars are not recorded or read for json files yet
        //also this mess below you needs to handle many var types
        //maybe have a little green tick if its valid for that var type?
        //close button does nothing
        if (e)
        {
            //e.GetType().GetField(propName).SetValue(e, iF.text);
            switch (e.GetType().GetField(propName).FieldType.Name)
            {
                case "String":
                    e.GetType().GetField(propName).SetValue(e, iF.text);
                    break;
                case "int":
                    // Console.WriteLine("Case 2");
                    break;
                case "GameObject":
                    GameObject g = Resources.Load(iF.text) as GameObject;
                    if(g)
                    {
                      e.GetType().GetField(propName).SetValue(e, iF.text);
                      valid.SetActive(true);
                    } else
                    {
                        valid.SetActive(false);
                    }
                    break;
                case "Sprite":
                    
                    Sprite s = Resources.Load(iF.text.ToString(), typeof(Sprite)) as Sprite;
                    Debug.Log(iF.text + " " + s);
                    if (s)
                    {
                        e.GetType().GetField(propName).SetValue(e, s);
                        valid.SetActive(true);
                    }
                    else
                    {
                        valid.SetActive(false);
                    }
                    break;
                case "bool":
                    // Console.WriteLine("Case 2");
                    break;
                default:
                    break;
            }
            
        }
    }

}
