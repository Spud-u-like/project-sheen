﻿[System.Serializable]
public class JaggedLayer {

    //public int[,] map;
    //public string[] tiles;
    //public string[] additional;
    //public string test = "ify";
    public IntArrayJson[] map;
    public string[] tiles;
    public string[] type;
    public StringArrayJson[] variables;
    public StringArrayJson[] data;
}

