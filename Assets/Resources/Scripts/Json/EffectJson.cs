﻿[System.Serializable]

public class EffectJson {

    public int gridX;
    public int gridY;
    public string effectName;
    public string[] variables;
    public string[] data;
}
