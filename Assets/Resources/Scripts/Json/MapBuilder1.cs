﻿using UnityEngine;
using SimpleJSON;
using System;

public class MapBuilder1  {
/*
    public int startX;
    public int startY;
    int[,] ground;
    GameObject[] groundTiles;
    int[][,] layers;
    GameObject[][] tiles;
    int[,] typeMap;
    string[] types;
    string[] e_type;
    int[][] e_values;
    int[] flipX;
    int[] flipY;
    int[][] npc;
    string[] npcType;
    TextAsset[] npcScript;
    JSONNode N;

    public bool init(string level)
    {
        try {
            if (level == null && N)
            {
                //reload saved level
            }
            else
            {
                TextAsset asset = Resources.Load(level) as TextAsset;
                N = JSON.Parse(asset.text);
            }
            Debug.Log("started loading JSON " + N.GetType());
            
            //get player starting X/Y Coords
            startX = N["startX"];
            startY = N["startY"];

            //Make the ground layer
            if (N["ground"].Count !=2)
            {
               throw new Exception("Can't load level, no Ground or Ground Malformed");
            }
            ground = new int[N["ground"]["map"].Count, N["ground"]["map"][0].Count];
            ground = assignCoords(ground, N, "ground", "map");
            if (N["ground"]["tilesFolder"])
            {
                groundTiles = getTiles(N["ground"]["tilesFolder"]);
            }
            else {
                groundTiles = new GameObject[N["ground"]["tiles"].Count];
                groundTiles = grabTilesetGround(groundTiles, N);
            }

            //Iterate through and add layers
            layers = new int[N["layers"].Count][,];
            tiles = new GameObject[N["layers"].Count][];
            for (int i = 0; i < N["layers"].Count; i++)
            {
                int[,] layer = new int[N["layers"][i]["map"].Count, N["layers"][i]["map"][0].Count];
                layer = assignTilesLayers(layer, N, i);
                if (N["layers"][i]["tilesFolder"])
                {
                    tiles[i] = getTiles(N["layers"][i]["tilesFolder"]);
                }
                else {
                    tiles[i] = new GameObject[N["layers"][i]["tiles"].Count];
                    tiles[i] = grabTilesetLayers(tiles[i], N, i);
                }
                layers[i] = layer;
            }

            //Change tile type
            typeMap = new int[N["typing"]["map"].Count, N["typing"]["map"][0].Count];
            typeMap = assignTyping(typeMap, N);
            types = new string[N["typing"]["types"].Count];
            types = getStrings(types, N, "typing", "types");

            //effect_cords, first two digits are the gridX/y to change, 
            //not sure if thats the neatest way of doing it, but it is the way I'm doing it
            e_type = new string[N["effects"]["effect_types"].Count];
            e_type = getStrings(e_type, N, "effects", "effect_types");
            e_values = new int[N["effects"]["effect_list"].Count][];
            e_values = assignArrayEffects(e_values, N);


            //add npcs


            npcType = new string[N["NPC"]["NPC_type"].Count];
            npcType = getStrings(npcType, N, "NPC", "NPC_type");
            npcScript = new TextAsset[N["NPC"]["NPC_script"].Count];
            npcScript = getJSON(npcScript, N, "NPC", "NPC_script");
            npc = new int[N["NPC"]["NPC_list"].Count][];
            npc = assignArrayNPC(npc, N);




    //Hoz Flip tiles
    flipX = new int[N["flipX"]["x"].Count];
            flipY = new int[N["flipX"]["y"].Count];
            flipX = getInts(flipX, N, "flipX", "x");
            flipY = getInts(flipY, N, "flipX", "y");

            return true;
        } catch (Exception e)
        {
            Debug.Log(e);
            return false;
        }
    }

    public Graph buildMap(String Mapname, bool preserveNPC, bool MM)
    {
        Graph outbound = new Graph(ground, groundTiles, startX, startY, Mapname, preserveNPC, MM);
        for (int i = 0; i < layers.Length; i++)
        {
            outbound = outbound.gen_layer(layers[i], tiles[i], outbound, MM);
        }
        outbound = outbound.change_layer_type(typeMap, types, outbound);
        outbound = outbound.add_effects_start(e_values, e_type, outbound, MM);
        //outbound = outbound.add_NPC_start(npc, npcType, npcScript, outbound);
        outbound = outbound.flip_hoz(flipX, flipY, outbound);
        return outbound;
    }

    public void setString(int x, int y, string tileName, int childCount)
    {
        try
        {
            //find layer
            //find tile name in tiles
            //not in tiles? add, and i becomes length+1
            //N["layers"][layer]["map"][i][j]
            int tileNumber = 0;
            Debug.Log("@1");
            if (childCount > 0)
            {
                //layers = "";
                if(childCount > N["layers"].Count)
                {
                    //addLayer();
                }
                tileNumber = inTiles(tileName, N["layers"][childCount]["tiles"], childCount);
                N["layers"][childCount]["map"][x][y] = tileNumber;
            } else
            {
                Debug.Log("@2");
                tileNumber = inTiles(tileName, N["ground"]["tiles"], childCount);
                Debug.Log("Before " + N["ground"]["map"][x][y]);
                N["ground"]["map"][x][y] = tileNumber;
                Debug.Log("After " + N["ground"]["map"][x][y]);
            }
        } catch (Exception e)
        {
            Debug.Log(e);
            return;
        }

    }

    int inTiles(string tileName, JSONNode tiles, int childCount)
    {
        int c = 0;
        //tiles = new string[1];
        for (c = 0; c < tiles.Count; c++)
        {
            if(tiles[c] == tileName)
            {
                Debug.Log("@3");
                //N["layers"][childCount]["tiles"] = tiles;
                //N["ground"]["tiles"] = tiles;
                return c;
            }
        }
        c++;
        tiles.Add(tileName);
        Debug.Log("@4");
        N["ground"]["tiles"] = tiles;
        return c;
    }

    int[,] assignCoords(int[,] map, SimpleJSON.JSONNode N, string params1, string params2)
    {
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                map[i, j] = N[params1][params2][i][j];
            }
        }
        return map;
    }

    int[][] assignArrayEffects(int[][] map, SimpleJSON.JSONNode N)
    {
        for (int i = 0; i < map.GetLength(0); i++)
        {
            int[] a = new int[N["effects"]["effect_list"][i].Count];

            for (int j = 0; j < a.Length; j++)
            {
                a[j] = N["effects"]["effect_list"][i][j];
            }
            map[i] = a;
        }
        return map;
    }

    int[][] assignArrayNPC(int[][] map, SimpleJSON.JSONNode N)
    {
        for (int i = 0; i < map.GetLength(0); i++)
        {
            int[] a = new int[N["NPC"]["NPC_list"][i].Count];

            for (int j = 0; j < a.Length; j++)
            {
                a[j] = N["NPC"]["NPC_list"][i][j];
            }
            map[i] = a;
        }
        return map;
    }

    GameObject[] grabTilesetGround(GameObject[] tiles, SimpleJSON.JSONNode N)
    {
        for (int i = 0; i < tiles.GetLength(0); i++)
        {
            Debug.Log(N["ground"]["tiles"][i]);
            tiles[i] = (GameObject)Resources.Load(N["ground"]["tiles"][i], typeof(GameObject));
        }
        return tiles;
    }

    int[,] assignTilesLayers(int[,] map, SimpleJSON.JSONNode N, int layer)
    {
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                map[i, j] = N["layers"][layer]["map"][i][j];
            }
        }
        return map;
    }

    GameObject[] grabTilesetLayers(GameObject[] tiles, SimpleJSON.JSONNode N, int layer)
    {
        for (int i = 0; i < tiles.GetLength(0); i++)
        {
            tiles[i] = (GameObject)Resources.Load(N["layers"][layer]["tiles"][i], typeof(GameObject));
        }
        return tiles;
    }

    private GameObject[] getTiles(String st)
    {
        GameObject[] tiles = Resources.LoadAll<GameObject>(st);
        Array.Sort(tiles, delegate (GameObject wp1, GameObject wp2) { return wp1.name.CompareTo(wp2.name); });
        return tiles;
    }

    int[,] assignTyping(int[,] map, SimpleJSON.JSONNode N)
    {
        for (int i = 0; i < map.GetLength(0); i++)
        {
            for (int j = 0; j < map.GetLength(1); j++)
            {
                map[i, j] = N["typing"]["map"][i][j];
            }
        }
        return map;
    }

    private string[] getStrings(string[] st, SimpleJSON.JSONNode N, string param1, string param2)
    {
        for (int j = 0; j < st.GetLength(0); j++)
        {
            st[j] = N[param1][param2][j];
        }
        return st;
    }

    private TextAsset[] getJSON(TextAsset[] st, SimpleJSON.JSONNode N, string param1, string param2)
    {
        for (int j = 0; j < st.GetLength(0); j++)
        {
            st[j] = Resources.Load(N[param1][param2][j]) as TextAsset;
        }
        return st;
    }

    private int[] getInts(int[] st, SimpleJSON.JSONNode N, string param1, string param2)
    {
        for (int j = 0; j < st.GetLength(0); j++)
        {
            st[j] = N[param1][param2][j];
        }
        return st;
    }


    //This looks like a good way to remote load the json file
    /*    IEnumerator SendRequest()
        {
            WWW request = new WWW("http://localhost:9999/post/results.json");


            yield return request;

            if (request.error == null || request.error == "")
            {

                var N = JSON.Parse(request.text);

                Debug.Log(N["projects"][0]["name"]);

            }
            else
            {
                Debug.Log("WWW error: " + request.error);
            }
        }
    */
}
