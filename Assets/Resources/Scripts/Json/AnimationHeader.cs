﻿[System.Serializable]
public class AnimationHeader
{
    public string name;
    public string spriteSheet;
    public float fps;
    public bool flipX;
    public bool flipY;
    public AnimationCords[] cords;
}

