﻿using UnityEngine;
using SimpleJSON;
using System;
using System.IO;
using System.Reflection;
using System.Collections.Generic;

public class MapBuilder  {

    public int startX;
    public int startY;
    int[,] ground;
    GameObject[] groundTiles;
    Layer[] layers;
    GameObject[][] tiles;
    Layer types;
    JaggedLayer effects;

    Layer flipX;
    Layer flipY;
    NpcHolder npc;
    JSONNode N;

    public Map map;

    private string gameDataProjectFilePath = "/StreamingAssets/Maps/";
    private string gameDataProjectFilePathNPC = "/StreamingAssets/Maps/";

    private List<String> spritesheets;

    public GameObject savedPreloader;

    int limiterX = 5;
    int limiterY = 5;

    public bool init(string level)
    {
        try {


            /*
                alright, heres where you left off

                @DONE@ flipx/y dont work get (go to graph and fix that)
                npc doesn't work
                the npc.json file is incomplete (thanks unity, you shitbag)
                you need to add a bunch of options to the file
                also you broke some of the MM (specifically the mousedown)
                when you rewrote all this shit
                undo does nothing
                You need to add flip x + Flip Y back into the save function
                @DONE@ finally saving maps still doesnt work, or it does but isnt hooked up to anything
            */
            //Debug.Log(File.Exists(Application.dataPath + gameDataProjectFilePath));

            //string dataAsJson = File.ReadAllText(Application.dataPath + gameDataProjectFilePath);
            //Map map = JsonUtility.FromJson<Map>(dataAsJson);
            //Debug.Log(map.startX);
            //Debug.Log(map.layers[0].map[0].i[0]);
            //Debug.Log(map.groundOut);

            //string dataAsJson = JsonUtility.ToJson(map);
            //string filePath = Application.dataPath + gameDataProjectFilePath + level + ".json";
            //File.WriteAllText(filePath, dataAsJson);
            string dataAsJson = File.ReadAllText(Application.dataPath + gameDataProjectFilePath + level + ".json");
            init_build(dataAsJson);

            return true;
        } catch (Exception e)
        {
            Debug.Log("level: " + level);
            Debug.Log("Full Path: " + Application.dataPath + gameDataProjectFilePath + level + ".json");
            Debug.Log(e);
            return false;
        }
    }

    public void init_build(string dataAsJson)
    {
        map = JsonUtility.FromJson<Map>(dataAsJson);
    }

    public string save(Graph g, int lx = 0, int ly = 0)
    {
        //GameObject saveme = new GameObject();
        //saveme.AddComponent<Graph>();
        //GraphHolder GH = saveme.AddComponent<GraphHolder>();
        //GH.graph = g;
        //AssetDatabase.CreateAsset(saveme, "assets/GHMAP");
        //PrefabUtility.CreatePrefab("StreamingAssets/test.prefab", saveme);
        if (lx == 0)
        {
            limiterX = g.nodes.GetLength(0);
        }
        else
        {
            limiterX = lx;
        }

        if (ly == 0)
        {
            limiterY = g.nodes.GetLength(1);
        }
        else
        {
            limiterY = ly;
        }


        map = new Map();
        int highpoint = 0; //tile with the most children
        spritesheets = new List<string>();
        //[0,1]

        //ground//
        map.startX = g.startX; 
        map.startY = g.startY;
        map.cameraLock = g.cameraLock;
        map.ground = new Layer();
        //map.ground.map = new ArrayOfIntArraysJson[g.nodes.GetLength(0)];
        map.ground.map = new ArrayOfIntArraysJson[limiterX];
        map.ground.tiles = new string[0];
        for (int k =0; k < map.ground.map.Length; k++)
        {
            map.ground.map[k] = new ArrayOfIntArraysJson();
            map.ground.map[k].i = new IntArrayJson[limiterY];
            for (int h = 0; h < map.ground.map[k].i.Length; h++)
            {
                //Debug.Log("k " +g.nodes.GetLength(0) + " " + k);
                //Debug.Log("h " + g.nodes.GetLength(1) + " " + h);
                if (g.nodes.GetLength(0) <= k || g.nodes.GetLength(1) <= h)
                {
                    //Layer checkTileExists(Node n, string[] s, Layer layer, int k, int h, int arrayOffset, int flips, int gridXOffset, int gridYOffset, int isRoof)
                    map.ground.map[k].i[h] = new IntArrayJson();
                    map.ground.map[k].i[h].i = new int[5] { -1, 0,0,0,0 };
                    continue;
                }
                //Debug.Log(g.nodes[k, h]);
                map.ground = checkTileExists(g.nodes[k,h], map.ground.tiles, map.ground, k, h, 0, flipCheck(g.nodes[k, h].gameObject.GetComponent<SpriteRenderer>()), 0, 0, 0);   
                if(g.nodes[k, h].transform.childCountIgnoringNodeless() > highpoint)
                {
                    highpoint = g.nodes[k, h].transform.childCountIgnoringNodeless(); 
                }         
            }

        }
        //////////
        //layers//

        map.layers = new Layer[highpoint];
        for(int q = 0; q < highpoint; q++)
        {
            Layer layer = new Layer();
            layer.map = new ArrayOfIntArraysJson[limiterX];
            layer.tiles = new string[0];
            for (int k = 0; k < layer.map.Length; k++)
            {
                layer.map[k] = new ArrayOfIntArraysJson();
                layer.map[k].i = new IntArrayJson[limiterY];
                for (int h = 0; h < layer.map[k].i.Length; h++)
                {

                    if (g.nodes.GetLength(0) <= k || g.nodes.GetLength(1) <= h)
                    {
                        layer.map[k].i[h] = new IntArrayJson();
                        layer.map[k].i[h].i = new int[2] { 0, 0 };
                        continue;
                    }

                    Node n = g.nodes[k, h];


					//to offset child objects that are part of the node but are not themselves nodes
					//int offset = 0;
					//int cc = n.transform.childCount;
					//while(q + offset < cc && !n.transform.GetChild(q+ offset).GetComponent<Node>())
					//{
					//   offset++;
					//}
					//Debug.Log(n.transform.childCountIgnoringNodeless() + " Layer:" + q + " Offset:" + offset + " ChildCount:" + n.transform.childCount + " " + n.transform.name);
					//Debug.Log(n.transform.name + " " + q);

					if (q >= n.transform.childCountIgnoringNodeless() || n.transform.childCountIgnoringNodeless() == 0 || n.transform.getChildIgnoringNodeless(q) == null)
                    {
                        layer.map[k].i[h] = new IntArrayJson();
                        layer.map[k].i[h].i = new int[2] { 0, 0 };
                        continue;
                    }
                    //Debug.Log(n.transform.getChildIgnoringNodeless(q));
                    //Debug.Log(n.transform.GetChild(q+ offset));
                    //Node n = g.nodes[k, h].transform.GetChild(q).GetComponent<Node>();
                    //if (n.transform.GetChild(q).GetComponent<Node>()) //if it doesn't have a node component ignore it
                    //{
						
						int isRoof = n.transform.getChildIgnoringNodeless(q).GetComponent<Node>().isRoof ? 1 : 0;
                    //Debug.Log("Saving roof " + isRoof + " " + n.transform.GetChild(q+ offset).GetComponent<Node>().isRoof + " " + n.transform.GetChild(q+ offset).name);
                        Node checkingThisNode = n.transform.getChildIgnoringNodeless(q).GetComponent<Node>();
                    //Debug.Log("Grid offset : " + checkingThisNode.offsetX + " " + checkingThisNode.offsetY);
                        layer = checkTileExists(checkingThisNode, layer.tiles, layer, k, h, 1, flipCheck(n.transform.getChildIgnoringNodeless(q).GetComponent<SpriteRenderer>()), checkingThisNode.offsetX, checkingThisNode.offsetY, isRoof);
                    //} 
                }

            }
            map.layers[q] = layer;
        }
        //////////
        //types///

        types = new Layer();
        types.map = new ArrayOfIntArraysJson[limiterX];
        types.tiles = new string[0];

        for (int k = 0; k < types.map.Length; k++)
        {
            types.map[k] = new ArrayOfIntArraysJson();
            types.map[k].i = new IntArrayJson[limiterY];
            for (int h = 0; h < types.map[k].i.Length; h++)
            {
                if (g.nodes.GetLength(0) <= k || g.nodes.GetLength(1) <= h)
                {
                    types.map[k].i[h] = new IntArrayJson();
                    types.map[k].i[h].i = new int[1] { 0 };
                    continue;
                }
                //Debug.Log(g.nodes[k, h])
                types = checkTypeExists(g.nodes[k, h], types.tiles, types, k, h, 1);
            }

        }
        map.types = types;

        //////////
        //effects/

        /*effects = new JaggedLayer();
        effects.map = new IntArrayJson[0];
        effects.tiles = new string[0];
        effects.variables = new StringArrayJson[0];
        effects.data = new StringArrayJson[0];*/

        EffectJsonHolder effects = new EffectJsonHolder();
        int totalEffects = 0;

        for (int k = 0; k < limiterX; k++)
        {
            for (int h = 0; h < limiterY; h++)
            {
                if (g.nodes.GetLength(0) <= k || g.nodes.GetLength(1) <= h)
                {
                    continue;
                }
                //if there are effects on the node
                if (g.nodes[k, h].gameObject.GetComponents<Effects>().Length > 0)
                {
                    for (int j = 0; j < g.nodes[k, h].gameObject.GetComponents<Effects>().Length; j++)
                    {
                        totalEffects++;
                    }
                }
            }
        }
        effects.e = new EffectJson[totalEffects];
        totalEffects = 0;
        for (int k = 0; k < limiterX; k++)
        {
            for (int h = 0; h < limiterY; h++)
            {
                //if there are effects on the node
                if (g.nodes.GetLength(0) <= k || g.nodes.GetLength(1) <= h)
                {
                    continue;
                }
                int numberOfEffects = g.nodes[k, h].gameObject.GetComponents<Effects>().Length;
                if (numberOfEffects > 0)
                {
                    //List<string> eNames = new List<string>();
                    for (int j = 0; j < numberOfEffects; j++)
                    {
                        //effects.e[totalEffects].effectName = g.nodes[k,h].gameObject.GetComponents<Effects>()[j].ToString();
                        Effects e = g.nodes[k, h].gameObject.GetComponents<Effects>()[j];
                        //eNames.Add(e.ToString().Split('(', ')')[1]);
                        //Debug.Log(e);
                        //Debug.Log(totalEffects);
                        //Debug.Log(effects.e.Length);
                        List<string> variables = new List<string>();
                        List<string> data = new List<string>();
                        effects.e[totalEffects] = new EffectJson();
                        effects.e[totalEffects].gridX = h;
                        effects.e[totalEffects].gridY = k;
                        effects.e[totalEffects].effectName = (e.ToString().Split('(', ')')[1]);
                        Debug.Log("Effect Name: " + e.ToString().Split('(', ')')[1]);
                        //effects.e[totalEffects] = ripVars(effects.e[totalEffects], g.nodes[k,h].gameObject.GetComponents<Effects>()[j]);
                        GetVarsFromScript gvfs = new GetVarsFromScript();
                        FieldInfo[] fields = gvfs.getFields(e);

                        foreach (var prop in fields)
                        {
                            //Debug.Log(prop.Name);
                            string n = e.GetType().GetField(prop.Name).FieldType.Name;
                            if (n == "String")// || n == "Int" || n == "GameObject" || n == "Sprite" || n == "Bool")
                            {
                                variables.Add(prop.Name.ToString());
                                data.Add((e.GetType().GetField(prop.Name).GetValue(e)).ToString());
                            }
                        }
                        effects.e[totalEffects].variables = new string[variables.Count];
                        effects.e[totalEffects].data = new string[data.Count];
                        effects.e[totalEffects].variables = variables.ToArray();
                        effects.e[totalEffects].data = data.ToArray();
                        totalEffects++;
                    }               
                }             
            }
        }

        map.effects = effects;

        //////////
        //XYFLIP//

        

        //////////
        //NPC////
        NpcHolder[] holder = new NpcHolder[0];

        for (int k = 0; k < limiterX; k++)
        {
            for (int h = 0; h < limiterY; h++)
            {
                if (g.nodes.GetLength(0) <= k || g.nodes.GetLength(1) <= h)
                {
                    continue;
                }
                if (g.nodes[k, h].npc)
                {
                    NpcHolder n = new NpcHolder();
                    n.startX = h;
                    n.startY = k;
                    n.sprite = g.nodes[k, h].npc.name;
                    n.dStart = 0;
                    n.c = g.nodes[k, h].npc.GetComponent<Npc>().JSONName;
                    NpcHolder[] holderRep = new NpcHolder[holder.Length+1];
                    for(int j =0; j < holder.Length; j++)
                    {
                        holderRep[j] = holder[j];
                    }
                    holderRep[holderRep.Length - 1] = n;
                    holder = holderRep;
                }
            }

        }
        map.spriteSheets = spritesheets.ToArray();
        map.npc = holder;
        string dataAsJson = JsonUtility.ToJson(map);
        
        return dataAsJson;
    }

    public bool saveWrite(string dataAsJson, string level)
    {
        try
        {
            string filePath = Application.dataPath + gameDataProjectFilePath + level + ".json";
            File.WriteAllText(filePath, dataAsJson);
            return true;
        }
        catch (Exception e)
        {
            Debug.Log(e);
            return false;
        }
    }

    Layer checkTileExists(Node n, string[] s, Layer layer, int k, int h, int arrayOffset, int flips, int gridXOffset, int gridYOffset, int isRoof)
    {
        string[] o = new string[s.Length + 1];
        layer.map[k].i[h] = new IntArrayJson();
        //Debug.Log(n.spritesheetName + "/" + n.nodeName);
        for (int i = 0; i < s.Length; i++)
        {
            //Debug.Log(s[i]);
            if (s[i] == n.spritesheetName +"/" + n.nodeName)
            {
                //layer.map[k].i[h] = i + offset;
                layer.map[k].i[h].i = new int[5] { i + arrayOffset, flips, gridXOffset, gridYOffset, isRoof};
                return layer;
            }
            o[i] = s[i];
        }
        
        o[o.Length - 1] = n.spritesheetName + "/" + n.nodeName;
        layer.tiles = o;
        addToSpriteSheets(n.spritesheetName);
        //Debug.Log(layer.map[k]);
        //Debug.Log(layer.map[k].i);
        //Debug.Log(layer.map[k].i.Length);
        //Debug.Log(h);
        //Debug.Log(layer.map[k].i[0]);
        //Debug.Log(layer.map[k].i[h].i);
        //layer.map[k] = new ArrayOfIntArraysJson();
        //layer.map[k].i = new IntArrayJson[g.nodes.GetLength(1)];

        layer.map[k].i[h].i = new int[5] { o.Length + arrayOffset - 1, flips, gridXOffset, gridYOffset, isRoof};
        return layer;
    }

    void addToSpriteSheets(string name)
    {
        if (spritesheets.IndexOf(name) < 0)
        {
            spritesheets.Add(name);
        }
    }

    Layer checkTypeExists(Node n, string[] listOfTiles, Layer m, int k, int h, int offset)
    {
        //.transform.childCount
        //string[] o = new string[listOfTiles.Length + 1];
        int[] tilesByNumber = new int[n.transform.childCountIgnoringNodeless() +1];
        if(n.gridX == 6 && n.gridY == 5)
        {
            Debug.Log("Base Tile " + n.landtype + " " + n.name);
            //Debug.Log(doesTypeExist(n, listOfTiles));
        }
        int num = doesTypeExist(n, listOfTiles);
        if(num>=0)
        {
            tilesByNumber[0] = num;
        } else
        {
            string[] temp = new string[listOfTiles.Length + 1];
            listOfTiles.CopyTo(temp, 0);
            listOfTiles = temp;
            listOfTiles[listOfTiles.Length - 1] = n.landtype;
            tilesByNumber[0] = listOfTiles.Length - 1;
        }

        m.map[k].i[h] = new IntArrayJson();
		int trueCount = 1;
        for (int i = 0; i < n.transform.childCount; i++)
        {
			if(!n.transform.GetChild(i).GetComponent<Node>()){
				continue;
			}

            Node currentNode = n.transform.GetChild(i).GetComponent<Node>();
            if (!currentNode)
            {
                continue;
            }
            if(currentNode.gridX == 6 && currentNode.gridY == 5)
            {
                //Debug.Log("anustopia " + currentNode.landtype + " " + currentNode.name + " " + n.transform.childCount);
            }
            num = doesTypeExist(currentNode, listOfTiles);
            if (num >=0)
            {
                tilesByNumber[trueCount] = num;
            } else
            {

                string[] temp = new string[listOfTiles.Length + 1];
                listOfTiles.CopyTo(temp, 0);
                listOfTiles = temp;
                listOfTiles[listOfTiles.Length - 1] = currentNode.landtype;
                tilesByNumber[trueCount] = listOfTiles.Length - 1;
            }
			trueCount++;
        }
        m.tiles = listOfTiles;
        m.map[k].i[h].i = tilesByNumber;
        return m;
        /*
        for (int i = 0; i < listOfTiles.Length; i++)
        {
            if (listOfTiles[i] == n.landtype)
            {
                m.map[k].i[h].i = new int[1] { i + offset };
                return m;
            }
            o[i] = listOfTiles[i];
        }

        o[o.Length - 1] = n.landtype;
        m.tiles = o;
        m.map[k].i[h].i = new int[1] { o.Length + offset - 1 };
        return m;*/
    }

    int doesTypeExist (Node n, string[] listOfTiles)
    {
        for (int i = 0; i < listOfTiles.Length; i++)
        {
            if (listOfTiles[i] == n.landtype)
            {
                
                return i;
            }
            
        }
        return -1;
    }

    JaggedLayer checkEffectExists(Effects[] listOfEffectsOnTile, int[] replacer, string[] stringNameOfAllEffects, JaggedLayer effectObject, int k, int h, int offset)
    {
        string[] o = new string[stringNameOfAllEffects.Length + 1];
        int r_place = 0;     
        for (int p = 0; p < listOfEffectsOnTile.Length; p++)
        {
            bool found = false;
            for (int i = 0; i < stringNameOfAllEffects.Length && !found; i++)
            {
                string output = listOfEffectsOnTile[p].ToString().Split('(', ')')[1];
                Debug.Log("effects names " + listOfEffectsOnTile[p] + " " + stringNameOfAllEffects[i] + " " + output);
                if (stringNameOfAllEffects[i] == output)
                {
                    replacer[r_place + 1] = i + offset;
                    r_place++;
                    found = true;
                }
                o[i] = stringNameOfAllEffects[i];
            }
            if(!found)
            {
                o[o.Length - 1] = listOfEffectsOnTile[p].GetType().ToString();
                replacer[r_place + 2] = o.Length - 1;
            }
            
        }

        effects = new JaggedLayer();
        effects.map = new IntArrayJson[effectObject.map.Length + 1];
        for (int i =0; i < effectObject.map.Length;i++)
        {
            effects.map[i] = effectObject.map[i];
        }
        effects.map[effects.map.Length - 1] = new IntArrayJson();
        effects.map[effects.map.Length - 1].i = replacer;
        effects.tiles = o;
        Debug.Log("effects.tiles " + effects.tiles[0] + " " + effects.tiles.Length);
        return effects;
    }

    /*EffectJson ripVars(EffectJson eLines, Effects e)
    {
        GetVarsFromScript g = new GetVarsFromScript();
        FieldInfo[] fields = g.getFields(e);
        List<string> variables = new List<string>();
        List<string> data = new List<string>();
        foreach (var prop in fields)
        {
            Debug.Log(prop.Name);
            string n = e.GetType().GetField(prop.Name).FieldType.Name;
            if (n == "String" || n == "Int" || n == "GameObject" || n == "Sprite" || n == "Bool")
            {
                variables.Add(prop.Name.ToString());
                data.Add((e.GetType().GetField(prop.Name).GetValue(e)).ToString());
            }
        }
        eLines.variables
        return eLines;
    }*/


    /*public int[,] combineArrays(ArrayOfIntArraysJson[] _a)
    {
        int[,] outbound = new int[_a.Length, _a[0].i.Length];
        for (int k = 0; k < _a.Length; k++)
        {
            for (int l = 0; l < _a[k].i.Length; l++)
            {
                outbound[k, l] = _a[k].i[l].i[0];
            }
        }
        return outbound;
    }*/

    public int[,][] combineArrays2D(ArrayOfIntArraysJson[] _a)
    {
        int[,][] outbound = new int[_a.Length, _a[0].i.Length][];
        for (int k = 0; k < _a.Length; k++)
        {
            for (int l = 0; l < _a[k].i.Length; l++)
            {
                //Debug.Log(_a[k].i[l]);
                //Debug.Log(_a[k].i[l].i);
                //Debug.Log(_a[k].i[l].i[0]);
                outbound[k, l] = _a[k].i[l].i;
            }
        }
        return outbound;
    }

    public int[][] combineArraysStack(IntArrayJson[] _a)
    {
        int[][] outbound = new int[_a.Length][];
        for (int i = 0; i < _a.GetLength(0); i++)
        {
            outbound[i] = _a[i].i;
        }
        return outbound;
    }

    public GameObject[] preloadGameobjects(string[] t)
    {
        GameObject[] tiles = new GameObject[t.Length];

        
            folderList f = new folderList();
        string[] folderName = f.folderNames;
        for (int k = 0; k < t.Length; k++)
        {
            //tiles[k] = (GameObject)Resources.Load(t[k], typeof(GameObject));

            
            for (int a =0; a< folderName.Length; a++)
            {
                tiles[k] = (GameObject)Resources.Load("Tiles/"+folderName[a]+"/" + t[k], typeof(GameObject));
                if (tiles[k] != null)
                {
                    break;
                }
            }
            if (tiles[k] == null)
            {
                Debug.Log("couldn't load tile: " + t[k]);
            }
        }
        return tiles;      
    }



    public void buildMap(string Mapname, bool preserveNPC, string Maptype, Main m, GameObject preloader)
    {
        //Debug.Log("camera Lock " + map.cameraLock);
        //Graph outbound = new Graph(combineArrays2D(map.ground.map), preloadGameobjects(map.ground.tiles), map.startX, map.startY, Mapname, preserveNPC, Maptype, false, map.cameraLock);
        if(preloader == null)
        {
            preloader = savedPreloader;
        } else
        {
            savedPreloader = preloader;
        }
        Graph outbound = new Graph(map.ground.tiles, combineArrays2D(map.ground.map), preloader, map.startX, map.startY, Mapname, preserveNPC, Maptype, false, map.cameraLock);
        

        for (int i = 0; i < map.layers.Length; i++)
        {
			Debug.Log("Layer Number: " +i);
            outbound = outbound.gen_layer(map.layers[i].tiles, combineArrays2D(map.layers[i].map), preloader, outbound, i, Maptype);
        }
        outbound = outbound.change_layer_type(combineArrays2D(map.types.map), map.types.tiles, outbound);

        /*
            public int gridX;
            public int gridY;
            public string effectName;
            public StringArrayJson[] variables;
            public StringArrayJson[] data;
        */
        outbound = outbound.add_effects_start(map.effects.e, outbound, Maptype);
        //outbound = outbound.add_effects_start(combineArraysStack(map.effects.map), map.effects.tiles, outbound, MM);

        CharJsonHolder[] jsonList = new CharJsonHolder[map.npc.Length];

        for(int j = 0; j < map.npc.Length; j++)
        {
            jsonList[j] = buildNpc(map.npc[j].c);
        }

        outbound = outbound.add_NPC_start(map.npc, jsonList, outbound);
        //outbound = outbound.flip_tiles(combineArrays(map.flipX.map), combineArrays(map.flipY.map), outbound);
        m.finishBuild(outbound);
        //return outbound;
    }

    public void buildMapInit(string Mapname, bool preserveNPC, string Maptype, Main m)
    {
        GameObject pre = new GameObject();
        //pre.SetActive(false);
        Preloader loader = pre.AddComponent<Preloader>();
        loader.init(map.spriteSheets, this, Mapname, preserveNPC, Maptype, m);
    }

    public CharJsonHolder buildNpc (string name)
    {
        /*CharJson single = new CharJson();
        single.type = "sound";
        single.name = "ff1";

        CharJson[] c = new CharJson[1] { single };

        CharJsonHolder holder = new CharJsonHolder();
        holder.j = c;
        
        string dataAsJson = JsonUtility.ToJson(holder);
        Debug.Log(dataAsJson);
        string filePath = Application.dataPath + gameDataProjectFilePathNPC;
        File.WriteAllText(filePath, dataAsJson);*/
        string dataAsJson = File.ReadAllText(Application.dataPath + gameDataProjectFilePathNPC + name + ".json");
        CharJsonHolder holder = JsonUtility.FromJson<CharJsonHolder>(dataAsJson);
        return holder;
    }

    int flipCheck(SpriteRenderer s)
    {
    	//0, no flip, 1 is x, 2 is y, 3 is x and y
        int flip = 0;
        if (s.flipX)
        {
            flip++;
        }
        if (s.flipY)
        {
            flip += 2;
        }
        return flip;
    }


    //This looks like a good way to remote load the json file


    /*    IEnumerator SendRequest()
        {
            WWW request = new WWW("http://localhost:9999/post/results.json");


            yield return request;

            if (request.error == null || request.error == "")
            {

                var N = JSON.Parse(request.text);

                Debug.Log(N["projects"][0]["name"]);

            }
            else
            {
                Debug.Log("WWW error: " + request.error);
            }
        }
    */

  
}
