﻿[System.Serializable]
public class NodeHolder
{
    public string nodeName;
    public float offsetX;
    public float offsetY;
    public bool flipX;
    public bool flipY;
    public string layergroup;
    public string landType;
    public string[] effects;

    public int x;
    public int y;
    public int width;
    public int height;
    public string spritesheetName;
    public string tag;
}

