﻿[System.Serializable]

public class NpcActions
{
    public string type;
    public string name;
    public string text;
    public int x;
    public int y;
    public int value;
    public int[] path; //used for things like choices
}
