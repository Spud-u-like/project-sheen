﻿[System.Serializable]
public class NPC
{
    //public int startX;
    //public int startY;
    //public int dStart;
    //public string c; //class
    public string characterName;
    public string characterClass;
    public int level;
    public float exp;
    public int hp;
    public int mp;
    public int attack;
    public int defence;
    public int agility;
    public int movement;
    public string weaponType;
    public int kills;
    public int defeats;
    public string promoteName;
    public string defaultText;
    public float[] weaponOffsetx;
    public float[] weaponOffsety;
    public float[] weaponOffsetr;

    public string[] equipment;
    public string[] spells;

    public string[] animationNames;
    public AnimationHeader[] animationHeaders;
}

