﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Settings : MonoBehaviour
    
{
    public string map;
    public bool editor = true;
    private static Settings settings;
    void Awake()
    {

        DontDestroyOnLoad(this.gameObject);
        if (settings == null)
        {
            settings = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

}