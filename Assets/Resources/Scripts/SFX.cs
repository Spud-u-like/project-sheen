﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFX : MonoBehaviour {

    // Use this for initialization

    public AudioClip Clip;
    AudioSource audioSource;

    AudioClip clip1 = (AudioClip)Resources.Load("Sounds/cube_release");
    AudioClip clip2 = Resources.Load<AudioClip>("Sounds/cube_up");
    AudioClip clip3 = Resources.Load("Sounds/cube_onslot", typeof(AudioClip)) as AudioClip;

    void Start () {
        audioSource = GetComponent<AudioSource>();
        audioSource.Play();
        audioSource.clip = clip1;
    }
	
	// Update is called once per frame
	void Update () {
        if (!audioSource.isPlaying)
        {
            Destroy(this);
        }
    }
}
