﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Rendering;



public class MM_MouseDown : MonoBehaviour


{
    MM_Main m;
    public bool child = false;
    //bool coRunning = false;
    public bool mousewait = true;
    public GameObject lineHolder;
    public bool beingCopied = false;

    private void Start()
    {
        m = MM_Main.instance;
        lineHolder = m.lineHolder;
        //m.mouseDownList.Add(this);
    }

    void copiedCellsHandler(GameObject gO)
    {
        if (m.copiedTiles)
        {
            GameObject firstTile = m.copiedTiles.transform.GetChild(0).gameObject;
            m.copiedTiles.transform.position = gO.transform.position;
            firstTile.GetComponent<Node>().gridX = gO.GetComponent<Node>().gridX;
            firstTile.GetComponent<Node>().gridY = gO.GetComponent<Node>().gridY;
            //print(gO.GetComponent<Node>().gridX + " " + gO.GetComponent<Node>().gridY);
            //print(firstTile.GetComponent<Node>().gridX + " " + firstTile.GetComponent<Node>().gridY);
        }
    }

    private void Update()
    {
        if (!m.mouseDown && mousewait)
        {
            //print("called mouse up");
            mousewait = false;
        }
    }

    void OnMouseOver()
    {
        //awful mouseover code

        if (m.copiedTiles && !beingCopied)
        {
            //if there are tiles being copied and you ARE NOT a tile being copied
            //and you are NOT a child (so the bottom tile) then set the pos of the copiedTiles to your pos (snap too)
            if (!child)
            {
              copiedCellsHandler(this.gameObject);

            }
        }


        if (m.mouseDown && !mousewait && !m.mousewait)
        {
            //if mousedown and this tile isn't waiting and the main hasn't told you to wait
            print("called mouse down");        
            /*if (child && !transform.parent.GetComponent<MM_MouseDown>().mousewait)
            {
                transform.parent.GetComponent<MM_MouseDown>().MouseHit();
                return;
            }*/
            mousewait = true;
            MouseHit();
        }

        /*if (!Input.GetMouseButton(0))
        {
            mousewait = true;
        }*/

        if (!m.mouseDown && m.Select.isOn && m.selectDragging && !m.copiedTiles)
        {
            m.selectDragging = false;
            lineHolder.GetComponent<Marque>().interact(this.gameObject);
        }
    }

    public void MouseHit()
    {

        //should also check for active m.fading
        //Debug.Log("test");

        //
        /*lineToMouse[] ltmArray = lineHolder.GetComponentsInChildren<lineToMouse>();
        //print(ltmArray.Length + " length of ltmarray");
        foreach (lineToMouse l in ltmArray)
        {
            l.resetPoints(transform.position);
        }*/

        if (beingCopied && !m.mousewait)
        {
            m.mousewait = true;
            m.startCopy(GetComponent<Node>().gridX, GetComponent<Node>().gridY);
            return;
        }

        if (EventSystem.current.IsPointerOverGameObject())
        {
            Debug.Log("Mouse down");
            return;
        }

        if(m.StartLocation.isOn)
        {
            m.graph.startX = GetComponent<Node>().gridX;
            m.graph.startY = GetComponent<Node>().gridY;
            m.startLocationToken.transform.position = this.transform.position;
        }

        if (m.Select.isOn)
        {
            if(m.selectDragging)
            {
                return;
            }
            m.selectDragging = true;
            lineHolder.GetComponent<Marque>().interact(this.gameObject);
            return;
        }

        if (m.tilesheetDropDown.transform.childCount == 4) //== 4 means "is the dropdown open", yes I'm aware how mad that is
        {
            return;
        }
        
        if (m.Fill.isOn)
        {
            Node oldNode = this.GetComponent<Node>();
            //if (oldNode.gameObject.name == m.dropdown.options[m.dropdown.value].text + "(Clone)")
            //print(" " + oldNode.gameObject.name + " " + m.swatchName);
            //print(oldNode.gameObject.name == m.swatchName);
            if (oldNode.gameObject.name == m.swatchName)
            {
                return;
            }
            findSimilarTiles(m.graph, oldNode.gridX, oldNode.gridY, oldNode.gameObject.name);
        }
        if (m.Brush.isOn)
        {
			if(child){
				//transform.parent.GetComponent<MM_MouseDown>().replaceTile(m.layer.isOn, false);
			} else{
				replaceTile(m.layer.isOn, false);
			}
        }
        if (m.Arrow.isOn)
        {
            arrowStart();
        }
        else
        {
            m.currentTile = null;
            m.NodeDetails.SetActive(false);                
        }
        m.markedForSave = true;
    }

    public void arrowStart()
    {
        m.effectsContents.transform.Clear();
        m.openNodeDetails(this.gameObject);
        print("Arrow start ");
        m.gridLayoutGroup.transform.Clear();
        stackGrid(this.transform, false);
        m.listOfLayers.SetActive(true);
        foreach (Transform childTransform in transform)
        {
            if (childTransform.hasNode())
            {
                stackGrid(childTransform, true);
            }
        }
    }

    public void stackGrid(Transform childTransform, bool isChild)
    {
        Sprite name = childTransform.GetComponent<SpriteRenderer>().sprite;
        
        GameObject icon = new GameObject();
        Image i = icon.AddComponent<Image>();
        i.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
        i.sprite = name;
        icon.transform.SetParent(m.gridLayoutGroup.transform);
        LayerMouseDown lmd = icon.AddComponent<LayerMouseDown>();
        lmd.child = isChild;
        lmd.relative = childTransform.gameObject;
        GameObject cube = GameObject.Instantiate((GameObject)Resources.Load("Prefabs/DeleteLayer"));
        cube.transform.SetParent(m.gridLayoutGroup.transform);
        DelMouseDown dmd = cube.GetComponent<DelMouseDown>();
        dmd.child = isChild;
        dmd.relative = childTransform.gameObject;
        dmd.icon = icon;
    }

    /*public void openSidePanel()
    {
        print("side panel");
        m.currentTile = null;
        m.FlipX.isOn = GetComponent<SpriteRenderer>().flipX;
        m.FlipY.isOn = GetComponent<SpriteRenderer>().flipY;
        for (int i = 0; i < m.type.options.Count; i++)
        {
            print(m.type.options[i]);
            if (GetComponent<Node>().landtype == m.type.options[i].text)
            {
                m.type.value = i;
            }
        }


        m.currentTile = this.gameObject;
        m.NodeDetails.SetActive(true);
        m.XYtext.text = "X: " + GetComponent<Node>().gridX + "  Y: " + GetComponent<Node>().gridY;

        Effects[] scripts = GetComponents<Effects>();
        print("scripts " + scripts.Length);
        for(int i = 0; i < scripts.Length; i++)
        {
            print(scripts[i].GetType());
            m.addEffectPrefab(scripts[i]);
        }
        print("testForNPC() " + testForNPC());
        m.loadJSON.text = testForNPC();
    }*/


    public string testForNPC()
    {
        if (GetComponent<Node>().npc)
        {
            //if (GetComponent<Node>().npc.GetComponent<Npc>().npcJSON)
            //{
            //    return GetComponent<Node>().npc.GetComponent<Npc>().npcJSON.name;
            //}
        }
        return "";
    }

    public Node addChild()
    {
        //print(this.gameObject.name + " ITS ME");
        GameObject cube = GameObject.Instantiate(this.gameObject);
        foreach (Transform child in cube.transform)
        {
            GameObject.Destroy(child.gameObject);
        }
        cube.AddComponent<SortingGroup>();
        cube.transform.parent = transform;
        cube.GetComponent<MM_MouseDown>().child = true;
        cube.GetComponent<SpriteRenderer>().sortingOrder = cube.transform.parent.GetComponent<SpriteRenderer>().sortingOrder + 1 + transform.childCount;
        cube.GetComponent<SortingGroup>().sortingOrder = cube.transform.parent.GetComponent<SpriteRenderer>().sortingOrder + 1 + transform.childCount;
        //cube.GetComponent<MM_MouseDown>().mousewait = false;
        cube.GetComponent<BoxCollider2D>().enabled = false;
        
        return cube.GetComponent<Node>();
    }

    public void overrideTile(GameObject replacement)
    {
        Node oldNode = GetComponent<Node>();
        oldNode.transform.delAllChildren();
        Node newNode = replacement.GetComponent<Node>(); 
        newNode.gridX = oldNode.gridX;
        newNode.gridY = oldNode.gridY;
        newNode.gameObject.transform.position = oldNode.transform.position;
        newNode.transform.parent = oldNode.transform.parent;
        m.graph.setNode(oldNode.gridY, oldNode.gridX, newNode);
        print("hit now " + replacement.name);
        Destroy(this.gameObject);
    }

    public void replaceTile(bool child, bool del, GameObject _replacement = null)
    {


        //FIRST REWRITE!
        
        Node oldNode = this.GetComponent<Node>();
        int old_x = oldNode.gridX;
        int old_y = oldNode.gridY;
        GameObject p = oldNode.transform.parent.gameObject;

        if(oldNode.spritesheetName == "SYSTEM")
        {
            removeSystemComponents(oldNode.gameObject);
        }

        if (child)
        {
            oldNode = addChild();
        }
        GameObject replacement = m.currentSwatch;
        if (_replacement != null)
        {
            replacement = _replacement;
            oldNode.GetComponent<SpriteRenderer>().sprite = replacement.GetComponent<SpriteRenderer>().sprite;
        }
        else
        {
            oldNode.GetComponent<SpriteRenderer>().sprite = replacement.GetComponent<Image>().sprite;
        }
        
        foreach (System.Reflection.FieldInfo f in replacement.GetComponent<Node>().GetType().GetFields())
        {
            f.SetValue(oldNode, f.GetValue(replacement.GetComponent<Node>()));
        }
        oldNode.gameObject.name = replacement.name;
        
        oldNode.Node_int(old_x, old_y, p, m.graph, true, child);
        if (oldNode.flipX)
        {
            oldNode.GetComponent<SpriteRenderer>().flipX = true;
        }
        if (oldNode.flipY)
        {
            oldNode.GetComponent<SpriteRenderer>().flipY = true;
        }

        //Sloppy, make something better here
        if(oldNode.spritesheetName == "SYSTEM")
        {
            waterPath w = oldNode.gameObject.AddComponent<waterPath>();
            Animator ani = oldNode.gameObject.AddComponent<Animator>();
            //ani.runtimeAnimatorController = Resources.Load("animations/waterAnimation") as RuntimeAnimatorController;
            w.StartUp();
            w.whatTile(true);
            oldNode.gameObject.AddComponent<stopAnimationsInEditor>();

            // doesn't stop animation after going into play mode
            // doesn't remove boarders on delete
            // doesn't check for correct name yet (sand not woring)
            // Doesn't do the boarders layer on placement (need to change to sorting groups ffs)

        }


        return;
        
        

        //ORIGINAL!
        GameObject cube;
        //print(oldNode.gridX + " " + oldNode.gridY);
        /*
        GameObject cube;
        if (!replacement)
        {
            cube = GameObject.Instantiate(m.currentSwatch);     
            MM_MouseDown md = cube.AddComponent<MM_MouseDown>();
            md.child = child;
            BoxCollider2D bc = cube.AddComponent<BoxCollider2D>();
            bc.isTrigger = true;
            cube.name = cube.name.Replace("(Clone)", "");
        } else
        {
            cube = Instantiate(replacement);
        }
        cube.transform.position = this.gameObject.transform.position;
        Node n = cube.GetComponent<Node>();
        SpriteRenderer sr = cube.AddComponent<SpriteRenderer>();
        sr.sprite = cube.GetComponent<Image>().sprite;

        if (child)
        {
            cube.transform.parent = transform;
            cube.GetComponent<SpriteRenderer>().sortingOrder = 1+ transform.childCount;
            n.Node_int(oldNode.gridX, oldNode.gridY, cube.transform.parent.gameObject, m.graph, true);
        }
        else
        {
            cube.GetComponent<SpriteRenderer>().sortingOrder = 0;
            cube.transform.parent = transform.parent;
            m.graph.setNode(oldNode.gridY, oldNode.gridX, n);
            n.Node_int(oldNode.gridX, oldNode.gridY, cube.transform.parent.gameObject, m.graph, true, true);  
            //print(m.ground.getNode(n.gridY, n.gridX));
			//print(transform.childCount + " Child Count");
			 while (transform.childCountIgnoringNodeless() > 0) {
				 transform.GetChild(transform.childCountIgnoringNodeless() - 1).SetParent(cube.transform, false);
			 }
         

            Destroy(this.gameObject);

        }
        */


    }

    void removeSystemComponents(GameObject oldNode)
    {
        Destroy(oldNode.GetComponent<waterPath>());
        Destroy(oldNode.GetComponent<Animator>());
        Destroy(oldNode.GetComponent<stopAnimationsInEditor>());
    }

    public void findSimilarTiles(Graph graph, int x, int y, string name)
    {

        List<Node> openSet = new List<Node>();
        List<Node> exitSet = new List<Node>();
        //List<Node> group = new List<Node>();
        //print("gridX " + gridX + " " + "y " + y);
        openSet.Add(graph.getNode(y, x));
        while (openSet.Count > 0)
        {
            Node currentNode = openSet[0];
            List<Node> neighbours = graph.GetNeighbours(currentNode, true);
            print(neighbours.Count + " ni");
            foreach (Node n in neighbours)
            {
                //print(n.gridX + " " + n.gridY);
                //print("this far " + n.gameObject.name + " " + name + (n.gameObject.name == name));
                if (n.gameObject.name == name)
                {
                    if (n.gCost == 0)
                    {
                        openSet.Add(n);
                        exitSet.Add(n);
                        n.gCost++;
                    }
                }
            }
            openSet.Remove(currentNode);

        }

        foreach (Node Node in exitSet)
        {
            Node.gameObject.GetComponent<MM_MouseDown>().replaceTile(m.layer.isOn, false);
        }

        return;
    }


}