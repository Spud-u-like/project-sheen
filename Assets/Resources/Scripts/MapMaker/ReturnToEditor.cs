﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ReturnToEditor : MonoBehaviour {

    // Use this for initialization
    void Awake()
    {

        DontDestroyOnLoad(this.gameObject);
    }

    private void OnGUI()
    {

        int width = 100;
        int height = 50;
        int xCenter = (Screen.width- width / 2);
        int yCenter = (Screen.height- height / 2);

        GUIStyle fontSize = new GUIStyle(GUI.skin.GetStyle("button"));
        fontSize.fontSize = 12;

        Scene scene = SceneManager.GetActiveScene();

        if (scene.name == "MapMaker" || scene.name == "NPCMap")
        {
        }
        else
        {
            // Show a button to allow scene1 to be returned to.
            if (GUI.Button(new Rect(xCenter - width / 2, yCenter - height / 2, width, height), "Back to Editor", fontSize))
            {
                SceneManager.LoadScene("MapMaker");
            }
        }
    }
}
