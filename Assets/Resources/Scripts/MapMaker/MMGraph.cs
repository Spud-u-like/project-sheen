﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MMGraph : MonoBehaviour {

    //
    public Graph init(int row, int col, bool preserveNPC = false)
    {
        int startX = 0;
        int startY = 0;
        int[,][] map = new int[row, col][];

        for (int i = 0; i < row; i++)
        {
            for (int k = 0; k < col; k++)
            {

                map[i, k] = new int[2] { 0, 0 };
            }
        }

        GameObject[] tiles = new GameObject[1] {
            (GameObject) Resources.Load("Tiles/blank", typeof(GameObject)),
        };

        //quickCheck(map, tiles.Length);
        return new Graph(null, map, null, startX, startY, "Map", preserveNPC, "MapMaker", true);
    }
}
