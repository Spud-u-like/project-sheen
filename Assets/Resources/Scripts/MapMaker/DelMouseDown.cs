﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DelMouseDown : MonoBehaviour {

    public GameObject relative;
    public GameObject icon;
    MM_Main m;
    public bool child = false;

    private void Start()
    {
        m = MM_Main.instance;
        Button b = gameObject.GetComponent<Button>();
        b.onClick.AddListener(down);
    }
    void down()
    {

        //if (m.currentTile == relative)
        //{
        if(child)
        {
            //m.effectsContents.transform.Clear();
            //m.openNodeDetails(this.gameObject);
            //m.gridLayoutGroup.transform.Clear();
            //m.openNodeDetails(this.transform.parent.gameObject);
            Destroy(relative);
            Destroy(icon);
            Destroy(this.gameObject);
        } else
        {
            if (relative.transform.childCount > 0)
            {
                GameObject child = relative.transform.GetChild(0).gameObject;
                //print(relative.name);
                //print(relative.transform.childCount);
                if (child != null)
                {
                    MM_MouseDown md = relative.GetComponent<MM_MouseDown>();
                    //print(child);
                    //print(md);
                    md.replaceTile(false, false, child);

                    StartCoroutine(delthenrebuild(child));

                    //Destroy(child);
                    //relative.GetComponent<MM_MouseDown>().arrowStart();

                    //child.GetComponent<DelMouseDown>().down();
                }
            }
        }
        //}


        
        //m.currentTile = relative;
        //m.openNodeDetails(relative);
        //m.NodeDetails.SetActive(true);
        //if its a child, it doesn't need the advanced node settings
        //m.NodeDetailsAdvanced.SetActive(!child);
    }

    IEnumerator delthenrebuild(GameObject child)
    {
        Destroy(child);
        while (child != null)
        {
            yield return null;
        }

        relative.GetComponent<MM_MouseDown>().arrowStart();
    }

    
}
