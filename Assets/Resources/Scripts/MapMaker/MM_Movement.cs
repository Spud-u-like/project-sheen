﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System.IO;
using UnityEngine.Rendering;

public class MM_Movement : MonoBehaviour
{
    // Start is called before the first frame update
    MM_Main m;

    private void Start()
    {
        m = MM_Main.instance;
    }
    void Update()
    {
        float xAxisValue = Input.GetAxis("Horizontal");
        float zAxisValue = Input.GetAxis("Vertical");
        m.keyLock = m.fade.activeInHierarchy;
        transform.Translate(new Vector3(xAxisValue, zAxisValue, 0.0f));
        m.cs.scroll();

        if (Input.GetKeyDown(KeyCode.Z) && !m.undoLock && !m.keyLock)
        {
            print("undo");
            m.undo();
            m.undoLock = true;
        }
        if (Input.GetKeyUp(KeyCode.Z) && m.undoLock && !m.keyLock)
        {
            m.undoLock = false;
        }
        if (Input.GetKeyDown(KeyCode.Y) && !m.undoLock && !m.keyLock)
        {
            print("redo");
            m.redo();
            m.undoLock = true;
        }
        if (Input.GetKeyUp(KeyCode.Y) && m.undoLock && !m.keyLock)
        {
            m.undoLock = false;
        }
        if (m.markedForSave && !Input.GetMouseButton(0))
        {
            m.markedForSave = false;
            print("Save called");
            m.saveToUndo();
        }
        if (Input.GetKeyUp(KeyCode.C) && !m.keyLock)
        {
            GameObject map = new GameObject();
            print("hit c");
            //int x_pos = 0;
            //int y_pos = 0;
            //float sizeup_a = 0;
            //float sizeup_b = 0;

            if (m.lineHolder.GetComponent<Marque>().nodeArray != null)
            {



                Node[,] nodeArray = m.lineHolder.GetComponent<Marque>().nodeArray;

                for (int i = 0; i < nodeArray.GetLength(0); i++)
                {
                    for (int k = 0; k < nodeArray.GetLength(1); k++)
                    {

                        GameObject n = Instantiate(nodeArray[i, k].gameObject);
                        Vector2 offset = n.GetComponent<Renderer>().bounds.size;
                        float sizeupX = 0;
                        float sizeupY = 0;
                        if (k > 0)
                        {
                            sizeupX = offset.x * k;
                        }
                        if (i > 0)
                        {
                            sizeupY = offset.y * i;
                        }
                        n.transform.position = new Vector3(0 + sizeupX, 0 - sizeupY, 0);
                        n.transform.parent = map.transform;
                        n.name = n.name.Replace("(Clone)", "");
                        n.GetComponent<SpriteRenderer>().sortingLayerName = "Copy" + n.GetComponent<SpriteRenderer>().sortingLayerName;
                        foreach (Transform child in n.transform)
                        {
                            print(child.name);
                            child.GetComponent<SortingGroup>().sortingLayerName = "Copy" + child.GetComponent<SortingGroup>().sortingLayerName;
                            child.GetComponent<BoxCollider2D>().enabled = false;
                        }
                        n.GetComponent<MM_MouseDown>().beingCopied = true;
                        n.GetComponent<BoxCollider2D>().enabled = false;
                        if (k + i == 0)
                        {
                            //first tile still needs a box collider, the others dont
                            n.GetComponent<BoxCollider2D>().enabled = true;
                        }
                        nodeArray[i, k] = n.GetComponent<Node>();
                    }
                }
            }
            m.copiedTiles = map;
            m.copiedTiles.name = "copiedTiles";
            m.lineHolder.SetActive(false);
        }
        if (Input.GetKeyUp(KeyCode.Delete) && !m.keyLock)
        {
            if (m.lineHolder.GetComponent<Marque>().nodeArray != null)
            {
                foreach (Node Node in m.lineHolder.GetComponent<Marque>().nodeArray)
                {
                    Node.gameObject.GetComponent<MM_MouseDown>().replaceTile(false, true);
                }
            }
            m.lineHolder.SetActive(false);
            m.saveToUndo();
        }

        if (Input.GetKeyUp(KeyCode.Escape) && !m.keyLock)
        {
            if (m.lineHolder.GetComponent<Marque>().gameObject.activeSelf)
            {
                m.lineHolder.GetComponent<Marque>().nodeArray = null;
                m.lineHolder.GetComponent<Marque>().gameObject.SetActive(false);
                m.lineHolder.SetActive(false);
            }
            if (m.copiedTiles)
            {
                Destroy(m.copiedTiles);
                m.copiedTiles = null;
            }
        }

        /*if (mouseDownList.Count > 0 && !Input.GetMouseButton(0))
        {

            MM_MouseDown[] mouseDownListArray = mouseDownList.ToArray();
            //print("actually got here " + mouseDownListArray.Length);
            for (int i =0; i < mouseDownListArray.Length; i++)
            {
                mouseDownListArray[i].mousewait = true;
                //print(mouseDownListArray[i].mousewait);
            }
            mouseDownList = new List<MM_MouseDown>();
        }*/
        if (Input.GetMouseButtonDown(0))
        {
            m.mouseDown = true;
        }
        if (Input.GetMouseButtonUp(0))
        {
            //print("called mouse up");
            m.mouseDown = false;
            m.mousewait = false;
        }
    }
}
