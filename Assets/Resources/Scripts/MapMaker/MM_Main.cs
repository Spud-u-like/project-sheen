﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System.IO;
using UnityEngine.Rendering;


public class MM_Main : Main {

    // Use this for initialization

    public MMGraph testMap;
    //public Graph graph;
    public static MM_Main instance;

    public Dropdown tilesheetDropDown;
    public Toggle Fill;
    public Toggle Brush;
    public Toggle Arrow;
    public Toggle npcToggle;
    public Toggle layer;
    public Toggle Select;
    public Toggle StartLocation;
    public Toggle roofToggle;
    public Toggle layerToggle;
    public GameObject pallet;
    public GameObject listOfLayers;

    public bool selectDragging = false;

    public GameObject currentTile;
    public GameObject NodeDetails;
    public GameObject NodeDetailsAdvanced;
    public Text XYtext;
    public Toggle FlipX;
    public Toggle FlipY;
    public Dropdown type;

    public InputField w;
    public InputField h;
    public Button create;
    public Button resize;
    public Button saveButton;

    public Button loadButton;
    public InputField loadJSON;
    public GameObject loadText;

    public Dropdown effectsDropdown;
    public GameObject effectsContents;
    public GameObject entryLines;
    public GameObject effectsLines;

    public InputField saveField;

    public GameObject alertBox;
    public Text alertBoxText;

    //public List<Graph> layers;
    //public ScrollRect layerNames;


    public GridLayoutGroup gridLayoutGroup;
    public GameObject fading;

    public string brush = "bush";
    public bool test = false;


    //public MapBuilder mapBuilder = new MapBuilder();

    public string[] undoList;
    public int undoPos = 0;
    public bool undoLock = false;

    public Dropdown loadDropdown;

    public bool markedForSave = false;

    public GridLayoutGroup palletGroup;
    public GameObject currentSwatch;
    public string swatchName;
    public string swatchLocation;
    public GameObject selector;

    public List<MM_MouseDown> mouseDownList;

    public bool lockToTop = true;

    public GameObject lineHolder;
    public GameObject copiedTiles;
    public Marque currentMarque;

    public GameObject settings;
    public GameObject startLocationToken;

    Vector3 cameraStartingPosition;

    animationHandler ah = new animationHandler();

    int maxScollValue = 10;
    int minScollValue = 2;

    public CameraScroll cs = new CameraScroll();

    bool resetUndo = false;

    public bool mouseDown = false;
    public bool mousewait= false;


    public InputField offsetXText;
    public InputField offsetYText;

    public bool keyLock = false;
    public GameObject fade;

    public GameObject tileOptions;
    public GameObject playLevelButton;
    public GameObject brushType;
    public GameObject brushOptions;

    void Awake()
    {
        instance = this;
        BetterStreamingAssets.Initialize();

        
    }

    /// <summary>
    /// ROADMAP TO 0.1 RELEASE!
    /// (DONE!) New should have popup box with name for map
    /// (DONE!) Load needs a drop down of all maps to load
    /// (DONE!) x and y size at the top need to be representative of the map
    /// (DONE!) New should also not be locked anymore
    /// (DONE!)Flip X and Y still not saved
    /// (DONE!) Effects still need to be added and need an option to pass var info (done? Fucking Finally)
    /// (DONE!) NPC's need a way to be added (and removed on resize
    /// Need in game way to flip X and Y on smaller tiles (plus way to edit their offsets)
    /// Need in game way to adjust x and y coords (some tiles dont sit flush)
    /// Way to group tiles so you are not scrolling through the same tiles time after time
    /// Depth needs another looking at (should be saved along with flipxy and offsets)
    /// Interesting Bug has turned up where you need to click and then click again to paint on tiles (oh boy!)
    /// Need to rip loads of tiles from the game
    /// NPC dialog editor (probably need a new scene for it)
    /// NPC script needs a complete rewrite
    /// And then we need to actually see if the bastered will load in normal play (Spoiler alert, it wont)
    /// Oh! And to cap it off we need to save: X/Y starting pos, if screen moving is locked (like it should be on the first map), what music is playing and probably some other shit
    /// Music (but not everything is needed at this stage)
    /// sound effects
    /// 
    /// 
    /// ROADMAP to 0.2 RELEASE!
    /// sprite importer
    /// sprite animation editor
    /// music importer
    /// music fade in/out
    /// music restart from last track position
    /// Merging maps should be an option
    /// 
    /// For new versions of the build maps will need to be opened and saved to have new data structure
    /// need a function that opens and saves each map automatically (rebuild maps for new version)
    /// </summary>

    void Start()
    {
        cameraStartingPosition = this.transform.position;
        //repopDropdowns();

        repopDropdown = GetComponent<repopDropdown>();
        print(repopDropdown);
        repopDropdown.repopulateDropdown(tilesheetDropDown, "Tilesheets/", "*.png");

        //Add additional as an option, for extra built in tiles
        Dropdown.OptionData add = new Dropdown.OptionData()
        {
            text = "Additional",
        };
        tilesheetDropDown.AddOptions(new List<Dropdown.OptionData>() { add });

        foreach (Effects e in Resources.FindObjectsOfTypeAll(typeof(Effects)) as Effects[])
        {

            Dropdown.OptionData OD = new Dropdown.OptionData()
            {
                text = e.name
            };
            effectsDropdown.AddOptions(new List<Dropdown.OptionData>() { OD });
        }
        //SaveLoad SL = SaveLoad.instance;
        //SL.WriteString();
        undoList = new string[5];
        mouseDownList = new List<MM_MouseDown>();
        settings = GameObject.Find("SettingsHolder");
        /*print("settings: " + settings);
        print("settings Map Name: " + settings.GetComponent<Settings>().map);
        saveField.text = settings.GetComponent<Settings>().map;*/
        if (settings)
        {
            if(settings.GetComponent<Settings>().map != "")
            {
                print(settings.GetComponent<Settings>().map);
                buildNewMapInit(settings.GetComponent<Settings>().map, true);
                saveField.text = settings.GetComponent<Settings>().map;
                
           }
        }
        //Directory.GetDirectories(Application.persistentDataPath);
    }

    /*public void addToUndo()
    {
        NPCUndo[0] = Instantiate(ground.NPCS);
        layerUndo[0] = Instantiate(ground.map);
        graphUndo[0] = ground;
    }
    */

    public void undo()
    {
        if (undoPos-1 != 0)
        {
            
            Destroy(graph.NPCS);
            Destroy(graph.map);
            resetUndo = false;
            buildMapUndo(undoList[undoPos-2]);
            undoPos--;
        }
    }

    public void redo()
    {
        if (undoPos < undoList.Length && undoList[undoPos] != null)
        {
            
            Destroy(graph.NPCS);
            Destroy(graph.map);
            resetUndo = false;
            buildMapUndo(undoList[undoPos]);
            undoPos++;

        }
    }

    public void saveToUndo()
    {

        //string name = (undoPos +1).ToString();
        undoList = pushToArray(mapBuilder.save(graph), undoList);
        //Debug.Log(undoList.Length);
        //Debug.Log(undoList[undoList.Length-1]);
    }

    string[] pushToArray(string str, string[] a)
    {
        //Debug.Log(a.Length);
        if (undoPos < a.Length)
        {
            a[undoPos] = str;
            undoPos++;
            for (int i = undoPos; i < a.Length; i++)
            {
                a[i] = null;
            }
            return a;
        }
        string[] outbound = new string[a.Length];
        for (int i = 0; i < outbound.Length-1; i++)
        {
            outbound[i] = a[i + 1];
        }
        outbound[outbound.Length - 1] = str;
        //Debug.Log(a.Length);
        return outbound;
    }


    void Update()
    {
        
    }

    public void startCopy(int x, int y)
    {
        if (lineHolder.GetComponent<Marque>().nodeArray != null)
        {
            /*foreach (Node Node in lineHolder.GetComponent<Marque>().nodeArray)
            {
                Node.gameObject.GetComponent<MouseDown>().replaceTile(false, true, );
            }*/

            Node[,] nodeArray = lineHolder.GetComponent<Marque>().nodeArray;
            print("Position:" + x + " " + y);
            print("lengths:" + nodeArray.GetLength(0) + " " + nodeArray.GetLength(1));
            for (int i = 0; i < nodeArray.GetLength(0); i++)
            {
                for (int k = 0; k < nodeArray.GetLength(1); k++)
                {
                    Node n = graph.getNode(k + y, i + x);
                    //print((i + x) + " " + (k + y) + " " + n);
                    if(n)
                    {
                        Node r = nodeArray[i, k];
                        //n.GetComponent<MM_MouseDown>().replaceTile(false, false, nodeArray[i, k].gameObject);
                        n.GetComponent<MM_MouseDown>().overrideTile(r.gameObject);
                        r.GetComponent<SpriteRenderer>().sortingLayerName = r.GetComponent<SpriteRenderer>().sortingLayerName.Replace("Copy", "");
                        foreach (Transform child in r.transform)
                        {
                            child.GetComponent<SortingGroup>().sortingLayerName = child.GetComponent<SortingGroup>().sortingLayerName.Replace("Copy", "");
                            //child.GetComponent<BoxCollider2D>().enabled = true;
                        }
                        r.GetComponent<MM_MouseDown>().beingCopied = false;
                        r.GetComponent<BoxCollider2D>().enabled = true;
                    }
                }
            }
        }

        lineHolder.SetActive(false);
        Destroy(copiedTiles);
        copiedTiles = null;
        saveToUndo();
        //mousewait = false;
    }

    public void save()
    {
        print(saveField.text);
        if (saveField.text != "")
        {
            if(mapBuilder.saveWrite(mapBuilder.save(graph), saveField.text))
            {
                graph.mapName = saveField.text;
                alert("Saved Succesfully");
            } else
            {
                alert("Failed to save");
            }
        }
    }

    public void alert(string s)
    {
        alertBox.SetActive(true);
        alertBoxText.text = s;
    }

    public void makeMap()
    {
        //fading.SetActive(true);
        palletGroup.transform.Clear();
        if (graph != null)
        {
            Destroy(graph.map);
            Destroy(graph.NPCS);
        }
        testMap = new MMGraph();
        graph = testMap.init(int.Parse(w.text), int.Parse(h.text));
        startCo();
        resize.interactable = true;
        saveButton.interactable = true;
        startLocationToken.transform.position = graph.getNode(0, 0).gameObject.transform.position;
        //create.interactable = false;
        //layers.Add(ground);
        //GameObject temp = (GameObject)Resources.Load("Prefabs/Layername", typeof(GameObject));
        //GameObject cube = GameObject.Instantiate(temp);
        //layerNames.content.
        //cube.transform.parent = layerNames.transform;
        //cube.transform.localScale = new Vector3(1, 1, 1);
        //cube.transform.localPosition = new Vector3(0, 0, 0);
    }

    public void startCo()
    {
        
        if (!graph.coRunning) { StartCoroutine(graph.co(this)); }
    }

    public void coDone()
    {
        fading.SetActive(false);
        undoList = new string[5];
        undoPos = 0;
        saveToUndo();
    }

    public void buildMapButton()
    {
        resetUndo = true;
        buildNewMapInit(null, false);
    }

    public void deleteMapButtom()
    {
        string mapName = loadDropdown.options[loadDropdown.value].text;
        File.Delete(Application.dataPath + "/StreamingAssets/Maps/"+ mapName + ".json");
        //loadDropdown.options.RemoveAt(loadDropdown.value);
        repopLoadLevelDropdowns();
    }

    public void buildMapUndo(string dataAsJson)
    {
        Debug.Log(dataAsJson);
        mapBuilder.init_build(dataAsJson);
        //buildNewMap(loadDropdown.options[loadDropdown.value].text, false, "MapMaker", this);
        //mapBuilder.buildMapInit(loadDropdown.options[loadDropdown.value].text, false, "MapMaker", this);
        mapBuilder.buildMap(loadDropdown.options[loadDropdown.value].text, false, "MapMaker", this, graph.preloader);
    }

    public void buildNewMapInit(string mapName, bool loadFromCommand)
    {

        //BushTestMap testMap = new BushTestMap();
        if(!loadFromCommand)
        {
            mapName = loadDropdown.options[loadDropdown.value].text;
        }
        if (!mapBuilder.init(mapName))
        {
            Debug.LogError("Failed to load level");
        }
        else
        {

            //finishBuild(resetUndo);
            if (graph != null)
            {
                Destroy(graph.preloader);
            }
            buildNewMap(mapName, false, "MapMaker", this);
            if (!loadFromCommand) { 
                saveField.text = loadDropdown.options[loadDropdown.value].text;
            }
            //menu = menuHolder.GetComponent<Menu>();
            //ground = testMap.init(int.Parse(w.text), int.Parse(h.text));
        }
    }

    public override void finishBuild(Graph _graph)
    {
        graph = _graph;
        //Destroy(graph.preloader);
        /*if (graph != null)
        {
            Destroy(graph.map);
            Destroy(graph.NPCS);
        }*/
        //graph = mapBuilder.buildMap(loadDropdown.options[loadDropdown.value].text, false, "MapMaker");
        resize.interactable = true;
        saveButton.interactable = true;
        if (resetUndo)
        {
            undoList = new string[5];
            undoPos = 0;
            saveToUndo();
        }
        //saveField.text = loadDropdown.options[loadDropdown.value].text;
        if (graph.startY >= graph.nodes.GetLength(0) || graph.startX >= graph.nodes.GetLength(1))
        {
            startLocationToken.transform.position = graph.getNode(0, 0).gameObject.transform.position;
        }
        else
        {
            startLocationToken.transform.position = graph.getNode(graph.startY, graph.startX).gameObject.transform.position;
        }
        tileOptions.SetActive(true);
        playLevelButton.SetActive(true);
        fading.SetActive(false);
    }

    /*public void rebuildMap()
    {
        Destroy(graph.map);
        Destroy(graph.NPCS);
        mapBuilder.buildMap(null, false, "MapMaker", this, null);
        //mapBuilder.buildMapInit(null, false, "MapMaker", this);
    }*/

    public void repopLoadLevelDropdowns()
    {

       string[] list = BetterStreamingAssets.GetFiles("/Maps/", "*.JSON");
        print(list.Length + " list length");
       loadDropdown.ClearOptions();
        foreach (string s in list)
        {
            string _text = s.Replace(".json", "");
            _text = _text.Replace("Maps/", "");
            Dropdown.OptionData OD = new Dropdown.OptionData()
            {
                text = _text,
            };
            loadDropdown.AddOptions(new List<Dropdown.OptionData>() { OD });
        }
    }

    public void repopAdditional(string tilesLocation)
    {
        Object[] root;
        root = Resources.LoadAll("Tiles/" + tilesLocation, typeof(GameObject));
        palletGroup.transform.Clear();
        for (int a = 0; a < root.Length; a++)
        {
            GameObject tile = (GameObject)root[a];
            Sprite name = tile.GetComponent<SpriteRenderer>().sprite;
            GameObject swatch = new GameObject();
            swatch.name = tile.name;
            Image i = swatch.AddComponent<Image>();
            i.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            i.sprite = name;
            swatch.transform.SetParent(palletGroup.transform);

            Node node = swatch.AddComponent<Node>();
            node.spriteX = 0;
            node.spriteY = 0;
            node.spriteWidth = tile.GetComponent<Node>().spriteWidth;
            node.spriteHeight = tile.GetComponent<Node>().spriteHeight;
            node.spritesheetName = tile.GetComponent<Node>().spritesheetName;
            node.layergroup = "default";
            node.nodeName = tile.GetComponent<Node>().nodeName;

            Button button = swatch.gameObject.AddComponent<Button>();
            button.onClick.AddListener(delegate { buttonClick(swatch.gameObject); });
            Outline o = swatch.gameObject.AddComponent<Outline>();
            o.effectDistance = new Vector2(10, 10);
            o.effectColor = new Color(255, 0, 190);
            o.enabled = false;
            //tile.name;
        }
    }

    public void repopPallet()
    {
        /*string tilesLocation = dropdown.options[dropdown.value].text;
        Object[] root;
        root = Resources.LoadAll("Tiles/" + tilesLocation, typeof(GameObject));
        palletGroup.transform.Clear();
        for (int a = 0; a < root.Length; a++)
        {
            GameObject tile = (GameObject) root[a];
            Sprite name = tile.GetComponent<SpriteRenderer>().sprite;
            GameObject swatch = new GameObject();
            Image i = swatch.AddComponent<Image>();
            i.transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            i.sprite = name;
            swatch.transform.SetParent(palletGroup.transform);
            swatchMouseDown SMD = swatch.AddComponent<swatchMouseDown>();
            SMD.swatchName = root[a].name;
            SMD.location = tilesLocation + "/";
            swatch.name = tilesLocation+"/"+ root[a].name;
            //tile.name;
        }*/
        palletGroup.transform.Clear();
        string fileName = tilesheetDropDown.options[tilesheetDropDown.value].text;
        if(fileName == "Additional")
        {
            repopAdditional("Additional");
            return;
        }
        StartCoroutine(ah.LoadSpriteSheet(fileName, "Tilesheets", (remoteTexture) => {
            if (remoteTexture)
            {
                Texture texture = remoteTexture;
                texture.filterMode = FilterMode.Point;
                Debug.Log(texture.width);
                string filePath = Application.dataPath + "/StreamingAssets/Tilesheets/" + Path.GetFileNameWithoutExtension(fileName) + "/" + fileName + ".json";
                string dataAsJson = File.ReadAllText(filePath);
                //init_build(dataAsJson);
                SpritesheetHolder spritesheetholder = JsonUtility.FromJson<SpritesheetHolder>(dataAsJson);

                //OK HERES THE LOGIC
                //load sheet
                //check if 
                spritesheetholder = ah.divideSpriteSheet(texture, palletGroup.transform, 24, 24, fileName, true, spritesheetholder, false);

                //GameObject instance = new GameObject();
                //instance.name = fileName;
                foreach (Transform child in palletGroup.transform)
                {
                    /*GameObject childInstance = new GameObject();
                    childInstance.name = child.name.Replace("(Clone)", "");
                    SpriteRenderer sr = childInstance.AddComponent<SpriteRenderer>();
                    sr.sprite = child.GetComponent<Image>().sprite;
                    Node n = childInstance.AddComponent<Node>();
                    foreach (System.Reflection.FieldInfo f in child.GetComponent<Node>().GetType().GetFields())
                    {
                        f.SetValue(n, f.GetValue(child.GetComponent<Node>()));
                    }
                    childInstance.transform.SetParent(instance.transform);*/
   

                    Button button = child.gameObject.AddComponent<Button>();
                    button.onClick.AddListener(delegate { buttonClick(child.gameObject); });
                    Outline o = child.gameObject.AddComponent<Outline>();
                    o.effectDistance = new Vector2(10, 10);
                    o.effectColor = new Color(255, 0, 190);
                    o.enabled = false;
                    

                }
                print(graph.preloader);
                
                if (!findChild(graph.preloader.transform, fileName))
                {
                    GameObject n = new GameObject();
                    n.name = fileName;
                    n.transform.SetParent(graph.preloader.transform);
                    ah.divideSpriteSheet(texture, n.transform, 24, 24, fileName, true, spritesheetholder, true);
                }


                brushType.SetActive(true);
                brushOptions.SetActive(true);

            }
        }));
    }

    bool findChild(Transform t, string name)
    {
        foreach (Transform child in t)
        {
            if (child.name == name)
            {
                return true;
            }
        }
        return false;
    }

    public void buttonClick(GameObject g)
    {
        if(currentSwatch)
        {
            currentSwatch.GetComponent<Outline>().enabled = false;
        }
        currentSwatch = g;
        Node node = g.GetComponent<Node>();
        swatchName = node.nodeName;
        swatchLocation = node.spritesheetName;
        g.GetComponent<Outline>().enabled = true;
        //selector.SetActive(true);
        //selector.transform.position = g.transform.position;
    }

    

    public void openResize() {
        w.text = graph.gridWidth().ToString();
        h.text = graph.gridLength().ToString();
    }

    public void resizeMap()
    {
        undoList = pushToArray(mapBuilder.save(graph, int.Parse(w.text), int.Parse(h.text)), undoList);
        Destroy(graph.map);
        buildMapUndo(undoList[undoPos - 1]);
        //testMap = new MMGraph();
        /*int width = int.Parse(w.text);
        int height = int.Parse(h.text);
        int wDiff = width - graph.nodes.GetLength(0);
        int hDiff = height - graph.nodes.GetLength(1);
        int[,][] map = new int[int.Parse(w.text), int.Parse(h.text)][];
        print("Map lengths: " + map.GetLength(0) + " " + map.GetLength(1));
        print("Old lengths: " + graph.nodes.GetLength(0) + " " + graph.nodes.GetLength(1));
        GameObject[] tiles = new GameObject[width*height];
        

        for (int h = 0; h < height; h++)
        {
            for (int w = 0; w < width; w++)
            {
                if(w < graph.nodes.GetLength(0) && h < graph.nodes.GetLength(1))
                {
                    map[w, h] = graph.rawGrid[w, h];

                } else
                {               
                    map[w, h] = new int[2] { -1, 0 };
                }
            }
        }

        //Graph _ground = new Graph(mapBuilder.map.ground.tiles, map, mapBuilder.savedPreloader, 0, 0, "layer", true);
        Destroy(graph.map);
        graph = new Graph(mapBuilder.map.ground.tiles, map, mapBuilder.savedPreloader, 0, 0, "layer", true); ;
        fading.SetActive(false);  */
    }

    public void flipX()
    {
        if (!currentTile) { return; }
        currentTile.GetComponent<SpriteRenderer>().flipX = !currentTile.GetComponent<SpriteRenderer>().flipX;
    }

    public void flipY()
    {
        if (!currentTile) { return; }
        currentTile.GetComponent<SpriteRenderer>().flipY = !currentTile.GetComponent<SpriteRenderer>().flipY;
    }

    public void setOffsetX()
    {
        if (!currentTile) { return; }
        Vector3 startingPos = currentTile.transform.parent.position;
        float result;
        float temp = 0f;
        if (float.TryParse(offsetXText.text, out result))
        {
            temp = result /100;
        }
        if (offsetXText.text.Length == 0) { return; }
        currentTile.transform.position = new Vector3(startingPos.x + temp, startingPos.y, startingPos.z);
        int saveToNode;
        if (int.TryParse((result).ToString(), out saveToNode))
        {
            currentTile.GetComponent<Node>().offsetX = saveToNode;
        }
        saveToUndo();
    }
    public void setOffsetY()
    {
        if (!currentTile) { return; }
        Vector3 startingPos = currentTile.transform.parent.position;
        float result;
        float temp = 0f;
        if(float.TryParse(offsetYText.text, out result))
        {
            temp = result / 100;
        }
        if(offsetYText.text.Length == 0) { return; }
        currentTile.transform.position = new Vector3(startingPos.x, startingPos.y + temp, startingPos.z);
        int saveToNode;
        if (int.TryParse((result).ToString(), out saveToNode))
        {
            currentTile.GetComponent<Node>().offsetY = saveToNode;
        }
        saveToUndo();
    }

    /*public void setOffsetY()
    {
        if (!currentTile) { return; }
        Vector3 startingPos = currentTile.transform.parent.position;
        float result;
        float temp = 0f;
        if (float.TryParse(offsetYText.text, out result))
        {
            temp = result;
        }
        if (offsetYText.text.Length == 0) { return; }
        currentTile.transform.position = new Vector3(startingPos.x, startingPos.y + temp, startingPos.z);
        int saveToNode;
        if (int.TryParse((temp * 100).ToString(), out saveToNode))
        {
            currentTile.GetComponent<Node>().offsetY = saveToNode;
        }
        saveToUndo();
    }*/

    public void changeType()
    {
        if (!currentTile) { return; }
        currentTile.GetComponent<Node>().landtype = type.options[type.value].text;
        if(type.options[type.value].text == "Foreground")
        {
            if (currentTile.GetComponent<MM_MouseDown>().child)
            {
                currentTile.GetComponent<SortingGroup>().sortingLayerName = "Foreground";
            }
            currentTile.GetComponent<SpriteRenderer>().sortingLayerName = "Foreground";
        } else
        {
            if (currentTile.GetComponent<MM_MouseDown>().child)
            {
                currentTile.GetComponent<SortingGroup>().sortingLayerName = "Default";
            }
            currentTile.GetComponent<SpriteRenderer>().sortingLayerName = "Default";
        }
        //print(currentTile.GetComponent<Node>().landtype + " " + type.options[type.value].text);
    }

    public void setString()
    {
        loadText.GetComponent<Text>().text = loadJSONFile();
    }

    public string loadJSONFile()
    {
        //find
        //is found?
        //set
        //return string
        Debug.Log("NPC " + loadJSON.text);

        TextAsset asset = Resources.Load(loadJSON.text) as TextAsset;
        if(asset)
        {
            //currentTile.GetComponent<Node>().npc.GetComponent<Npc>().npcJSON = asset;
            return "Success";
        } else
        {
            currentTile.GetComponent<Node>().npc.GetComponent<Npc>().npcJSON = null;
        }
        return "Failed";
    }

    public void addEffect()
    {
        System.Type MyScriptType = System.Type.GetType(effectsDropdown.options[effectsDropdown.value].text + ",Assembly-CSharp");
        Effects e = (Effects) currentTile.gameObject.AddComponent(MyScriptType);
        addEffectPrefab(e);
        //EffectLine.GetComponent<effectLine>().e = e;
        //EffectLine.GetComponent<effectLine>().named.text = e.name;
    }

    public void addEffectPrefab(Effects e)
    {
        GameObject EffectLine = GameObject.Instantiate((GameObject)Resources.Load("Prefabs/EffectLine"));
        EffectLine.transform.parent = effectsContents.transform;
        EffectLine.GetComponent<effectLine>().init(e);
    }

    public void closeAddEffect()
    {
        fading.SetActive(false);
        effectsLines.SetActive(false);
        entryLines.transform.Clear();
    }

    public void testLevel()
    {
        if(settings)
        {
            settings.GetComponent<Settings>().map = graph.mapName;
        }
        SceneManager.LoadScene(1);
    }

    public void togglePallet()
    {
        print("fill " + Fill.isOn);
        print("Brush " + Brush.isOn);
        if (Fill.isOn || Brush.isOn)
        {
            pallet.SetActive(true);
            NodeDetails.SetActive(false);
        } else
        {
            pallet.SetActive(false);
        }
        if(!Arrow.isOn)
        {
            listOfLayers.SetActive(false);
        }
    }

    public void toggleCameraLock()
    {
        graph.cameraLock = !graph.cameraLock;
    }

    public void toggleRoof()
    {

        foreach(Node par in graph.nodes)
        {
            foreach(Transform n in par.gameObject.transform)
            {
				if(n.gameObject.GetComponent<Node>()){
					if (n.gameObject.GetComponent<Node>().landtype == "roof")
					{
						n.gameObject.SetActive(roofToggle.isOn);
					}				
				}

            }
        }

    }

    public void toggleLayers()
    {

        foreach(Node par in graph.nodes)
        {
            foreach(Transform n in par.gameObject.transform)
            {
                n.gameObject.SetActive(layerToggle.isOn);              
            }
        }
        roofToggle.gameObject.SetActive(layerToggle.isOn);
    }    

    public void openNodeDetails(GameObject n)
    {
        print("side panel");
        currentTile = null;
        FlipX.isOn = n.GetComponent<SpriteRenderer>().flipX;
        FlipY.isOn = n.GetComponent<SpriteRenderer>().flipY;
        print(type.options.Count + "type.options.Count");
        if (!setLTDropdown(n.GetComponent<Node>().landtype))
        {
            setLTDropdown("open");
        }


        currentTile = n;
        NodeDetails.SetActive(true);
        XYtext.text = "X: " + n.GetComponent<Node>().gridX + "  Y: " + n.GetComponent<Node>().gridY;

        Effects[] scripts = n.GetComponents<Effects>();
        print("scripts " + scripts.Length);
        for (int i = 0; i < scripts.Length; i++)
        {
            print(scripts[i].GetType());
            addEffectPrefab(scripts[i]);
        }
        print("testForNPC() " + n.GetComponent<MM_MouseDown>().testForNPC());
        loadJSON.text = n.GetComponent<MM_MouseDown>().testForNPC();
    }

    public void resetCamera()
    {
        this.transform.position = cameraStartingPosition;
    }
    bool setLTDropdown(string comp)
    {
        for (int i = 0; i < type.options.Count; i++)
        {
            //print(type.options[i] + " Type");
            if (comp == type.options[i].text)
            {
                type.value = i;
                return true;
            }

        }
        return false;
    }

}




/*
            //print(root[a] + " LENGTH");
            GameObject temp = (GameObject)root[a];
            //print(temp.GetComponent<SpriteRenderer>().sprite);
            string s =  temp.GetComponent<SpriteRenderer>().sprite.name;
            Sprite ss = (Sprite) Resources.Load("Textures/" + s, typeof(Sprite));
            Dropdown.OptionData OD = new Dropdown.OptionData() {
                text = root[a].name,
                image = (Sprite) ss
            };
            dropdown.AddOptions(new List<Dropdown.OptionData>() { OD });

*/
