﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LayerMouseDown : MonoBehaviour {

    public GameObject relative;
    MM_Main m;
    public bool child = false;
    // Use this for initialization
    private void Start()
    {
        m = MM_Main.instance;
        Button b = gameObject.AddComponent<Button>();
        b.onClick.AddListener(down);
    }
    void down()
    {
        //Destroy(relative);
        //Destroy(this.gameObject);
        m.currentTile = relative;
        m.openNodeDetails(relative);
        m.NodeDetails.SetActive(true);
        //if its a child, it doesn't need the advanced node settings
        m.NodeDetailsAdvanced.SetActive(!child);
    }

}
