﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class swatchMouseDown : MonoBehaviour {

    public GameObject relative;
    MM_Main m;
    public string location;
    public string swatchName;
    // Use this for initialization
    private void Start()
    {
        Button b = gameObject.AddComponent<Button>();
        b.onClick.AddListener(down);
        m = MM_Main.instance;
    }
    void down()
    {
        m.swatchName = swatchName;
        m.swatchLocation = location;
        m.selector.SetActive(true);
        m.selector.transform.position = this.transform.position;
    }

}
