﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;

public class effectLine : MonoBehaviour {

    // Use this for initialization
    MM_Main m;
    public Button del;
    public Button expand;
    public Text named;
    public Effects e;


	public void init (Effects _e) {
        m = MM_Main.instance;
        del = this.gameObject.transform.GetChild(0).GetComponent<Button>();
        expand = this.gameObject.transform.GetChild(1).GetComponent<Button>();
        named = expand.gameObject.transform.GetChild(0).GetComponent<Text>();
        e = _e;
        named.text = e.GetType().ToString();
    }

    public void delMe()
    {
        Destroy(e);
        Destroy(this.gameObject);
    }


    public void openOptions()
    {
        GetVarsFromScript g = new GetVarsFromScript();
        FieldInfo[] fields = g.getFields(e);
        foreach (var prop in fields)
        {
            Debug.Log(prop.Name);
            //Debug.Log(m.GetType().GetField(prop.Name));
            //Debug.Log(m.GetType().GetField(prop.Name).FieldType.Name);
            //Debug.Log("String" == (m.GetType().GetField(prop.Name).FieldType.Name));
            //Debug.Log((m.GetType().GetField(prop.Name).GetValue(m))); 
            string n = e.GetType().GetField(prop.Name).FieldType.Name;
            if (n == "String" || n == "Int" || n == "GameObject" || n == "Sprite" || n == "Bool")
            {
                string prefab = "Prefabs/entryString";
                if (n == "Bool")
                {
                    prefab = "Prefabs/entryBool";
                }
                GameObject EffectLine = GameObject.Instantiate((GameObject)Resources.Load(prefab));
                EffectLine.transform.SetParent(m.entryLines.transform);
                string val = "";
                if ((e.GetType().GetField(prop.Name).GetValue(e))!=null)
                {
                    val = (e.GetType().GetField(prop.Name).GetValue(e)).ToString();
                }
                EffectLine.GetComponent<entryString>().init(val, e, prop.Name);
            }    
            
            /*switch (e.GetType().GetField(prop.Name).FieldType.Name)
            {
                case "String":
                    GameObject EffectLine = GameObject.Instantiate((GameObject)Resources.Load("Prefabs/entryString"));
                    EffectLine.transform.parent = m.entryLines.transform;
                    string val = (e.GetType().GetField(prop.Name).GetValue(e)).ToString();
                    EffectLine.GetComponent<entryString>().init(val, e, prop.Name);
                    break;
                case "int":
                    // Console.WriteLine("Case 2");
                    break;
                case "gameObject":
                    // Console.WriteLine("Case 2");
                    break;
                case "sprite":
                    // Console.WriteLine("Case 2");
                    break;
                case "bool":
                    // Console.WriteLine("Case 2");
                    break;
                default:
                    break;
            }*/
            //Debug.Log(prop.GetValue(m.GetType().GetField(prop.Name)));
        }
        m.fading.SetActive(true);
        m.effectsLines.SetActive(true);
    }
	

}
