﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Effects : MonoBehaviour
{

    abstract public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement);

    abstract public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement);

    abstract public string type();
}