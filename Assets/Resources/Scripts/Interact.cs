﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//abstract 
    public class Interact : MonoBehaviour {

    // Use this for initialization
    public string[] events;
    public string[] dialog;
    public GameObject item;
    public int d_counter = 0;
    public int e_counter = 0;
    

    void Start () {
		
	}
	
	public void RandomDialog()
    {
        //list of events
        //for example, dialog one, dialog two, give item, launch battle, end
        // String[] events = {dialog};
        // String[] events = {dialog_random};
        // String[] events = {dialog, dialog, give_item};
        // String[] events = {dialog, dialog, launch_battle};
        // String[] events = {dialog, run_event};
    }

    public void EventList(UiHandler uiHandler)
    {
        //print(events.Length);
        if (events.Length > e_counter)
        {
            
            switch (events[e_counter])
            {
                    case "Dialog":
                        uiHandler.goText(Dialog());
                        break;
                    default:
                        break;
            }
            
        }
    }

    public string Dialog()
    {
        string outline = "Default Message";
        if(dialog.Length > d_counter)
        {
            outline = dialog[d_counter];
            d_counter++;
        }
        return outline;
    }


}
