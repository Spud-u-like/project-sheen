﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class repopDropdown : MonoBehaviour
{
    ErrorHandler e = new ErrorHandler();
    public bool repopulateDropdown(Dropdown DropDown, string path, string Extention)
    {
        try
        {
            //Debug.Log ("/" + path + "/" + Extention);

            string[] list = BetterStreamingAssets.GetFiles("/" + path + "/", Extention);
            DropDown.ClearOptions();
            //add blank at start
            Dropdown.OptionData OD = new Dropdown.OptionData()
            {
                text = "",
            };
            DropDown.AddOptions(new List<Dropdown.OptionData>() { OD });

            if (list.Length == 0)
            {
                throw new Exception("No " + Extention + " Files Found");
            }
            
            foreach (string s in list)
            {
                string _text = s.Replace(".json", "");
                //print(path);
                _text = _text.Replace(path, "");
                OD = new Dropdown.OptionData()
                {
                    text = _text,
                };
                DropDown.AddOptions(new List<Dropdown.OptionData>() { OD });
            }
            return true;
        }
        catch (Exception exp)
        {
            e.error(exp, true);
            return false;
        }
    }
}
