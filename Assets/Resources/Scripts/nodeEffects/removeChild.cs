﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class removeChild : Effects
{
    public string whatType = "before";
    bool skipfirst = true;


    override public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {

        Transform[] ts = GetComponentsInChildren<Transform>();
        foreach (Transform child in ts)
        {
            if (skipfirst) { skipfirst = false; }
            else {
                Destroy(child.gameObject);
            }
        }
        Destroy(transform.GetComponent<removeChild>());
        return false;
    }

    override public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        return false;
    }

    override public string type()
    {
        return whatType;
    }
}
