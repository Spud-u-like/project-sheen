﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loadNewMap : Effects
{
    public string whatType = "before";
    public string mapName = "villageTest";

    override public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        print("loading new map");
        Game_Main.instance.buildNewMap(mapName, false, "normal", Game_Main.instance);
        return false;
    }

    override public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        return false;
    }

    override public string type()
    {
        return whatType;
    }
}
