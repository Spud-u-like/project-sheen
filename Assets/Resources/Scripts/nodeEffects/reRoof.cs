﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class reRoof : Effects
{
    public string whatType = "before";
    public List<Node> roof;
    public List<Node> exitNodes;

    override public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        //print("reRoof" + gridX + " " + y);
        foreach (Node n in roof)
        {
            //n.gameObject.GetComponent<SpriteRenderer>().sprite = n.gameObject.GetComponent<theRoof>().roof;
            //n.gameObject.GetComponent<theRoof>().open = false;
            //print("roofing");
            n.GetComponent<SpriteRenderer>().enabled =true;
            float a = n.GetComponent<SpriteRenderer>().color.a;
            //a += 1 * Time.deltaTime;
            //n.GetComponent<SpriteRenderer>().color = new Color(n.GetComponent<SpriteRenderer>().color.r, n.GetComponent<SpriteRenderer>().color.g, n.GetComponent<SpriteRenderer>().color.b, a); 
            //SpriteRenderer sR = n.GetComponent<SpriteRenderer>();
            //sR.enabled = true;
            // StartCoroutine("co", sR);
            n.transform.parent.GetComponent<Node>().landtype = "roof";
            n.alpha = 1;
            n.startCo();
        }

        foreach (Node e in exitNodes)
        {
            Destroy(e.GetComponent<reRoof>());
            //e.removeEffects(e.GetComponent<reRoof>());
        }

        return false;
    }

    override public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        return false;
    }

    public void initReRoof(List<Node> _roof, List<Node> _exitNodes)
    {
        roof = _roof;
        exitNodes = _exitNodes;
    }

    override public string type()
    {
        return whatType;
    }

}
