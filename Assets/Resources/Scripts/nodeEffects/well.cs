﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class well : Effects
{
    public string whatType = "interact";
    public List<Node> roof;
    public List<Node> exitNodes;

    override public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        /*main.uiHandler.goText(player.name + " looked in the well... it was dark inside."
            );*/
        Game_Main.instance.uiHandler.goText("I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG!" +
            " I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG!" +
            " I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG!" +
            " I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG!" +
            " I'm as strong as an ant, if an ant was @size:20,ital,bold,color:#0000ffff,2@ THIS BIG!"
            );

        Game_Main.instance.gamestate = "text";
        return false;
    }

    override public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        return false;
    }

    public void initReRoof(List<Node> _roof, List<Node> _exitNodes)
    {
        roof = _roof;
        exitNodes = _exitNodes;
    }

    override public string type()
    {
        return whatType;
    }
}
