﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class stairsUp : Effects
    {
        
    public string whatType = "before";
    //bool skipfirst = true;


    override public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {


        //get the tile x+1 y+1
        //if not null
        //if stairs
        //set target of movement to that

        //change movement script
        //onleave, stair leaving script runs
        //check direction of travel
        //check above or below that for stairs
        //if found set that as targets
        //
        //before enter, set dest as something else
        print("am I even being called? going up " + movement.previousY + " " + x + " " + y);
        print("am I even being called? going up " + movement.previousX + " " + x + " " + y);
        if (movement.previousX > movement.x)
        {

            Node d = graph.getNode(movement.x, movement.y - 1);
            print(d.gridX + " " + d.gridY);
            //if (d.landtype == "stairs")
            //{
            print(d);
            movement.dest = d.transform.position;
            movement.y = movement.y - 1;
            //}

        }


        return true;
        
    }

    override public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        //find stairs nearby?
        return false;
    }

    public Node targetNode()
    {
        return new Node();
    }

    override public string type()
    {
        return whatType;
    }
}