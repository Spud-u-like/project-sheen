﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testEffect : Effects
{
    public string whatType = "after";

    override public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        print("testEffect");
        return false;
    }

    override public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        return false;
    }

    override public string type()
    {
        return whatType;
    }
}
