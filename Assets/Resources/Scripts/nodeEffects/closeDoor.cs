﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class closeDoor : Effects
{
    public string whatType = "onLeave";


    override public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        Sprite door = GetComponent< unRoof>().door;
        GetComponent<SpriteRenderer>().sprite = door;
        return false;
    }

    override public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        return false;
    }

    override public string type()
    {
        return whatType;
    }
}
