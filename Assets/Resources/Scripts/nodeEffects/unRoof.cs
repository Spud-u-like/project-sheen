﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class unRoof : Effects {

    public string whatType = "before";

    public Sprite floor, door;
    public string unroofed = "open";
    reRoof re_Roof;
    List<Node> theRoof = new List<Node>();
    List<Node> exitSet;
    List<string> landType = new List<string>();


    /*Where you were up to
     * 
     * flip for sprites
        npc map screen
        npc creator screen
        actually import the npc into the main game
        get door opening again
        get roof removing again
        music playing
        Direction bowie starts the map facing, for example facing right when he starts on the stairs
        log depth is wrong
        can walk off stairs at wrong angle
    */
    

    override public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        //print(theRoof.Count + " " + landType.Count);
        for (int a = 0; a< theRoof.Count;a++)
        {
            //SpriteRenderer sR = theRoof[a].GetComponent<SpriteRenderer>();
            //sR.color = new Color(sR.color.r, sR.color.g, sR.color.b, 0);
            //print(theRoof[a].transform.parent.GetComponent<Node>().landtype);
            //Node p = theRoof[a].transform.parent.GetComponent<Node>();
            //Node n = theRoof[a];
            //p.landtype = landType[a];
            theRoof[a].alpha = 0;
            //n.red = 0.5f;
            theRoof[a].startCo();
            //sR.enabled = false;
            //co.go(-4, sR);
            //public roofCo co = new roofCo(); 
        }
        foreach (Node n in exitSet)
        {
            //re_Roof = new reRoof();
            //re_Roof.initReRoof(roof,exitSet);
            n.GetComponent<SpriteRenderer>().color = Color.yellow;
            re_Roof = n.gameObject.AddComponent<reRoof>();
            re_Roof.initReRoof(theRoof, exitSet);
            n.effects.Add(re_Roof);
            //n.addEffect(re_Roof);
        }
        return false;
    }



    override public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        //gameObject.GetComponent<SpriteRenderer>().sprite = floor;
        //print("unRoof");
        //List<Node> roof = new List<Node>();
        List<Node> openSet = new List<Node>();
        exitSet = new List<Node>();
        openSet.Add(graph.getNode(y, x));
        
        while (openSet.Count > 0)
        {
            
            Node currentNode = openSet[0];
            List<Node> neighbours = graph.GetNeighbours(currentNode);
            foreach (Node n in neighbours)
            {
                if (n.gCost == 0)
                {
                    foreach (Transform t in n.transform)
                    {
                        //print(t.name);
                        if (!t.GetComponent<Node>())
                        {
                            continue;
                        }
                        if (t.GetComponent<Node>().isRoof)
                        {
                            theRoof.Add(t.GetComponent<Node>());
                        }
                    }
                    openSet.Add(n);
                    n.gCost++;
                }
                //print("this far " + n.landtype);
                if (n.traversable(player, true) && n.landtype!="door")
                {
                    if(n.gCost ==0)
                    {
                        exitSet.Add(n);
                        n.gCost++;
                    }
                }
            }
            openSet.Remove(currentNode);

        }
        foreach (Node Node in graph.nodes)
        {
            Node.Clear();
        }
        /*if (roof.Count == 0)
        {
            print("no roof");
            return false;
        }*/

        return false;
    }

    override public string type()
    {
        return whatType;
    }


}