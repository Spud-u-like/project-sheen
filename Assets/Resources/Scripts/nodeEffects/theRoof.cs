﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class theRoof : Effects
{
    public string whatType = "never";
    public Sprite floor, roof;
    public bool open = false;

    override public bool runEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        //print("testEffect");
        return false;
    }

    override public bool initEffect(Graph graph, int x, int y, moveModifiers player, Movement movement)
    {
        return false;
    }

    override public string type()
    {
        return whatType;
    }
}
