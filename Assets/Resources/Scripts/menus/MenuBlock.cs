﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuBlock : MonoBehaviour {

    //menu data
    public string function = "";
    public bool isSelected = false;
    public bool isSelectable = false;

    //sprite data
    private Sprite currentSprite;
    public Sprite mySprite1;
    public Sprite mySprite2;

    //ref to menu obj
    public GameObject menu;


    private void Start()
    {
        currentSprite = GetComponent<Image>().sprite;
        mySprite1 = currentSprite;
        mySprite2 = currentSprite;
        MakeSelectable(false);
    }

    public void MakeSelectable(bool selectable)
    {
        isSelectable = selectable;
        currentSprite = mySprite1;

        GetComponent<Image>().enabled = selectable;  
        
        GetComponent<Image>().sprite = currentSprite;
    }

    public void MakeSelected(bool selected)
    {
        isSelected = selected;       

        if (!selected)
        {
            currentSprite = mySprite1;
            
        }
        else
        {
            currentSprite = mySprite2;
        }

        GetComponent<Image>().sprite = currentSprite;
    }

    public void FlashImage()
    {
        if (currentSprite == mySprite1)
        {
            currentSprite = mySprite2;
        }
        else
        {
            currentSprite = mySprite1;
        }

        GetComponent<Image>().sprite = currentSprite;

    }
}
