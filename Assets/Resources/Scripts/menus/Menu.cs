﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Menu : MonoBehaviour {

    //A menu needs to know...

    //if it can accept being closed by the player...
    public bool canBeClosed = false;

    //how many menu items show up... it's upto 4 and can never be 0
    //it might only be 2 if it's a simple yes no dialog choice...
    private int menuItems = 0;

    //the type of menu this is, "menu", "choice" etc... 
    private string menuType = "";

    //which menu this is, if it is a menu
    private string menuName;

    //whether or not the menu is moving
    public bool moving = false;

    //each of the four elements which will make up the menu
    public GameObject menuBlock0; //each of these has a script attached for keeping track of it's image etc (MenuBlock.cs)
    public GameObject menuBlock1;
    public GameObject menuBlock2;
    public GameObject menuBlock3;

    public GameObject[] menuBlockArray;

    public float menuFlashTimer;
    public float menuFlashTimerMax;

    //animates the menu in and out of the frame
    Animator anim;

    //should be hashing the animation states, will look into that...
    /*
    int menuInHash;
    int menuOutHash;
    int menuIdleHash;
    int menuIdleOffHash;
    */

    [SerializeField]
    int menuState = 4;
    //there is a 'menuState' for the animation of the menu...
    /*
     * 0- menu coming in
     * 1- menu going out
     * 2- menu idle
     * 3- menu_idle offscreen
     */

    //A menu will be 4 pictures arranged in a cross, which when each is activated may cause the current menu to close and open a new menu

    //The hierarchy is as follows:
    /*
     * -Initial Menu
     * -- ..complete later when i need to actually set up the menu to mirror sf2 exactly
     * 
     * 
     */

    //the menu is composed of these elements:
    /*
     *  
     * 
     * 
     */

    private void Start()
    {
        anim = GetComponent<Animator>();
        menuFlashTimerMax = 0.25f;
        menuFlashTimer = menuFlashTimerMax;

        menuBlockArray = new GameObject[4];
        menuBlockArray[0] = menuBlock0;
        menuBlockArray[1] = menuBlock1;
        menuBlockArray[2] = menuBlock2;
        menuBlockArray[3] = menuBlock3;


        /*
        menuInHash = Animator.StringToHash("menu_in");
        menuIdleHash = Animator.StringToHash("menu_idle");
        menuOutHash = Animator.StringToHash("menu_out");
        menuIdleOffHash = Animator.StringToHash("menu_idle_offscreen");
        */ // should use hashes, they are more effecient, they are not working for some raisin!

        menuState = 3;

    }

    private void Update()
    {
        SetMenuState();

        if (menuState == 1)
        {
            if (menuFlashTimer > 0f)
            {
                menuFlashTimer -= Time.deltaTime;
            }
            else
            {
                menuFlashTimer = menuFlashTimerMax;
                FlashMenuBlock();
            }
        }

        InteractWithMenu();
    }

    void MenuHandler(string thisFunction)
    {
        //thisFunction is grabbed from the menuBlock and is saved in the data of the menu. it tells the handler what to do when that thing is selected...

        switch (thisFunction)
        {
            //the item menu
            case "item":
                {
                    GenerateMenu(4, thisFunction, "menu");
                }
                break;
        }

    }

    void InteractWithMenu()
    {
        if (menuState == 1)
        {
            if (Input.GetButtonDown("Submit"))
            {

                GameObject thisMenuBlock = ReturnCurrentlySelectedBlock();
                string thisFunction = thisMenuBlock.GetComponent<MenuBlock>().function;
                MenuHandler(thisFunction);


            }
        }
    }

    GameObject ReturnCurrentlySelectedBlock()
    {
        for (int i = 0; i < menuBlockArray.Length; i++)
        {
            if (menuBlockArray[i].GetComponent<MenuBlock>().isSelected)
            {
                return menuBlockArray[i];
            }
        }
        return null;
    }

    void FlashMenuBlock()
    {
        GameObject thisMenuBlock = ReturnCurrentlySelectedBlock();
        if (thisMenuBlock != null)
        {
            thisMenuBlock.GetComponent<MenuBlock>().FlashImage();
        }
    }

    void SetMenuState()
    {
        /*
         * 0- menu coming in
         * 1- menu idle
         * 2- menu goint out
         * 3- menu_idle offscreen
         */


        switch (menuState)
        {
            case 0:
                {
                    if (anim.GetCurrentAnimatorStateInfo(0).IsName("menu_in") && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
                    {
                        menuState = 1;
                    }
                    anim.Play("menu_in");
                }
                break;
            case 1:
                {
                    anim.Play("menu_idle");
                    //nothing happens here because its idle
                }
                break;
            case 2:
                {
                    if (anim.GetCurrentAnimatorStateInfo(0).IsName("menu_out") && anim.GetCurrentAnimatorStateInfo(0).normalizedTime >= 1f)
                    {
                        menuState = 3;
                    }
                    anim.Play("menu_out");
                }
                break;
            case 3:
                {
                    anim.Play("menu_idle_offscreen");
                    //nothing happens here because its idle
                }
                break;

        }
    }


    IEnumerator SwapMenu(int numOfItems, string thisMenuName, string thisMenuType)
    {
        while (menuState == 0)
        {
            yield return null;
        }
        while (menuState == 1)
        {
            yield return null;
            menuState = 2;
        }
        while (menuState == 2)
        {
            
            yield return null;
        }
        if (menuState == 3)
        {
            GenerateMenu(numOfItems, thisMenuName, thisMenuType);
            yield return null;
        }
    }


    public void GenerateMenu(int numOfItems, string thisMenuName, string thisMenuType)
    {

        menuItems = numOfItems;
        menuType = thisMenuType; //could be "choice", "menu" etc.
        menuName = thisMenuName; //could be "main", "item" "party"

        print("generating this type of menu: " + thisMenuType + " and with this name: " + thisMenuName);
        
        //set it to be outside the stage and animating to enter the stage now..
        //if it currently is 3 then it's off screen idle and just needs to be created...
        if (menuState != 3)
        {
            StartCoroutine(SwapMenu(numOfItems, thisMenuName, thisMenuType));
            return;
        }

        menuState = 0;

        //also set the flashing to start from the beginning
        menuFlashTimer = menuFlashTimerMax;

        switch (thisMenuType)
        {
            case "menu":
                {
                    canBeClosed = true;
                }
                break;
            case "choice":
                {
                    canBeClosed = false;
                }
                break;
        }

        //set all menu blocks to be not selectable by default
        menuBlock0.GetComponent<MenuBlock>().MakeSelectable(false); //up
        menuBlock1.GetComponent<MenuBlock>().MakeSelectable(false); //right
        menuBlock2.GetComponent<MenuBlock>().MakeSelectable(false); //bottom
        menuBlock3.GetComponent<MenuBlock>().MakeSelectable(false); //left

        menuBlock0.GetComponent<MenuBlock>().MakeSelected(false); //up
        menuBlock1.GetComponent<MenuBlock>().MakeSelected(false); //right
        menuBlock2.GetComponent<MenuBlock>().MakeSelected(false); //bottom
        menuBlock3.GetComponent<MenuBlock>().MakeSelected(false); //left



        //now that we have set up the correct armount of menublocks for the menu
        //we can check which menu this is exactly and load a set of images from there...

        //to do this we use the paramater menuName
        //and access the function on the MenuArray.cs script called ReturnSpriteArray()

        Sprite[][] thisMenuSpriteArray = GetComponent<MenuArrays>().ReturnSpriteArray(menuName);
        string[] thisFunctionStringArray = GetComponent<MenuArrays>().ReturnFunctionArray(menuName);

        if (thisMenuSpriteArray != null && thisFunctionStringArray != null)
        {
            //assign those function strings to each menublock as it comes in...
            menuBlock0.GetComponent<MenuBlock>().function = thisFunctionStringArray[0];
            menuBlock1.GetComponent<MenuBlock>().function = thisFunctionStringArray[1];
            menuBlock2.GetComponent<MenuBlock>().function = thisFunctionStringArray[2];
            menuBlock3.GetComponent<MenuBlock>().function = thisFunctionStringArray[3];

            //unwrap those sprite arrays...
            Sprite[] spriteArray0 = thisMenuSpriteArray[0];
            Sprite[] spriteArray1 = thisMenuSpriteArray[1];
            Sprite[] spriteArray2 = thisMenuSpriteArray[2];
            Sprite[] spriteArray3 = thisMenuSpriteArray[3];

            //there's a chance of of these could be empty if it is going to be disabled from input
            menuBlock0.GetComponent<MenuBlock>().mySprite1 = spriteArray0[0];
            menuBlock0.GetComponent<MenuBlock>().mySprite2 = spriteArray0[1];            

            menuBlock1.GetComponent<MenuBlock>().mySprite1 = spriteArray1[0];
            menuBlock1.GetComponent<MenuBlock>().mySprite2 = spriteArray1[1];

            menuBlock2.GetComponent<MenuBlock>().mySprite1 = spriteArray2[0];
            menuBlock2.GetComponent<MenuBlock>().mySprite2 = spriteArray2[1];

            menuBlock3.GetComponent<MenuBlock>().mySprite1 = spriteArray3[0];
            menuBlock3.GetComponent<MenuBlock>().mySprite2 = spriteArray3[1];

        }
        else
        {
            print("tried to grab a null sprite array");
        }

        //this sets the correct configuration of the blocks depending on how many are meant to be enabled...
        //0 is top, 1 is right, 2 is bottom, 3 is left
        switch (menuItems)
        {
            case 0:
                {

                }
                break;
            case 1:
                //top only
                {
                    menuBlock0.GetComponent<MenuBlock>().MakeSelectable(true);


                    menuBlock0.GetComponent<MenuBlock>().MakeSelected(true);
                }
                break;
            case 2:
                //left and right only
                {
                    menuBlock1.GetComponent<MenuBlock>().MakeSelectable(true);
                    menuBlock3.GetComponent<MenuBlock>().MakeSelectable(true);

                    menuBlock3.GetComponent<MenuBlock>().MakeSelected(true);
                }
                break;
            case 3:
                //left right and top only
                {
                    menuBlock0.GetComponent<MenuBlock>().MakeSelectable(true);
                    menuBlock1.GetComponent<MenuBlock>().MakeSelectable(true);
                    menuBlock3.GetComponent<MenuBlock>().MakeSelectable(true);

                    menuBlock0.GetComponent<MenuBlock>().MakeSelected(true);
                }
                break;
            //all
            case 4:
                {
                    menuBlock0.GetComponent<MenuBlock>().MakeSelectable(true);
                    menuBlock1.GetComponent<MenuBlock>().MakeSelectable(true);
                    menuBlock2.GetComponent<MenuBlock>().MakeSelectable(true);
                    menuBlock3.GetComponent<MenuBlock>().MakeSelectable(true);

                    menuBlock0.GetComponent<MenuBlock>().MakeSelected(true);
                }
                break;

        }

    }

    public void CloseMenu()
    {
        //  menuBlock0.GetComponent<MenuBlock>().MakeSelectable(false);
        //  menuBlock1.GetComponent<MenuBlock>().MakeSelectable(false);
        //  menuBlock2.GetComponent<MenuBlock>().MakeSelectable(false);
        //  menuBlock3.GetComponent<MenuBlock>().MakeSelectable(false);

        menuState = 2;

    }

    public void SelectMenuBlock(float thisHoz, float thisVert)
    {
        //set them all to unselected first off..

        menuBlock0.GetComponent<MenuBlock>().MakeSelected(false);
        menuBlock1.GetComponent<MenuBlock>().MakeSelected(false);
        menuBlock2.GetComponent<MenuBlock>().MakeSelected(false);
        menuBlock3.GetComponent<MenuBlock>().MakeSelected(false);


        //reset the flash timer!
        menuFlashTimer = menuFlashTimerMax;

        //up
        if (thisHoz == 0 && thisVert > 0)
        {
            if (menuBlock0.GetComponent<MenuBlock>().isSelectable)
            {
                menuBlock0.GetComponent<MenuBlock>().MakeSelected(true);
            }
        }
        //right
        else if(thisHoz > 0 && thisVert == 0)
        {
            if (menuBlock1.GetComponent<MenuBlock>().isSelectable)
            {
                menuBlock1.GetComponent<MenuBlock>().MakeSelected(true);
            }
        }
        //down
        else if (thisHoz == 0 && thisVert < 0)
        {
            if (menuBlock2.GetComponent<MenuBlock>().isSelectable)
            {
                menuBlock2.GetComponent<MenuBlock>().MakeSelected(true);
            }
        }
        //left
        else if (thisHoz < 0 && thisVert == 0)
        {
            if (menuBlock3.GetComponent<MenuBlock>().isSelectable)
            {
                menuBlock3.GetComponent<MenuBlock>().MakeSelected(true);
            }
        }

    }

}
