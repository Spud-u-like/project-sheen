﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuArrays : MonoBehaviour {



    //OVERALL there is an array called allMenuBlockArray

        //within that is stored an array of arrays called 'blankBlockMain' where blank is which menu we're talking about

        //with each of those arrays is an array of two images which represent the button flashing


    private MenuBlockType[] allMenuBlockArray;

    //MAIN
    public Sprite[][] menuBlockMainMenu;
    string[] menuBlockFunctionMainMenu;
    //ITEM
    public Sprite[][] menuBlockItemMenu;
    string[] menuBlockFunctionItemMenu;
    //FUNCTION


    //each menu block has its own array of sprites, set up in the inspector
    //these are for main...
    public Sprite[] spriteArrayMember;
    public Sprite[] spriteArrayItem;    
    public Sprite[] spriteArraySearch;
    public Sprite[] spriteArrayMagic;
    
    //these are for item menu
    public Sprite[] spriteArrayUse;
    public Sprite[] spriteArrayEquip;
    public Sprite[] spriteArrayDrop;
    public Sprite[] spriteArrayGive;

        //these are for choices etc..

    


    private void Start()
    {
        InitArrays();        
    }

    private void InitArrays()
    {

        //first set up the allMenuBlockArray to store all of the arrays we're about to make... one for each menu, including choice menus.
        allMenuBlockArray = new MenuBlockType[2];





        //"main" - 0

        menuBlockMainMenu = new Sprite[4][];
        menuBlockMainMenu[0] = spriteArrayMember;
        menuBlockMainMenu[1] = spriteArrayItem;
        menuBlockMainMenu[2] = spriteArraySearch;
        menuBlockMainMenu[3] = spriteArrayMagic;

        menuBlockFunctionMainMenu = new string[4];
        menuBlockFunctionMainMenu[0] = "member";
        menuBlockFunctionMainMenu[1] = "item";
        menuBlockFunctionMainMenu[2] = "search";
        menuBlockFunctionMainMenu[3] = "magic";

        allMenuBlockArray[0] = new MenuBlockType("main", "menu", menuBlockMainMenu, menuBlockFunctionMainMenu);


        //"item" - 1

        menuBlockItemMenu = new Sprite[4][];
        menuBlockItemMenu[0] = spriteArrayUse;
        menuBlockItemMenu[1] = spriteArrayEquip;
        menuBlockItemMenu[2] = spriteArrayDrop;
        menuBlockItemMenu[3] = spriteArrayGive;

        menuBlockFunctionItemMenu = new string[4];
        menuBlockFunctionItemMenu[0] = "use";
        menuBlockFunctionItemMenu[1] = "equip";
        menuBlockFunctionItemMenu[2] = "drop";
        menuBlockFunctionItemMenu[3] = "give";

        allMenuBlockArray[1] = new MenuBlockType("item","menu", menuBlockItemMenu, menuBlockFunctionItemMenu);



    }

    public Sprite[][] ReturnSpriteArray(string thisName)
    {
        for (int i = 0; i < allMenuBlockArray.Length; i++)
        {
            string checkName = allMenuBlockArray[i].menuName;
            if (checkName == thisName)
            {
                return allMenuBlockArray[i].menuSpriteArray;
            }
        }

        return null;
        
    }

    public string[] ReturnFunctionArray(string thisName)
    {
        for (int a = 0; a < allMenuBlockArray.Length; a++)
        {
            string checkName = allMenuBlockArray[a].menuName;
            if (checkName == thisName)
            {
                return allMenuBlockArray[a].menuFunctionStringArray;
            }
        }

        return null;
    }



}
