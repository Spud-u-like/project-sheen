﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuBlockType {

    public string menuType;
    public string menuName;
    public Sprite[][] menuSpriteArray;
    public string[] menuFunctionStringArray;

    public MenuBlockType(string thisMenuName,string thisMenuType, Sprite[][] thisMenuSpriteArray, string[] thisMenuFunctionStringArray)
    {
        menuName = thisMenuName;
        menuType = thisMenuType;

        menuSpriteArray = thisMenuSpriteArray;
        menuFunctionStringArray = thisMenuFunctionStringArray;
    }


}
