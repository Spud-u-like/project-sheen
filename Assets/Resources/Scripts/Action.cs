﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

abstract public class Action : MonoBehaviour
{

    abstract public bool runAction(Graph graph, int x, int y, moveModifiers player);

    abstract public bool initEffect(Graph graph, int x, int y, moveModifiers player);

    abstract public string type();
}