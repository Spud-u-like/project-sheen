﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class dragHandler : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{
    private RectTransform t;
    [SerializeField] private Canvas canvas;

    private void Awake()
    {
        t = GetComponent<RectTransform>();
    }
    public void OnPointerDown(PointerEventData p)
    {

    }

    public void OnDrag(PointerEventData p)
    {
        t.anchoredPosition += p.delta / canvas.scaleFactor;
    }

    public void OnDrop(PointerEventData p)
    {
        t.anchoredPosition += p.delta / canvas.scaleFactor;
    }

    public void OnBeginDrag(PointerEventData p)
    {

    }

    public void OnEndDrag(PointerEventData p)
    {

    }
}
