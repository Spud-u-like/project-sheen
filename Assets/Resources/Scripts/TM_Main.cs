﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEditor;
using Better;
using System;
using System.IO;

public class TM_Main : Main {

    public Dropdown spriteSheets;
    //repopDropdown repopDropdown;
    animationHandler ah = new animationHandler();
    public GridLayoutGroup spriteGrid;

    int spriteWidth = 24;
    int spriteHeight = 24;
    public string fileName = "";
    Texture texture;

    public InputField nodeName;
    public InputField nodeOffsetX;
    public InputField nodeOffsetY;
    public Dropdown nodeLayerGroup;
    public Toggle nodeFlipX;
    public Toggle nodeFlipY;
    public Dropdown landtype;
    public Dropdown tagtype;

    public Node currentNode;
    public SpritesheetHolder spritesheetholder;

    void Start () {
        repopDropdown = GetComponent<repopDropdown>();
        BetterStreamingAssets.Initialize();
        repopDropdown.repopulateDropdown(spriteSheets, "Tilesheets/", "*.png");
    }

    public void loadSpriteSheet()
    {
        print(spriteSheets.options[spriteSheets.value].text);
        fileName = spriteSheets.options[spriteSheets.value].text;
        //fileName = spriteSheets.options[spriteSheets.value].text;
        //spriteGrid.transform.delAllChildren();
        //frameGrid.transform.delAllChildren();
        //StartCoroutine(LoadSpriteSheet());
        spriteGrid.transform.delAllChildren();
        StartCoroutine(ah.LoadSpriteSheet(fileName, "Tilesheets", (remoteTexture) => {
            if (remoteTexture)
            {
                texture = remoteTexture;
                texture.filterMode = FilterMode.Point;
                Debug.Log(texture.width);
                if (!Directory.Exists(Application.dataPath + "/StreamingAssets/Tilesheets/" + Path.GetFileNameWithoutExtension(fileName) + "/"))
                {
                    //if it doesn't, create it
                    Directory.CreateDirectory(Application.dataPath + "/StreamingAssets/Tilesheets/" + Path.GetFileNameWithoutExtension(fileName) + "/");

                    saveSpriteSheet(new SpritesheetHolder());
                } else
                {
                    string filePath = Application.dataPath + "/StreamingAssets/Tilesheets/" + Path.GetFileNameWithoutExtension(fileName) + "/" + fileName + ".json";
                    string dataAsJson = File.ReadAllText(filePath);
                    //init_build(dataAsJson);
                    spritesheetholder = JsonUtility.FromJson<SpritesheetHolder>(dataAsJson);
                }
                //OK HERES THE LOGIC
                //load sheet
                //check if 
                divideSpriteSheet(texture);
                //savedAnimations.gameObject.SetActive(true);
                //repopDropdown.repopulateDropdown(savedAnimations, "NPCAnimations/" + Path.GetFileNameWithoutExtension(fileName) + "/", "*.JSON");
            }
        }));
    }

    void saveSpriteSheet(SpritesheetHolder ssh)
    {
        string filePath = Application.dataPath + "/StreamingAssets/Tilesheets/" + Path.GetFileNameWithoutExtension(fileName) + "/" + fileName + ".json";

        
        string dataAsJson = JsonUtility.ToJson(ssh);
        File.WriteAllText(filePath, dataAsJson);
    }

    void divideSpriteSheet(Texture texture)
    {
        spritesheetholder = ah.divideSpriteSheet(texture, spriteGrid.transform, spriteHeight, spriteWidth, fileName, true, spritesheetholder, false);
        saveSpriteSheet(spritesheetholder);

        foreach (Transform child in spriteGrid.transform)
        {
            Button button = child.gameObject.AddComponent<Button>();
            button.onClick.AddListener(delegate { buttonClick(child.gameObject); });
        }
        if (File.Exists(Application.dataPath + "/StreamingAssets/Tilesheets/" + Path.GetFileNameWithoutExtension(fileName) + "/" + fileName + ".json"))
        {
            //load file
            //if grid cords are larger than current size make as "deleted" but don't remove from file

        }
        else
        {
            //create file

        }
        //load ss.json file
        //emptySpriteAnimator();
    }

    public void buttonClick(GameObject g)
    {
        Node node = g.GetComponent<Node>();
        currentNode = node;
        nodeName.text = node.nodeName;
        nodeOffsetX.text = node.offsetX.ToString();
        nodeOffsetY.text = node.offsetY.ToString();
        //nodeLayerGroup
        /*for (int i = 0; i < landtype.options.Count; i++)
        {

            if (node.landtype == landtype.options[i].text)
            {
                print(landtype.options[i].text);
                landtype.value = i;
            }
        }*/
        node.landtype = setDropdown(landtype, node.landtype, "open");
        node.tag = setDropdown(tagtype, node.tag, "Untagged");
        nodeFlipX.isOn = node.flipX;
        nodeFlipY.isOn = node.flipY;
        //landEffect;
        
    }

    string setDropdown(Dropdown d, string target, string defaultName)
    {
        for (int i = 0; i < d.options.Count; i++)
        {

            if (target == d.options[i].text)
            {
                print(d.options[i].text);
                d.value = i;
            }
        }
        if(target == "")
        {
            return defaultName;
        }
        return target;
    }

    public void updateNode()
    {
        print("node updating");
        currentNode.nodeName = nodeName.text;
        //currentNode.offsetX = float.Parse(nodeOffsetX.text);
        //currentNode.offsetY = float.Parse(nodeOffsetY.text);
        print(nodeFlipX.isOn);
        currentNode.flipX = nodeFlipX.isOn;
        currentNode.flipY = nodeFlipY.isOn;
        print(currentNode.landtype);
        print(landtype.options[landtype.value].text);
        currentNode.landtype = landtype.options[landtype.value].text;
        currentNode.tag = tagtype.options[tagtype.value].text;
        print(currentNode.tag + " tag name");
        spritesheetholder.nodeHolderArray[currentNode.nodeSpriteGridPosX].nodeHolder[currentNode.nodeSpriteGridPosY].nodeName = nodeName.text;
        spritesheetholder.nodeHolderArray[currentNode.nodeSpriteGridPosX].nodeHolder[currentNode.nodeSpriteGridPosY].landType = currentNode.landtype;
        spritesheetholder.nodeHolderArray[currentNode.nodeSpriteGridPosX].nodeHolder[currentNode.nodeSpriteGridPosY].tag = currentNode.tag;
        saveSpriteSheet(spritesheetholder);
        print(spritesheetholder.nodeHolderArray[currentNode.nodeSpriteGridPosX].nodeHolder[currentNode.nodeSpriteGridPosY].nodeName);
    }

    public void applyAll()
    {
        for(int i = 0; i < spritesheetholder.nodeHolderArray.Length; i++)
        {
            for (int h = 0; h < spritesheetholder.nodeHolderArray[i].nodeHolder.Length; h++)
            {
                spritesheetholder.nodeHolderArray[i].nodeHolder[h].flipX = nodeFlipX.isOn;
                spritesheetholder.nodeHolderArray[i].nodeHolder[h].flipY = nodeFlipY.isOn;
                spritesheetholder.nodeHolderArray[i].nodeHolder[h].landType = landtype.options[landtype.value].text;
                spritesheetholder.nodeHolderArray[i].nodeHolder[h].tag = tagtype.options[tagtype.value].text;
            }
        }
        foreach (Transform child in spriteGrid.transform)
        {
            Node n = child.gameObject.GetComponent<Node>();
            n.flipX = nodeFlipX.isOn;
            n.flipY = nodeFlipY.isOn;
            n.landtype = landtype.options[landtype.value].text;
            n.tag = tagtype.options[tagtype.value].text;
        }
        saveSpriteSheet(spritesheetholder);
    }

}
