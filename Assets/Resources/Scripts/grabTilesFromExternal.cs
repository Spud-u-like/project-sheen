﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Better;
using UnityEngine.Networking;

public class grabTilesFromExternal : MonoBehaviour {

	// Use this for initialization
	void Start () {
        BetterStreamingAssets.Initialize();
        StartCoroutine(LoadSprite());
    }

    public void swapOutTile()
    {

        //Sprite = Resources.LoadAll<Sprite>(spriteNames);
        string[] list = BetterStreamingAssets.GetFiles("/Tiles/", "*.png");
        print(list[0]);
        WWW localFile;
        localFile = new WWW(list[0]);
        Texture texture = localFile.texture;
        Sprite sprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
        SpriteRenderer spriteR = gameObject.GetComponent<SpriteRenderer>();
        spriteR.sprite = sprite;

    }

    public IEnumerator LoadSprite()
    {
        string[] list = BetterStreamingAssets.GetFiles("/Tiles/", "*.png");
        print(list[0]);
        WWW localFile = new WWW(Application.dataPath + "/StreamingAssets/Tiles/bowiespritesheetreplacement.png");
        //localFile = new WWW(list[0]);
        //print("file://" + System.IO.Path.Combine(Application.streamingAssetsPath, list[0]));
        yield return localFile;
        Debug.Log(localFile.error);
        print(localFile.isDone);
        Texture texture = localFile.texture;
        print(texture);
        //Renderer renderer = GetComponent<Renderer>();
        //renderer.material.mainTexture = texture;
        Sprite newSprite = Sprite.Create(texture as Texture2D, new Rect(0, 0, texture.width, texture.height), Vector2.zero);
        SpriteRenderer spriteR = gameObject.GetComponent<SpriteRenderer>();
        print(spriteR.sprite.name);
        print(newSprite);
        spriteR.sprite = newSprite;
        print(spriteR.sprite.name);
    }


    public IEnumerator again()
    {
        // Start a download of the given URL
        WWW www = new WWW("file://" + System.IO.Path.Combine(Application.streamingAssetsPath, "Tiles/a.png"));
        using (www);
        {
            // Wait for download to complete
            yield return www;

            // assign texture
            Renderer renderer = GetComponent<Renderer>();
            renderer.material.mainTexture = www.texture;
        }
    }

	IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            if (webRequest.isNetworkError)
            {
                Debug.Log(pages[page] + ": Error: " + webRequest.error);
            }
            else
            {
                Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
            }
        }
    }
}
