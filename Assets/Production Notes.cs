﻿/*

Fixed the resizing script, bundled it into map builder. Also works with undo, but not if resize is the first thing you do
___________________________________________________________________________________________________________

Small copy tile error, not showing correct depth
empty tiles cause map to not load, because empty tiles have no tile set, all fixed now (I think)
Found more bugs than I fixed (oh joy)
started work on granseal tile set, need to do the red roof tile set (split over two files atm)
Also add house wall tiles to granseal tile set
___________________________________________________________________________________________________________

Added sorting group to player
Now he starts at correct layer

Fixed bug on unload play level on map maker

Unity now uses a code called "sorting groups" to handle Sub-hiraracy bullshit so now we need to add that to each sprite

Deleting tiles at the bottom of a stack now works! Even if I had to use a co routine to do it
For future reference Destroy() is not instant. Who knew?

___________________________________________________________________________________________________________

At the end of another week, what do we have to show for it?

Well, not much and a great deal...

Not being able to delete child tiles                                                                        DONE!
Redid roof tiles, now they actually line up

What you need to do is:
Last few tiles for the red roof to finish it off
Last half of the water way tiles (oh, and it turns out they shimmer too, whoopie!)
Unroof effect no longer works, you need to change it to look for child tiles that have the roof landtype
Bowie need to have diagonal walking on stairs
And then Finally (fucking finally) you need to add the npc's back in

But then that should be granseal done. I mean, no items, menu or combat yet but...

Known bugs:
Layer screen doesn't turn off (ever)
Loads of tiles need to be set to ground
On adding upper tiles you can no longer click on the damn things
___________________________________________________________________________________________________________

fixed: issue with map not loading when cameralock set to true
fixed: changed sprite textures to use pixel snap
fixed: roof layer
Added Roof toggle
Changed layers tab to pull up node details
Enabled changing type of child tiles

But in the process you broke

Not being able to delete child tiles
all the maps now have the wrong land type
Also you didnt save the options cog button, need to remake that

___________________________________________________________________________________________________________

Right, where the hell am I up to?

Button on map maker screen that lets you play the map                                                       DONE!
Start coords in top left                                                                                    DONE!
pallet needs to disapper when not in use                                                                    DONE!
auto path creation tool (maybe start with water? Or roofs! Maybe start with roofs!)
Roof! Need to take another look at that
Roof! Again! This time have a toggle for roof tiles on map maker, turn them on and off
___________________________________________________________________________________________________________


Take a look at max_X in Graph, its fucking up auto map creation
Command Console - load new level, add objects to stage, etc
On enter building hide roof (radiate out from tile you touch)                                               DONE!
Add objects to Nodes
Add wondering NPC's
Dialog Boxes (Oh what joy)
Interate with Object on space (A Well is a good one to do, or a chest)
NPC and Object placement on map creation
No diagonal movement option
Cutscene Function (you've got movement down, but currently it quits out on forced movement completian)
Character class (right now its all going off the player class, change that to more generic character class)
Shadow effect for tiles that are next to buildings


Load level from external file
Load tiles from external file
Load list of tiles to be used from external file

__________________________________________________________________________________________________________

And these are just the ones I could think of at half 11 on a Monday night!


__________________________________________________________________________________________________________

14 - 01 - 18

Oh boy, so the roof script is now run ONCE when the map is loaded, this was due to lag on entering building
Roof is a mess now, but shouldn't take too long to redo the tiles.
Next week, maybe look at the Dialog boxes, that way it doesn't feel like the project is just spinning 
its wheels



this line has been changed
__________________________________________________________________________________________________________

Converted Main to singleton, no more passing the main down, to access just use Main.instance
Loading from JSON file is working, but still many bugs and try catch blocks to implament


__________________________________________________________________________________________________________

NPC's now run from JSON file, can move and talk and exit loop
Can't move through objects that are impassable, even if they should be able to
Need a way to pass down overrides that isnt a big list of booleans

__________________________________________________________________________________________________________

*/


